//
//  Queue.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import TaskQueue
import Firebase
import FirebaseDatabase
import RealmSwift
import Realm

class QueueManager: NSObject {
    public var mainQueue: TaskQueue
    
    public let sequenceNode = "sequence"
    public let mainSequenceNode = "sequence/main"
    private var mainJobIndex = 0
    private var queueArray: [Any] = []
    public var currentSequenceID: String?
    private let couchDB: minimalCouch!
    private var couchDBEnabled = false
    
    // MARK: INITIALIZATION
    init(sequenceID: String? = nil, couchDBEnabled: Bool = true) {
        mainQueue = TaskQueue()
        databaseReference.child(mainSequenceNode).removeValue()
        couchDB = minimalCouch(databaseName: databaseName, mainDocumentID: sequenceDocumentID, databaseURL: databaseURL, databaseUsername: databaseUsername, databasePassword: databasePassword)
        self.couchDBEnabled = true
        
        if sequenceID != nil {
            currentSequenceID = sequenceID
            if let sequence = realm.object(ofType: Sequence.self, forPrimaryKey: sequenceID!) {
                for module in sequence.modules {
                    queueArray.append(module.value)
                }
            }
        }
    }
    
    // MARK: ADDING TO QUEUE
    func addToQueue(module: AnyObject) {
        queueArray.append(module)
    }
    
    func addToQueue(waitForAction: WaitForAction) {
        queueArray.append(waitForAction)
    }
    
    func addToSuccessQueue(module: AnyObject, waitForAction: WaitForAction) {
        print("Apppeding module \((module as! Module).name) to success array...")
        // Saving to database as a quick fix to allow polymorphism in Realm
        try! realm.write {
            realm.add(module as! Module)
            waitForAction.successModules.append(AnyModule(module as! Module))
        }
        
    }
    
    func addToFailureQueue(module: AnyObject, waitForAction: WaitForAction) {
        print("Apppeding module \((module as! Module).name) to failure array...")
        // Saving to database as a quick fix to allow polymorphism in Realm
        try! realm.write {
            realm.add(module as! Module)
            waitForAction.failureModules.append(AnyModule(module as! Module))
        }
    }
    
    // MARK: RUNNING QUEUE
    func runMainQueue() {
        print("Sending sequence to database...")
        debugPrint(queueArray)
        if couchDBEnabled {
            var body: [String: Any] = [:]
            var subBody: [String: Any] = [:]
            
            for module in self.queueArray {
                let path = String(self.mainJobIndex)
                subBody[path] = [(module as! Module).name: (module as! Module).dictionary()]
                self.mainJobIndex += 1
            }
            
            body["connected"] = false
            body["main"] = subBody
            
            print("Uploading sequence...")
            debugPrint(subBody)
            debugPrint(body)
            self.couchDB.update(path: self.sequenceNode, value: body) {
                print("Sequence uploaded successfully.")
            }
        } else {
            databaseReference.child(mainSequenceNode).removeValue()
            databaseReference.child(sequenceNode + "/connected").setValue(false)
        
            for module in queueArray {
                mainQueue.tasks +=! {
                    let path = self.mainSequenceNode + "/" + String(self.mainJobIndex)
                    databaseReference.child(path).setValue([(module as! Module).name: (module as! Module).dictionary()])
                    self.mainJobIndex += 1
                }
            }
            
            // Set running flag to true and reset current job index
            databaseReference.child(sequenceNode).setValue(true)
            databaseReference.child(sequenceNode + "/currentJobIndex").setValue(0)
            databaseReference.child(sequenceNode + "/running").setValue(true)
        }
        
        self.mainQueue.run()
    }
    
    // MARK: SAVE QUEUE
    func saveQueue(name: String? = nil) {
        if !array().isEmpty {
            try! realm.write {
                print("Saving sequence...")
                var sequence = Sequence()
                
                // Check if sequence already exists
                if currentSequenceID != nil {
                    print("Sequence already exists with ID \(currentSequenceID!). Updating...")
                    sequence = realm.object(ofType: Sequence.self, forPrimaryKey: currentSequenceID)!
                    if name != nil {
                        sequence.name = name!
                    }
                } else {
                    // Set sequence name
                    if name != nil {
                        sequence.name = name!
                    } else {
                        let randomIndex = Int(arc4random_uniform(UInt32(emojis.count)))
                        sequence.name = "Simulation " + emojis[randomIndex]
                    }
                }
                
                // Setup modules
                let moduleArray = List<AnyModule>()
                debugPrint(array())
                for module in array() {
                    moduleArray.append(AnyModule(module as! Module))
                }
                debugPrint(moduleArray)
                realm.add(array() as! [Module], update: .all)
                
                // Clean up before adding
                realm.delete(sequence.modules)
                sequence.modules.removeAll()
                
                // Add modules to sequence
                sequence.modules.insert(contentsOf: moduleArray, at: 0)
                realm.add(sequence, update: .all)
                currentSequenceID = sequence.id
            }
        } else {
            print("Empty sequence. Canceling saving...")
        }
    }
    
    // MARK: RETRIEVING FROM QUEUE GET
    func array() -> [Any] {
        return queueArray
    }
    
    func moduleAt(index: Int, fromSuccessFailure: Bool? = nil, waitForAction: WaitForAction? = nil) -> Module? {
        switch fromSuccessFailure {
        case nil:
            if index < queueArray.count {
                return queueArray[index] as? Module
            }
        case true?:
            if index < (waitForAction?.successModules.count)! {
                return waitForAction?.successModules[index].value
            }
        case false?:
            if index < (waitForAction?.failureModules.count)! {
                return waitForAction?.failureModules[index].value
            }
        }
        
        return nil
    }
    
    func parallelModuleAt(parentModuleIndex: Int) -> Module? {
        if parentModuleIndex < queueArray.count {
            if let parentModule = queueArray[parentModuleIndex] as? Module {
                if parentModule.parallelModule != nil {
                    return parentModule.parallelModule!.value
                }
            }
        }
        return nil
    }

    
    // MARK: UPDATING QUEUE
    func updateModuleAt(index: Int, module: Module, fromSuccessFailure: Bool? = nil, waitForAction: WaitForAction? = nil) {
        switch fromSuccessFailure {
        case nil:
            print("Updating module \(module.id)...")
            if index < queueArray.count {
                queueArray[index] = module
            }
        case true?:
            print("Updating success module \(module.id)...")
            if index < (waitForAction?.successModules.count)! {
                try! realm.write {
                    waitForAction?.successModules.remove(at: index)
                    waitForAction?.successModules.insert(AnyModule(module), at: index)
                }
            }
        case false?:
            print("Updating failure module \(module.id)...")
            if index < (waitForAction?.failureModules.count)! {
                try! realm.write {
                    waitForAction?.failureModules.remove(at: index)
                    waitForAction?.failureModules.insert(AnyModule(module), at: index)
                }
            }
        }
        try! realm.write {
            realm.add(module, update: .all)
            debugPrint(queueArray)
        }
    }
    
    func updateParallelModuleAt(index: Int, module: Module) {
        print("Updating module \(module.id)...")
        try! realm.write {
            if index < queueArray.count {
                realm.add(module, update: .all)
                if let parentModule = queueArray[index] as? Module {
                    parentModule.parallelModule = AnyModule(module)
                }
            }
        }
    }

    
    func reorder(from: Int, to: Int, toSuccessQueue: Bool? = nil) {
        print("Reordering queue from \(from) to \(to)...")
        switch toSuccessQueue {
        case nil:
            guard let item: Module = queueArray[from] as? Module
            else {
                return
            }
            queueArray.remove(at: from)
            queueArray.insert(item, at: to)
        case true?:
            try! realm.write {
                let item = currentWaitForAction.successModules[from]
                currentWaitForAction.successModules.remove(at: from)
                currentWaitForAction.successModules.insert(item, at: to)
                print("Queue new order:")
                debugPrint(currentWaitForAction.successModules)
            }
        case false?:
            try! realm.write {
                let item = currentWaitForAction.failureModules[from]
                currentWaitForAction.failureModules.remove(at: from)
                currentWaitForAction.failureModules.insert(item, at: to)
                print("Queue new order:")
                debugPrint(currentWaitForAction.failureModules)
            }
        }
    }
    
    // MARK: DELETING AND RESETING QUEUE
    func deleteSequence(at index: Int) {
        try! realm.write {
            let sequence = realm.objects(Sequence.self)[index]
            if sequence != nil {
                realm.delete(sequence.modules)
                realm.delete(sequence)
            }
        }
    }
    
    func deleteSequence(with id: String) {
        try! realm.write {
            if let sequence = realm.object(ofType: Sequence.self, forPrimaryKey: id) {
                realm.delete(sequence.modules)
                realm.delete(sequence)
            }
        }
    }

    
    func deleteModuleAt(index: Int, fromSucessFailure: Bool? = nil, waitForAction: WaitForAction? = nil) {
        try! realm.write {
            switch fromSucessFailure {
            case nil:
                if index < queueArray.count {
                    print("Deleting module \(index)...")
                    queueArray.remove(at: index)
                } else {
                    print("Cannot delete. Index doesn't exist.")
                }
            case true?:
                if index < (waitForAction?.successModules.count)! {
                    print("Deleting success module \(index)...")
                    waitForAction?.successModules.remove(at: index)
                } else {
                    print("Cannot delete success module. Index doesn't exist.")
                }
            case false?:
                if index < (waitForAction?.failureModules.count)! {
                    print("Deleting failure module \(index)...")
                    waitForAction?.failureModules.remove(at: index)
                } else {
                    print("Cannot delete failure module. Index doesn't exist.")
                }
            }
        }
    }
    
    func deleteParallelModuleAt(parentModuleIndex: Int) {
        try! realm.write {
            if parentModuleIndex < queueArray.count {
                print("Deleting paralle module at parent module \(index)...")
                if let parentModule = queueArray[parentModuleIndex] as? Module {
                    debugPrint(parentModule)
                    if let paralleModule = parentModule.parallelModule {
                        debugPrint(paralleModule)
                        realm.delete(paralleModule)
                        parentModule.parallelModule = nil
                    }
                }
            } else {
                print("Cannot delete. Index doesn't exist.")
            }
        }
    }
    
    func emptyAllQueue() {
        mainQueue.removeAll()
        queueArray.removeAll()
        mainJobIndex = 0
        if couchDBEnabled {
           couchDB.delete(path: mainSequenceNode)
        } else {
            databaseReference.child(mainSequenceNode).removeValue()
        }
    }
    
    func reset() {
        mainJobIndex = 0
        if couchDBEnabled {
            couchDB.delete(path: mainSequenceNode)
        } else {
            databaseReference.child(mainSequenceNode).removeValue()
        }
    }
}

