//
//  AnyModule.swift
//  SequenceManager
//
//  Created by Abdol on 3/27/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift

class AnyModule: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var typeName: String = ""
    @objc dynamic var _primaryKey: String = ""
    
    // AnyModule primary key (used in updating already-stored objects)
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // A list of all subclasses that this wrapper can store
    @objc static let supportedClasses: [Module.Type] = [
        Delay.self, WaitForAction.self, ChangeParameters.self, DisplayMessage.self, StopSimulation.self, Bleeding.self, Deoxygenation.self, OxygenatorNoise.self, PowerDisconnection.self, LineShattering.self, Clotting.self, VitalSigns.self
    ]
    
    // Construct the type-erased payment method from any supported subclass
    @objc convenience init(_ module: Module) {
        self.init()
        typeName = String(describing: type(of: module))
        guard let _primaryKeyName = type(of: module).primaryKey() else {
            fatalError("`\(typeName)` does not define a primary key")
        }
        guard let _primaryKeyValue = module.value(forKey: _primaryKeyName) as? String else {
            fatalError("`\(typeName)`'s primary key `\(_primaryKeyName)` is not a `String`")
        }
        _primaryKey = _primaryKeyValue
    }
    
    // Dictionary to lookup subclass type from its name
    @objc static let methodLookup: [String : Module.Type] = {
        var dict: [String : Module.Type] = [:]
        for method in supportedClasses {
            dict[String(describing: method)] = method
        }
        return dict
    }()
    
    // Use to access the *actual* Module value, using `as` to upcast
    @objc var value: Module {        
        guard let type = AnyModule.methodLookup[typeName] else {
            fatalError("Unknown method `\(typeName)`")
        }
        guard let value = try! Realm().object(ofType: type, forPrimaryKey: _primaryKey as AnyObject) else {
            fatalError("`\(typeName)` with primary key `\(_primaryKey)` does not exist")
        }
        return value
    }
}
