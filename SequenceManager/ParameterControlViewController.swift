//
//  ParameterControlViewController.swift
//  AssistedControl
//
//  Created by Abdullah Alsalemi on 10/20/16.
//  Copyright © 2016 QU. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import NotificationCenter
import anim

internal var parameters = Parameters()
internal var parametersPatient = ParametersPatient()


class ParameterControlViewController: UIViewController, UITextFieldDelegate {

    // MARK: INDENTIFERS

    // Database
    var couchDBclientParameters: minimalCouch!
    var parameterDatabaseReference: DatabaseReference!
    
    // MARK: LIVE MODE
    @IBInspectable var liveMode: Bool = false
    @IBInspectable var collapsed: Bool = false

    // MARK: Outlets
    // rpm
    @IBOutlet weak var rpmLabel: UILabel!
    @IBOutlet weak var rpmSlider: UISlider!
    @IBOutlet weak var rpmField: UITextField!
    // Tven
    @IBOutlet weak var PvenLabel: UILabel!
    @IBOutlet weak var PvenSlider: UISlider!
    @IBOutlet weak var PvenField: UITextField!
    // Part
    @IBOutlet weak var PartLabel: UILabel!
    @IBOutlet weak var PartSlider: UISlider!
    @IBOutlet weak var PartField: UITextField!
    // Pint
    @IBOutlet weak var PintLabel: UILabel!
    @IBOutlet weak var PintSlider: UISlider!
    @IBOutlet weak var PintField: UITextField!
    // ∆P
    @IBOutlet weak var deltaPLabel: UILabel!
    // svO2
    @IBOutlet weak var svO2Label: UILabel!
    @IBOutlet weak var svO2Slider: UISlider!
    @IBOutlet weak var svO2Field: UITextField!
    // Tart
    @IBOutlet weak var TartLabel: UILabel!
    @IBOutlet weak var TartSlider: UISlider!
    @IBOutlet weak var TartField: UITextField!
    // Tven
    @IBOutlet weak var TvenLabel: UILabel!
    @IBOutlet weak var TvenSlider: UISlider!
    @IBOutlet weak var TvenField: UITextField!
    // Hb
    @IBOutlet weak var HbLabel: UILabel!
    @IBOutlet weak var HbSlider: UISlider!
    @IBOutlet weak var HbField: UITextField!
    // Hct
    @IBOutlet weak var HctLabel: UILabel!
    @IBOutlet weak var HctSlider: UISlider!
    @IBOutlet weak var HctField: UITextField!
    // V
    @IBOutlet weak var VLabel: UILabel!
    @IBOutlet weak var VSlider: UISlider!
    @IBOutlet weak var VField: UITextField!

    // Wait for Action
    var isSuccessFailureAction: Bool? = nil
    
    // Miscellaneous
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var removeParallelModuleButton: UIButton!
    @IBOutlet weak var addParallelModuleButton: ModuleButton!
    var minimumPrimaryColumnWidth: CGFloat = 0
    
    var activeField: UITextField?
    var defaultSliderThumbColor: UIColor?
    var editModuleIndex: Int?
    var parallelModuleParentIndex: Int?
    
    // Delegate
    var delegate: PopoverDelegate?
    
    // MARK: INITIALIZATION
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TEXTFIELD UTILITY METHODS
        addDoneButtonOnKeyboard(rpmField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(PvenField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(PartField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(PintField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(svO2Field, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(TartField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(TvenField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(HbField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(HctField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        addDoneButtonOnKeyboard(VField, target: self, action: #selector(ParameterControlViewController.doneButtonAction))
        
        // Disable swipe back gesture
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        // Add button setup
        addButton.setTitleColor(moduleButtonTextColorAlt, for: .normal)
        addButton.backgroundColor = moduleButtonBackgroundColorAlt
        
        // Add parallel module button
        if !liveMode {
            addParallelModuleButton.isHidden = true
        }
        
        // Setup
        rpmLabel.textColor = parameterControlColor
        rpmSlider.tintColor = parameterControlColor
        rpmField.textColor = parameterControlColor
        rpmSlider.isContinuous = continuousSliderEvents
        rpmSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        rpmSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        PvenLabel.textColor = parameterControlColor
        PvenSlider.tintColor = parameterControlColor
        PvenField.textColor = parameterControlColor
        PvenSlider.isContinuous = continuousSliderEvents
        PvenLabel.attributedText = parameterLabel("PVen (mmHg)", subscriptCharacter: "V", numberOfSubscriptCharacters: 3)
        PvenSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        PvenSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        PartLabel.textColor = parameterControlColor
        PartSlider.tintColor = parameterControlColor
        PartField.textColor = parameterControlColor
        PartSlider.isContinuous = continuousSliderEvents
        PartLabel.attributedText = parameterLabel("PArt (mmHg)", subscriptCharacter: "A", numberOfSubscriptCharacters: 3)
        PartSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        PartSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        PintLabel.textColor = parameterControlColor
        PintSlider.tintColor = parameterControlColor
        PintField.textColor = parameterControlColor
        PintSlider.isContinuous = continuousSliderEvents
        PintLabel.attributedText = parameterLabel("PInt (mmHg)", subscriptCharacter: "I", numberOfSubscriptCharacters: 3)
        PintSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        PintSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        svO2Label.textColor = parameterControlColor
        svO2Slider.tintColor = parameterControlColor
        svO2Field.textColor = parameterControlColor
        svO2Slider.isContinuous = continuousSliderEvents
        svO2Label.attributedText = parameterLabel("svO2 (%)", subscriptCharacter: "O", numberOfSubscriptCharacters: 2)
        svO2Slider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        svO2Slider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        TartLabel.textColor = parameterControlColor
        TartSlider.tintColor = parameterControlColor
        TartField.textColor = parameterControlColor
        TartSlider.isContinuous = continuousSliderEvents
        TartLabel.attributedText = parameterLabel("TArt (ºC)", subscriptCharacter: "A", numberOfSubscriptCharacters: 3)
        TartSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        TartSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        TvenLabel.textColor = parameterControlColor
        TvenSlider.tintColor = parameterControlColor
        TvenField.textColor = parameterControlColor
        TvenSlider.isContinuous = continuousSliderEvents
        TvenLabel.attributedText = parameterLabel("TVen (ºC)", subscriptCharacter: "V", numberOfSubscriptCharacters: 3)
        TvenSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        TvenSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        HbLabel.textColor = parameterControlColor
        HbSlider.tintColor = parameterControlColor
        HbField.textColor = parameterControlColor
        HbSlider.isContinuous = continuousSliderEvents
        HbSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        HbSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        HctLabel.textColor = parameterControlColor
        HctSlider.tintColor = parameterControlColor
        HctField.textColor = parameterControlColor
        HctSlider.isContinuous = continuousSliderEvents
        HctSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        HctSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        VLabel.textColor = parameterControlColor
        VSlider.tintColor = parameterControlColor
        VField.textColor = parameterControlColor
        VSlider.isContinuous = continuousSliderEvents
        VSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpInside), for: .touchUpInside)
        VSlider.addTarget(self, action: #selector(ParameterControlViewController.sliderDidTouchUpOutside), for: .touchUpOutside)
        
        // MISCELLANEOUS
        defaultSliderThumbColor = rpmSlider.thumbTintColor
        // Change navigation bar and tab bar tint colors
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        tabBarController?.tabBar.barTintColor = UIColor.white
        tabBarController?.tabBar.tintColor = UIColor.black
        
        // Configure split view master view width
        if liveMode {
            splitViewController?.preferredDisplayMode = .allVisible
            splitViewController?.preferredPrimaryColumnWidthFraction = 0.3
            let minimumWidth = min((splitViewController?.view.bounds.size.width)!,(splitViewController?.view.bounds.height)!)
            if let parameterControlVC = splitViewController?.viewControllers.first as? ParameterControlViewController {
                parameterControlVC.minimumPrimaryColumnWidth = minimumWidth / 2.2
            }
            splitViewController?.minimumPrimaryColumnWidth = minimumWidth / 2.2
            splitViewController?.maximumPrimaryColumnWidth = minimumWidth / 2.2
            if let leftNavController = splitViewController?.viewControllers.first as? ParameterControlViewController {
                leftNavController.view.frame = CGRect(x: leftNavController.view.frame.origin.x, y: leftNavController.view.frame.origin.y, width: (minimumWidth / splitViewWidthConstant), height: leftNavController.view.frame.height)
            }
        }
        print("Frame width:", view.frame.width)
        debugPrint("Params:", splitViewController?.preferredPrimaryColumnWidthFraction, splitViewController?.minimumPrimaryColumnWidth, splitViewController?.maximumPrimaryColumnWidth, self.splitViewController?.preferredDisplayMode, minimumPrimaryColumnWidth)
        
        self.setupDatabase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // SCROLLVIEW SETUP
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: (scrollView.subviews.last?.bounds.maxY)!)
        if self.view.bounds.width > 50 {
            NotificationCenter.default.addObserver(self, selector: #selector(ParameterControlViewController.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(ParameterControlViewController.keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
       }
        
        // ASSIGNING TEXTFIELD DELEGATES
        rpmField.delegate = self
        PvenField.delegate = self
        PartField.delegate = self
        PintField.delegate = self
        svO2Field.delegate = self
        TartField.delegate = self
        TvenField.delegate = self
        HbField.delegate = self
        HctField.delegate = self
        VField.delegate = self
    }
    
    func setupDatabase() {
        if !collapsed {
            if couchDBEnabled {
                if editModuleIndex == nil {
                    initCouchDB()
                    listenForParameterChangesCouch()
                } else {
                    setupEditMode()
                }
            } else {
                databaseReference = Database.database().reference()
                if editModuleIndex == nil {
                    listenForParameterChanges()
                } else {
                    setupEditMode()
                }
            }
        }
    }
    
    // MARK: UPDATE UI
    func updateUI() {
        self.rpmSlider.value = parameters.rpm.value != nil ? parameters.rpm.value! : self.rpmSlider.minimumValue
        self.rpmField.text = String(format: rpmFormat, self.rpmSlider.value)
        
        self.PvenSlider.value = parameters.Pven.value != nil ? parameters.Pven.value! : self.PvenSlider.minimumValue
        self.PvenField.text = String(format: pressureFormat, self.PvenSlider.value)
        
        print("Updating Part to: \(parameters.Part.value!)...")
        self.PartSlider.value = parameters.Part.value != nil ? parameters.Part.value! : self.PartSlider.minimumValue
        self.PartField.text = String(format: pressureFormat, self.PartSlider.value)
        
        self.PintSlider.value = parameters.Pint.value != nil ? parameters.Pint.value! : self.PintSlider.minimumValue
        self.PintField.text = String(format: pressureFormat, self.PintSlider.value)
        
        self.svO2Slider.value = parameters.svO2.value != nil ? parameters.svO2.value! : self.svO2Slider.minimumValue
        self.svO2Field.text = String(format: svO2Format, self.svO2Slider.value)
        
        self.TartSlider.value = parameters.Tart.value != nil ? parameters.Tart.value! : self.TartSlider.minimumValue
        self.TartField.text = String(format: temperatureFormat, self.TartSlider.value)
        
        self.TvenSlider.value = parameters.Tven.value != nil ? parameters.Tven.value! : self.TvenSlider.minimumValue
        self.TvenField.text = String(format: temperatureFormat, self.TvenSlider.value)
        
        self.HbSlider.value = parameters.Hb.value != nil ? parameters.Hb.value! : self.HbSlider.minimumValue
        self.HbField.text = String(format: HbFormat, self.HbSlider.value)
        
        self.HctSlider.value = parameters.Hct.value != nil ? parameters.Hct.value! : self.HctSlider.minimumValue
        self.HctField.text = String(format: HctFormat, self.HctSlider.value)
        
        self.VSlider.value = parameters.V.value != nil ? parameters.V.value! : self.VSlider.minimumValue
        self.VField.text = String(format: Vformat, self.VSlider.value)
    }
    
    func updateSlidersThumbColor() {
        if !Parameters.rpmRange.contains(Double(rpmSlider.value)) {
            rpmSlider.thumbTintColor = UIColor.red
        } else {
            rpmSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.PvenRange.contains(Double(PvenSlider.value)) {
            PvenSlider.thumbTintColor = UIColor.red
        } else {
            PvenSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.PartRange.contains(Double(PartSlider.value)) {
            PartSlider.thumbTintColor = UIColor.red
        } else {
            PartSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.PintRange.contains(Double(PintSlider.value)) {
            PintSlider.thumbTintColor = UIColor.red
        } else {
            PintSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.svO2Range.contains(Double(svO2Slider.value)) {
            svO2Slider.thumbTintColor = UIColor.red
        } else {
            svO2Slider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.TartRange.contains(Double(TartSlider.value)) {
            TartSlider.thumbTintColor = UIColor.red
        } else {
            TartSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.TvenRange.contains(Double(TvenSlider.value)) {
            TvenSlider.thumbTintColor = UIColor.red
        } else {
            TvenSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.HbRange.contains(Double(HbSlider.value)) {
            HbSlider.thumbTintColor = UIColor.red
        } else {
            HbSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.HctRange.contains(Double(HctSlider.value)) {
            HctSlider.thumbTintColor = UIColor.red
        } else {
            HctSlider.thumbTintColor = defaultSliderThumbColor
        }
        
        if !Parameters.VRange.contains(Double(VSlider.value)) {
            VSlider.thumbTintColor = UIColor.red
        } else {
            VSlider.thumbTintColor = defaultSliderThumbColor
        }
    }
    
    // MARK: SLIDER METHODS
    @objc func sliderDidTouchUpInside(_ slider: UISlider) {
        
        if !continuousSliderCloudUpdates {
            switch slider {
            case rpmSlider:
                updateParameter(key: "rpm", value: rpmSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                rpmField.text = String(format: rpmFormat, rpmSlider.value)
            case VSlider:
                updateParameter(key: "V", value: VSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                VField.text = String(format: Vformat, VSlider.value)
            case svO2Slider:
                updateParameter(key: "svO2", value: svO2Slider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                svO2Field.text = String(format: svO2Format, svO2Slider.value)
            case PvenSlider:
                updateParameter(key: "Pven", value: PvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                PvenField.text = String(format: pressureFormat, PvenSlider.value)
            case PartSlider:
                updateParameter(key: "Part", value: PartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                updateDeltaP()
                PartField.text = String(format: pressureFormat, PartSlider.value)
            case PintSlider:
                updateParameter(key: "Pint", value: PintSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                updateDeltaP()
                PintField.text = String(format: pressureFormat, PintSlider.value)
            case TartSlider:
                updateParameter(key: "Tart", value: TartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                TartField.text = String(format: temperatureFormat, TartSlider.value)
            case TvenSlider:
                updateParameter(key: "Tven", value: TvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                TvenField.text = String(format: temperatureFormat, TvenSlider.value)
            case HbSlider:
                updateParameter(key: "Hb", value: HbSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                HbField.text = String(format: HbFormat, HbSlider.value)
            case HctSlider:
                updateParameter(key: "Hct", value: HctSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
                HctField.text = String(format: HctFormat, HctSlider.value)
            default:
                break
            }
            updateSlidersThumbColor()
        }
    }
    
    @objc func sliderDidTouchUpOutside(_ slider: UISlider) {
        sliderDidTouchUpInside(slider)
    }

    
    // MARK: RPM
    @IBAction func rpmSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "rpm", value: rpmSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        
        // Update module pump parameters
        /*if rpmSlider.value == 0 {
            updateModuleParameter(key: "pump", value: 0)
        } else {
            //updateModuleParameter(key: "pump", value: 0)
        }*/
        
        rpmField.text = String(format: rpmFormat, rpmSlider.value)
        
        updateSlidersThumbColor()
    }
    @IBAction private func rpmFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        rpmSlider.value = value
        
        updateParameter(key: "rpm", value: rpmSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        // Update module pump parameters
        /*if rpmSlider.value == 0 {
            updateModuleParameter(key: "pump", value: 0)
        } else {
            //updateModuleParameter(key: "pump", value: 0)
        }*/
        
        updateSlidersThumbColor()
    }
    
    // MARK: PVEN
    @IBAction func PvenSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Pven", value: PvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        PvenField.text = String(format: pressureFormat, PvenSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction private func PvenFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        PvenSlider.value = value < 0 ? value : -value // Making sure value is negative
        updateParameter(key: "Pven", value: PvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: PART
    @IBAction func PartSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Part", value: PartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
            updateDeltaP()
        } else {
            updateDeltaP(updateRemote: false)
        }
        PartField.text = String(format: pressureFormat, PartSlider.value)
        
        updateSlidersThumbColor()
    }
    @IBAction private func PartFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        PartSlider.value = value
        updateParameter(key: "Part", value: PartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateDeltaP()
        
        updateSlidersThumbColor()
    }
    
    // MARK: PINT
    @IBAction func PintSliderDidChange(_ sender: Any) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Pint", value: PintSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
            updateDeltaP()
        } else {
            updateDeltaP(updateRemote: false)
        }
        PintField.text = String(format: pressureFormat, PintSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction func PintFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        PintSlider.value = value
        updateParameter(key: "Pint", value: PintSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateDeltaP()
        
        updateSlidersThumbColor()
    }
    
    // MARK: SV02
    @IBAction func svO2SliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "svO2", value: svO2Slider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        svO2Field.text = String(format: svO2Format, svO2Slider.value)
        
        updateSlidersThumbColor()
    }
    @IBAction func svO2FieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        svO2Slider.value = value
        updateParameter(key: "svO2", value: svO2Slider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: TART
    @IBAction func TartSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Tart", value: TartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        TartField.text = String(format: temperatureFormat, TartSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction private func TartFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        TartSlider.value = value
        updateParameter(key: "Tart", value: TartSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: TVEN
    @IBAction func TvenSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Tven", value: TvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        TvenField.text = String(format: temperatureFormat, TvenSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction func TvenFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        TvenSlider.value = value
        updateParameter(key: "Tven", value: TvenSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: HB
    @IBAction func HbSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Hb", value: HbSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        HbField.text = String(format: HbFormat, HbSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction func HbFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        HbSlider.value = value
        updateParameter(key: "Hb", value: HbSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: HCT
    @IBAction func HctSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "Hct", value: HctSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        HctField.text = String(format: HctFormat, HctSlider.value)
        
        updateSlidersThumbColor()
    }
    
    @IBAction func HctFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        HctSlider.value = value
        updateParameter(key: "Hct", value: HctSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    // MARK: V
    @IBAction func VSliderDidChange(_ sender: AnyObject) {
        if continuousSliderCloudUpdates {
            updateParameter(key: "V", value: VSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
        VField.text = String(format: Vformat, VSlider.value)
        
        updateSlidersThumbColor()
    }
    @IBAction func VFieldDidEndEditing(_ sender: UITextField) {
        guard let value = Float(sender.text!)
            else { return }
        VSlider.value = value
        updateParameter(key: "V", value: VSlider.value, updateRemote: liveMode, couchDB: couchDBclientParameters)
        
        updateSlidersThumbColor()
    }
    
    func updateDeltaP(updateRemote: Bool = true) {
        let deltaP = PintSlider.value - PartSlider.value
        deltaPLabel.text = "∆P = " + String(format: deltaPressureFormat, deltaP)
        if updateRemote {
            updateParameter(key: "deltaP", value: deltaP, updateRemote: liveMode, couchDB: couchDBclientParameters)
        }
    }

    // MARK: LISTENING TO DATABASE CHANGE
    func initCouchDB() {
        couchDBclientParameters = minimalCouch(databaseName: databaseName, mainDocumentID: parametersDocumentID, databaseURL: databaseURL, databaseUsername: databaseUsername, databasePassword: databasePassword)
    }
    
    func listenForParameterChanges() {
        print("Listening for parameter changes.")
        _ = databaseReference.observe(DataEventType.value, with: { (snapshot) in
            print("Listened for parameter change.")
            // TODO: Check internet connection before listening to prevent crashing
            // TODO: Start listening as soon as internet connection appears
            
            // Update parameters array
            if let database = snapshot.value as? NSDictionary {
                debugPrint(databaseReference)
                guard let updatedParameters = database["parameters"] as? [String: Float]
                else {
                    print("No parameters remotely.")
                    initParameters(couchDB: self.couchDBclientParameters)
                    return
                }
                initParameters(dictionary: updatedParameters, couchDB: self.couchDBclientParameters)
                self.updateUI()
                self.updateSlidersThumbColor()
                self.updateDeltaP(updateRemote: false)

                print("Parameters updated.")
                debugPrint(updatedParameters)
            } else {
                print("Database retrieval failed.")
                return
            }
        })
    }
    
    func listenForParameterChangesCouch() {
        print("Listening for parameter changes...")
        couchDBclientParameters.startLongPolling {
            print("Listened for parameter change.")
            // TODO: Check internet connection before listening to prevent crashing
            // TODO: Start listening as soon as internet connection appears
            
            // Update parameters array
            let database = self.couchDBclientParameters.database
            guard let updatedParameters = database["parameters"] as? [String: Float]
                else {
                    print("No parameters remotely.")
                    initParameters(couchDB: self.couchDBclientParameters)
                    return
                }
            initParameters(dictionary: updatedParameters, couchDB: self.couchDBclientParameters)
            self.updateUI()
            self.updateSlidersThumbColor()
            self.updateDeltaP(updateRemote: false)
            
            print("Parameters updated.")
            debugPrint(updatedParameters)
        }
    }

    // MARK: TEXT FIELD DELEGATE METHODS
    @objc func keyboardDidShow(_ notification: NSNotification) {
        
        let information = notification.userInfo
        let keyboardInformation = information?[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        let keyboardSize: CGSize = keyboardInformation.size
        let margin: CGFloat = liveMode ? 0 : 50
        print("Keyboard did show with height \(keyboardSize.height).")
        //let height = keyboardSize.height < 500 || liveMode ? keyboardSize.height: 400 - self.activeField!.frame.height
        var height = liveMode ? keyboardSize.height : keyboardSize.height + 125
        if keyboardSize.height >= 500 { height -= 220 }
        //let height: CGFloat = 400 - self.activeField!.frame.height
        
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: height, right: 0.0)
        if liveMode { self.scrollView.contentInset = contentInsets }
        ///scrollView.scrollIndicatorInsets = contentInsets
            
        // Move field to be edit to the top so it's visible
        var aRect = self.view.bounds
        print("View has height \(aRect.height).")
        if aRect.height > (keyboardSize.height - 100) {
            aRect.size.height -= height
        }
        print("View has now height \(aRect.height).")
        guard let origin = self.activeField?.superview?.superview?.frame.origin
            else { return }
        print("Active field's position:")
        debugPrint(origin)
        print(origin.y)
        if /*aRect.contains(origin) &&*/ origin.y > (keyboardSize.height - margin) {
            print("Scrolling to active field...")
            Timer.after(0.1, {
                self.scrollView.contentInset = contentInsets
                self.scrollView.scrollRectToVisible(keyboardSize.height < 500 ? (self.activeField?.frame)! : self.view.frame, animated: true)
                if self.liveMode {
                    self.scrollView.scrollRectToVisible(self.view.frame, animated: true)
                }
            })
        } else {
            scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 0, height: 0), animated: true)
        }
        // Select text in the active field
        activeField?.selectAll(self)
    }

    @objc func keyboardDidHide(_ notifcation: NSNotification) {
        // Not implemented yet.
//        let information = notifcation.userInfo
//        let keyboardInformation = information?[UIKeyboardFrameBeginUserInfoKey] as! CGRect
//        let keyboardSize: CGSize = keyboardInformation.size
//        anim {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height
//            }
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
        
        // Scroll content to original position
        let contentInsets = UIEdgeInsets.zero;
        scrollView.contentInset = contentInsets;
        //scrollView.scrollIndicatorInsets = contentInsets;
        scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 0, height: 0), animated: true)
    }
    
    @objc func doneButtonAction()
    {
        self.rpmField.resignFirstResponder()
        self.PvenField.resignFirstResponder()
        self.PartField.resignFirstResponder()
        self.PintField.resignFirstResponder()
        self.svO2Field.resignFirstResponder()
        self.TartField.resignFirstResponder()
        self.TvenField.resignFirstResponder()
        self.HbField.resignFirstResponder()
        self.HctField.resignFirstResponder()
        self.VField.resignFirstResponder()
    }
    
    // MARK: NAVIGATION
    @IBAction func addModule(_ sender: Any) {
        if isSuccessFailureAction == nil {
            self.performSegue(withIdentifier: "addToSequence", sender: self)
        } else {
            self.performSegue(withIdentifier: "addToWaitForAction", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let changeParameters = ChangeParameters()
        changeParameters.parameters = parameters
        if segue.identifier == "addParallelModule" {
            if let destination = segue.destination as? ModuleCollectionViewController {
                destination.parallelModuleParentIndex = editModuleIndex!
            }
        } else {
            if parallelModuleParentIndex != nil {
                mainQueue.updateParallelModuleAt(index: parallelModuleParentIndex!, module: changeParameters)
            } else {
                if editModuleIndex == nil {
                    if isSuccessFailureAction == nil {
                        mainQueue.addToQueue(module: changeParameters)
                    } else if isSuccessFailureAction == true {
                        mainQueue.addToSuccessQueue(module: changeParameters, waitForAction: currentWaitForAction)
                    } else if isSuccessFailureAction == false {
                        mainQueue.addToFailureQueue(module: changeParameters, waitForAction: currentWaitForAction)
                    }
                    if let destination = segue.destination as? SequenceCollectionViewController {
                        destination.justAdded = true
                    } else if let destination = segue.destination as? WaitForActionViewController {
                        destination.justAdded = true
                    }
                } else {
                    switch isSuccessFailureAction {
                    case nil:
                        mainQueue.updateModuleAt(index: editModuleIndex!, module: changeParameters)
                    case true?, false?:
                        mainQueue.updateModuleAt(index: editModuleIndex!, module: changeParameters, fromSuccessFailure: isSuccessFailureAction!, waitForAction: currentWaitForAction)
                    }
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("Removing keyboard observers...")
        if view.bounds.width > 50 {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
        }
    }
    
    // MARK: EDIT MODE
    func setupEditMode() {
        addButton.setTitle("Update", for: .normal)
        
        var module: ChangeParameters?
        
        if parallelModuleParentIndex != nil {
            module = mainQueue.parallelModuleAt(parentModuleIndex: parallelModuleParentIndex!) as? ChangeParameters
        } else {
            switch isSuccessFailureAction {
            case nil:
                module = mainQueue.moduleAt(index: editModuleIndex!) as? ChangeParameters
            case true?, false?:
                module = mainQueue.moduleAt(index: editModuleIndex!, fromSuccessFailure: isSuccessFailureAction!, waitForAction: currentWaitForAction) as? ChangeParameters
            }
        }

        // Show/hide remove parallel module button
        removeParallelModuleButton.backgroundColor = removeParallelModuleBackgroundColor
        if parallelModuleParentIndex != nil && editModuleIndex != nil {
            removeParallelModuleButton.isHidden = false
        } else {
            removeParallelModuleButton.isHidden = true
        }
        
        if module != nil {
            if let moduleParameters = module!.parameters?.dictionary() as? [String : Float]? {
                initParameters(dictionary: moduleParameters, couchDB: couchDBclientParameters)
                self.updateUI()
                self.updateSlidersThumbColor()
                self.updateDeltaP(updateRemote: false)
            }
        }
    }
    
    @IBAction func removeParallelModule(_ sender: Any) {
        print("Removing parallel module of parent module \(parallelModuleParentIndex)...")
        if parallelModuleParentIndex != nil {
            mainQueue.deleteParallelModuleAt(parentModuleIndex: parallelModuleParentIndex!)
        }
        self.dismiss(animated: true) {
            self.delegate?.popoverDismissed()
        }
    }
    
    // MARK: UTILITIES
    func parameterLabel(_ label: String, subscriptCharacter: Character, numberOfSubscriptCharacters: Int) -> NSMutableAttributedString {
        return NSMutableAttributedString().characterSubscriptAndSuperscript(
            string: label,
            characters: [subscriptCharacter],
            type: .aSub,
            fontSize: 17,
            scriptFontSize: 14,
            offSet: 3,
            length: [numberOfSubscriptCharacters],
            alignment: .left,
            color: parameterControlColor)
    }
}














