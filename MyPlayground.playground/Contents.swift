//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//// Bezier Drawing
let bezierPath = UIBezierPath()
bezierPath.move(to: CGPoint(x: 0, y: 15.5))
bezierPath.addLine(to: CGPoint(x: 0, y: 106.5))
UIColor.lightGray.setStroke()
bezierPath.lineWidth = 1
bezierPath.stroke()


//// Oval Drawing
let ovalPath = UIBezierPath(ovalIn: CGRect(x: 0, y: 10, width: 9, height: 9))
UIColor.lightGray.setFill()
ovalPath.fill()
