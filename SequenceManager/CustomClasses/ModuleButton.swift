//
//  ModuleButton.swift
//  SequenceManager
//
//  Created by Abdol on 4/8/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class ModuleButton: UIButton {
    
    @objc var originalButtonText: String?
    @objc var activityIndicator: UIActivityIndicatorView!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = buttonCornerRadius
        self.layer.masksToBounds = true
        
        if isEnabled == true {
            self.alpha = 1
        } else {
            self.alpha = disabledFieldAlpha
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled == true {
                self.alpha = 1
            } else {
                self.alpha = disabledFieldAlpha
            }
        }
    }
    
    @objc func showLoading() {
        originalButtonText = self.titleLabel?.text
        self.setTitle("", for: UIControl.State.normal)
        
//        if (activityIndicator == nil) {
            activityIndicator = createActivityIndicator()
//        } else {
         //   activityIndicator.isHidden = false
//        }
        
        showSpinning()
    }
    
    @objc func hideLoading() {
        if activityIndicator != nil {
            self.setTitle(originalButtonText, for: .normal)
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = false
        activityIndicator.color = moduleButtonTextColor
        return activityIndicator
    }
    
    private func showSpinning() {
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        centerActivityIndicatorInButton()
        activityIndicator.startAnimating()
    }
    
    private func centerActivityIndicatorInButton() {
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
    }
}
