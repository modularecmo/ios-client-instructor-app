//
//  Parameters.swift
//  AssistedControl
//
//  Created by Abdullah Alsalemi on 10/20/16.
//  Copyright © 2016 QU. All rights reserved.
//

import Foundation
import RealmSwift

class ParametersPatient: Object {
    @objc dynamic var id: String = UUID().uuidString
    var ecg = RealmOptional<Float>()
    var spo2 = RealmOptional<Float>()
    var resp = RealmOptional<Float>()
    var temp = RealmOptional<Float>()
    var SBP = RealmOptional<Float>()
    var DBP = RealmOptional<Float>()
    var NISBP = RealmOptional<Float>()
    var NIDBP = RealmOptional<Float>()
    var BIS = RealmOptional<Float>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func names() -> Array<String> {
        return ["ecg", "spo2", "resp", "temp", "SBP", "DBP", "NISBP", "NIDBP", "BIS"]
    }
    
    func dictionary() -> [String: Float?] {
        return ["ecg": ecg.value != nil ? Float(Int(ecg.value!)) : 0.0, "spo2": spo2.value, "resp": resp.value, "temp": temp.value, "SBP": SBP.value, "DBP": DBP.value, "NISBP": NISBP.value, "NIDBP": NIDBP.value, "BIS": BIS.value]
    }

    // MARK: Parameter Normal Operating Ranges
    static let ecgRange = 0...220
    static let spo2Range = 0...100
    static let respRange = 0...30
    static let tempRange = 34...41.5
    static let SBPRange = 0...180
    static let DBPRange = 0...120
    static let NISBPRange = 0...180
    static let NIDBPRange = 0...120
    static let BISRange = 0...100
}
