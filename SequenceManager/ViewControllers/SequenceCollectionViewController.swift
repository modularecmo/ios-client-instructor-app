//
//  SequenceCollectionViewController.swift
//  SequenceManager
//
//  Created by Abdol on 3/18/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Hue
import anim

private let moduleBlockIdentifier = "blockCell"
private let bundleBlockIdentifier = "bundleCell"
private let waitForActionReuseIdentifier = "waitForActionCell"
private let addBlockCellIdentifier = "addBlockCell"

class SequenceCollectionViewController: UICollectionViewController, UIPopoverPresentationControllerDelegate, UICollectionViewDelegateFlowLayout, PopoverDelegate {

    @IBOutlet weak var startButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    @IBOutlet weak var sequenceCollectionViewLayout: UICollectionViewFlowLayout!
    var editMode = false
    var sequenceID: String?
    var justAdded = false
    var isReordering = false
    
    @IBInspectable var liveMode = true
    
    // MARK: INITIALIZATION
    override func viewDidLoad() {
        super.viewDidLoad()
        self.installsStandardGestureForInteractiveMovement = true
        
        // Change navigation bar tint and text color
        self.navigationController?.navigationBar.barTintColor = navigationBarTintColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navigationBarTextColor]
        navigationController?.navigationBar.tintColor = navigationBarTextColor
        
        // Retrieve sequence
        mainQueue = QueueManager(sequenceID: sequenceID)
        print("Modules array:")
        debugPrint(mainQueue.array())
        
        // Draw timeline
        drawTimelineTo(view: collectionView!)
        
        // Set title
        if sequenceID != nil {
            guard let sequenceName = realm.object(ofType: Sequence.self, forPrimaryKey: sequenceID)?.name
                else { return }
            title = sequenceName
        }
        
        // Custom back button
        navigationController?.setNavigationBarHidden(false, animated:true)
        let myBackButton:UIButton = UIButton(type: .custom)
        myBackButton.addTarget(self, action: #selector(SequenceCollectionViewController.saveAndClose), for: UIControl.Event.touchUpInside)
        myBackButton.setTitle("Save and Close", for: UIControl.State.normal)
        myBackButton.setTitleColor(UIColor.gray, for: UIControl.State.normal)
        myBackButton.sizeToFit()
        let myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBackButton)
        self.navigationItem.leftBarButtonItem = myCustomBackButtonItem
        
        // Setting popover presentation controller delegate
        self.popoverPresentationController?.delegate = self
        debugPrint(self.popoverPresentationController)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionView?.reloadData()
        updateCollectionViewLayout()

        print("Current modules:")
        print(mainQueue.array())
        if justAdded, let itemsCount = (collectionView?.numberOfItems(inSection: 0)) {
            collectionView?.scrollToItem(at: IndexPath(row: itemsCount - 1, section: 0), at: .bottom, animated: true)
            justAdded = false
        }
        
        // Enable swipe back gesture
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // Disable start button when sequence is empty
        disableEnableStartButton()
        
        // Change navigation bar tint and text color
        self.navigationController?.navigationBar.barTintColor = normalBarBackgroundColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        navigationController?.navigationBar.tintColor = UIColor.black
        
        // Configure split view master view width
        anim { settings -> (animClosure) in
            settings.duration = 0.4
            return {
                self.splitViewController?.preferredPrimaryColumnWidthFraction = 0
                self.splitViewController?.minimumPrimaryColumnWidth = 0
                self.splitViewController?.maximumPrimaryColumnWidth = 0
            }
        }
        
        // Draw timeline
        drawTimelineTo(view: collectionView!)
    }
    
    // MARK: COLLECTION VIEW DATASOURCE
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Main queue has \(mainQueue.array().count) items.")
        return (mainQueue.array().count + 1)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Setting cell width
        if indexPath.row != (mainQueue.array().count) {
            if let module = mainQueue.array()[indexPath.row] as? Module {
                if module.label == "Wait for Action" { // Wait for Action
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: waitForActionReuseIdentifier, for: indexPath) as! WaitForActionCollectionViewCell
                    let modulesCount = (module as! WaitForAction).successModules.count > (module as! WaitForAction).failureModules.count ? (module as! WaitForAction).successModules.count : (module as! WaitForAction).failureModules.count
                    
                    let cellHeight: CGFloat = CGFloat(modulesCount) * waitForActionModuleItemSize.height + 120
                    cell.collectionViewHeight = cellHeight
                    print("Wait for action cell height \(cellHeight).")
                    cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cellWidth, height: cellHeight)
                    cell.contentView.frame = CGRect(x: cell.contentView.frame.origin.x, y: cell.contentView.frame.origin.y, width: cellWidth, height: cell.frame.height)
                    cell.successCollectionView.frame = CGRect(origin: cell.successCollectionView.frame.origin, size: CGSize(width: cell.successCollectionView.frame.width, height: cellHeight))
                    cell.failureCollectionView.frame = CGRect(origin: cell.failureCollectionView.frame.origin, size: CGSize(width: cell.failureCollectionView.frame.width, height: cellHeight))

                    cell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
                    let textColor = UIColor.white
                    
                    cell.moduleLabel.text = module.label
             
                    if let actionLabelString = (module as! WaitForAction).actionLabel {
                        cell.moduleLabel.text = actionLabelString.isEmpty ? cell.moduleLabel.text! : cell.moduleLabel.text! + ": " + actionLabelString
                        cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cellWidth, height: waitForActionCellHeight)
                        cell.contentView.frame = CGRect(x: cell.contentView.frame.origin.x, y: cell.contentView.frame.origin.y, width: cellWidth, height: cell.frame.height)
                    }
                    
                    cell.detailLabel.isHidden = true
                    // cell.detailLabel.text = Module.detailTextFromModule(module: module)
                    
                    cell.detailLabel.textColor = textColor
                    cell.moduleLabel.textColor = textColor
                    cell.deleteButton.isHidden = true
                    cell.reorderButton.isHidden = true
                    if editMode {
                        // cell.reorderButton.isHidden = false
                        cell.deleteButton.isHidden = false
                        cell.deleteButton.setTitleColor(textColor, for: .normal)
                        cell.deleteButton.tag = indexPath.row
                        cell.deleteButton.addTarget(self, action: #selector(SequenceCollectionViewController.deleteModule), for: .touchUpInside)
                    }
                    
                    cell.successModules = (module as! WaitForAction).successModules
                    cell.failureModules = (module as! WaitForAction).failureModules
                    
                    cell.layoutSubviews()
                    cell.updateConstraints()
                    return cell
                } else { // Bundle Module Cell
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bundleBlockIdentifier, for: indexPath) as! BundleCollectionViewCell
                    cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cellWidth, height: cellHeight)
                    cell.contentView.frame = CGRect(x: cell.contentView.frame.origin.x, y: cell.contentView.frame.origin.y, width: cellWidth, height: cell.frame.height)
                    
                    cell.firstModuleView.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
                    let textColor = UIColor.white
                    
                    cell.firstModuleLabel.text = module.label
                    cell.firstDetailLabel.text = Module.detailTextFromModule(module: module)
                    
                    cell.firstModuleLabel.textColor = textColor
                    cell.firstDetailLabel.textColor = textColor

                    cell.stackView.distribution = .fillEqually
                    cell.secondModuleView.isHidden = true
                    
                    if !Module.nonParallelModules().contains(cell.firstModuleLabel.text!) {
                        cell.secondModuleView.isHidden = false
                        cell.stackView.distribution = .fillProportionally
                        cell.secondModuleView.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!).add(hue: 0, saturation: -0.2, brightness: 0, alpha: 0), frame: cell.frame)
                        cell.secondModuleLabel.text = "+"
                        cell.secondDetailLabel.text = nil
                        cell.secondModuleLabel.textColor = textColor
                        cell.secondModuleView.tag = indexPath.row // identifier used in editing
                        cell.secondModuleView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SequenceCollectionViewController.addParallelModule)))
                    }
                    
                    if module.parallelModule != nil {
                        print("Parallel Module:")
                        debugPrint(module.parallelModule?.value)
                        
                        cell.secondModuleView.isHidden = false
                        cell.stackView.distribution = .fillEqually
                        
                        cell.secondModuleView.backgroundColor = gradientFrom(color: UIColor(hex: module.parallelModule!.value.tintColorString!), frame: cell.frame)
                        //cell.removeConstraint(cell.secondModuleWidthConstraint)
                        
                        cell.secondModuleLabel.text = module.parallelModule?.value.label
                        cell.secondDetailLabel.text = Module.detailTextFromModule(module: module.parallelModule!.value)
                        
                        cell.secondModuleLabel.textColor = textColor
                        cell.secondDetailLabel.textColor = textColor
                        
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SequenceCollectionViewController.editParallelModule))
                        cell.secondModuleView.isUserInteractionEnabled = true
                        cell.secondModuleView.addGestureRecognizer(tapGesture)
                    }
                    
                    cell.deleteButton.isHidden = true
                    cell.reorderButton.isHidden = true
                    
                    if editMode {
                       // cell.reorderButton.isHidden = false
                        cell.deleteButton.isHidden = false
                        cell.deleteButton.setTitleColor(textColor, for: .normal)
                        cell.deleteButton.tag = indexPath.row
                        cell.deleteButton.addTarget(self, action: #selector(SequenceCollectionViewController.deleteModule), for: .touchUpInside)
                    }
                    
                    cell.layoutSubviews()
                    cell.updateConstraints()
                    return cell
                }
                /*} else { // Module Cell
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: moduleBlockIdentifier, for: indexPath) as! ModuleCollectionViewCell
                    cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cellWidth, height: cellHeight)
                    cell.contentView.frame = CGRect(x: cell.contentView.frame.origin.x, y: cell.contentView.frame.origin.y, width: cellWidth, height: cell.frame.height)
                    cell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
                    let textColor = (UIColor.black.isContrastingWith(cell.backgroundColor!)) ? UIColor.black : UIColor.white
                    
                    cell.moduleLabel.text = module.label
                    cell.detailLabel.text = Module.detailTextFromModule(module: module)
                    
                    cell.detailLabel.textColor = textColor
                    cell.moduleLabel.textColor = textColor
                    cell.deleteButton.isHidden = true
                    cell.reorderButton.isHidden = true
                    if editMode {
                        cell.reorderButton.isHidden = false
                        cell.deleteButton.isHidden = false
                        cell.deleteButton.setTitleColor(textColor, for: .normal)
                        cell.deleteButton.tag = indexPath.row
                        cell.deleteButton.addTarget(self, action: #selector(SequenceCollectionViewController.deleteModule), for: .touchUpInside)
                    }
                    cell.layoutSubviews()
                    cell.updateConstraints()
                    return cell
                }*/
        }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addBlockCellIdentifier, for: indexPath) as! ModuleCollectionViewCell
            cell.moduleLabel.text = "+ add"
            cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cellWidth, height: cell.frame.size.height)
            cell.contentView.frame = CGRect(x: cell.contentView.frame.origin.x, y: cell.contentView.frame.origin.y, width: cellWidth, height: cell.frame.size.height)
            cell.backgroundColor = addModuleCellBackgroundColor
            cell.layoutSubviews()
            cell.updateConstraints()
            return cell
        }
        return UICollectionViewCell()
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if indexPath.row != (mainQueue.array().count) /* &&  !isReordering */ {
            if mainQueue.array()[indexPath.row] is WaitForAction {
                if let cell = collectionView.cellForItem(at: indexPath) as? WaitForActionCollectionViewCell {
                    print("Layout: Wait for action cell height \(cell.collectionViewHeight).")
                    return CGSize(width: cellWidth, height: cell.collectionViewHeight)
                }
            }
        }
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if indexPath.row == (collectionView.numberOfItems(inSection: indexPath.section) - 1) {
            print("Can't move item at \(indexPath)")
            return false // Disable moving the add button
        } else {
            print("Can move item at \(indexPath)")
            isReordering = true
            return true
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, targetIndexPathForMoveFromItemAt originalIndexPath: IndexPath, toProposedIndexPath proposedIndexPath: IndexPath) -> IndexPath {
        if proposedIndexPath.row == (collectionView.numberOfItems(inSection: proposedIndexPath.section) - 1) {
            print("Moving item original \(originalIndexPath)")
            return originalIndexPath
        } else {
            print("Moving item proposed \(proposedIndexPath)")
            updateCollectionViewLayout()
            return proposedIndexPath
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("Actually moving item from \(sourceIndexPath) to \(destinationIndexPath).")
        let sIndex = sourceIndexPath.row
        let dIndex = destinationIndexPath.row
        mainQueue.reorder(from: sIndex, to: dIndex)
        isReordering = false
        collectionView.reloadData()
        updateCollectionViewLayout()
        mainQueue.saveQueue()
        print("Moved item from \(sIndex) to \(dIndex).")
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? BundleCollectionViewCell {
            switch cell.firstModuleLabel.text! {
            case "Delay", "Bleeding", "Oxygenation Failure", "Pump Noise", "Line Shattering", "Power Disconnection", "Display Message", "Clotting", "Vital Signs":
                self.performSegue(withIdentifier: "editModule", sender: self)
            case "Change Parameters":
                self.performSegue(withIdentifier: "editChangeParameters", sender: self)
            default:
                if (cell.firstModuleLabel.text?.contains("Wait for Action"))! {
                    self.performSegue(withIdentifier: "editWaitForAction", sender: self)
                }
            }
        } else if (collectionView.cellForItem(at: indexPath) as? WaitForActionCollectionViewCell) != nil {
            self.performSegue(withIdentifier: "editWaitForAction", sender: self)
        }
    }
    
    // MARK: EDIT MODE
    @IBAction func toggleEditMode(_ sender: Any) {
        editMode = !editMode
        editButton.title = editMode ? "Done" : "Edit"
        collectionView?.reloadData()
        collectionView?.performBatchUpdates(nil, completion: nil)
        collectionView?.layoutIfNeeded()
    }
    
    @objc func deleteModule(_ sender: UIButton) {
        print("Deleting module \(sender.tag)...")
        mainQueue.deleteModuleAt(index: sender.tag)
        collectionView?.reloadData()
        updateCollectionViewLayout()
        disableEnableStartButton()
    }
    
    func disableEnableStartButton() {
        if let modulesCount = collectionView?.numberOfItems(inSection: 0) {
            startButton.isEnabled = modulesCount > 1
        } else {
            startButton.isEnabled = false
        }
    }

    @objc func editParallelModule(recognizer: UITapGestureRecognizer) {
        if let indexPathRow = recognizer.view?.tag {
            collectionView?.selectItem(at: IndexPath(row: indexPathRow, section: 0), animated: false, scrollPosition: .init(rawValue: 0))
            
            if let module = mainQueue.moduleAt(index: indexPathRow)?.parallelModule?.value {
                if module.label == "Change Parameters" {
                    self.performSegue(withIdentifier: "editParallelChangeParametersModule", sender: self)
                } else {
                    self.performSegue(withIdentifier: "editParallelModule", sender: self)
                }
            }
        }
    }
    
    // MARK: RUN SEQUENCE
    @IBAction func startSequence(_ sender: Any) {
        checkInternetConnection(target: self, handler: { connected in
            if connected {
                self.performSegue(withIdentifier: "startSequence", sender: self)
            }
        }, message: "You are not connected to the internet. Please connect to a network to run a sequence.")
    }
    
    // MARK: SAVE SEQUENCE
    func save(close: Bool = false) {
        if sequenceID == nil {
            let alertController = UIAlertController(title: "Save Sequence", message: "Enter Sequence Name", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Simulation name"
            }
            let stopAction = UIAlertAction(title: "Save", style: .default, handler: {
                action in
                let textField = alertController.textFields![0] as UITextField
                print("Saving \(textField.text)...")
                mainQueue.saveQueue(name: textField.text)
                self.title = textField.text
                
                if close {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(stopAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        } else {
            mainQueue.saveQueue()
            if close {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    // MARK: NAVIGATION
    @objc func saveAndClose() {
        if (collectionView?.numberOfItems(inSection: 0))! > 1 {
            save(close: true)
        } else {
            mainQueue.emptyAllQueue()
            if let sequenceID = mainQueue.currentSequenceID {
                mainQueue.deleteSequence(with: sequenceID)
            }
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (collectionView?.numberOfItems(inSection: 0))! > 1 {
            mainQueue.saveQueue()
        }
    }
    
    @objc func addParallelModule(sender: UITapGestureRecognizer) {
        if let indexPathRow = sender.view?.tag {
            collectionView?.selectItem(at: IndexPath(row: indexPathRow, section: 0), animated: false, scrollPosition: .init(rawValue: 0))
            self.performSegue(withIdentifier: "addParallelModule", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Setup popover controller
        if segue.destination.modalPresentationStyle == .popover {
            segue.destination.popoverPresentationController?.sourceView = self.view
            segue.destination.popoverPresentationController?.delegate = self
        }
        
        switch segue.identifier {
        case "editModule"?:
            if let navigationController = segue.destination as? UINavigationController {
                if let destination = navigationController.children.first as? ModuleConfigureViewController {
                    if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                        destination.editModuleIndex = indexPath.row
                        if let selectedCell = collectionView?.cellForItem(at: indexPath) as? BundleCollectionViewCell {
                            destination.selectedModuleLabel = selectedCell.firstModuleLabel.text
                        }
                    }
                }
            }
        case "editParallelModule"?:
            if let navigationController = segue.destination as? UINavigationController {
                if let destination = navigationController.children.first as? ModuleConfigureViewController {
                    if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                        destination.delegate = self
                        destination.parallelModuleParentIndex = indexPath.row
                        destination.editModuleIndex = destination.parallelModuleParentIndex
                        if let selectedCell = collectionView?.cellForItem(at: indexPath) as? BundleCollectionViewCell {
                            destination.selectedModuleLabel = selectedCell.secondModuleLabel.text
                        }
                    }
                }
            }
        case "editParallelChangeParametersModule"?:
            if let navigation = segue.destination as? UINavigationController {
                if let destination = navigation.children.first as? ParameterControlViewController {
                    if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                        destination.delegate = self
                        destination.parallelModuleParentIndex = indexPath.row
                        destination.editModuleIndex = destination.parallelModuleParentIndex
                    }
                }
            }
        case "editChangeParameters"?:
            if let navigation = segue.destination as? UINavigationController {
                if let destination = navigation.children.first as? ParameterControlViewController {
                    if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                        destination.editModuleIndex = indexPath.row
                    }
                }
            }
        case "editWaitForAction"?:
            print("Segueing to wait for action edit mode...")
            debugPrint(segue.destination)
            if let destination = segue.destination.children.first as? WaitForActionViewController {
                if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                    destination.editModuleIndex = indexPath.row
                }
            }
        case "addParallelModule"?:
            print("Segueing to add parallel module...")
            if let navigation = segue.destination as? UINavigationController {
                if let destination = navigation.children.first as? ModuleCollectionViewController {
                    if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
                        destination.parallelModuleParentIndex = indexPath.row
                    }
                }
            }
        case "startSequence"?: break
//            let lastIndex = mainQueue.array().count - 1
//            if let lastModule = (mainQueue.array() as! [Module])[lastIndex] as? Module {
//                if lastModule.label != "Stop Simulation" {
//                    print("No stop simulation module found. Adding...")
//                    mainQueue.addToQueue(module: StopSimulation())
//                }
//            }
        default:
            break
        }
    }
    
    @IBAction func prepareForUnwind(_ segue: UIStoryboardSegue) {
        print("Unwinding to sequence...")
        collectionView?.reloadData()
        updateCollectionViewLayout()
        disableEnableStartButton()
        Timer.after(1, {
            self.collectionView?.reloadData()
            self.updateCollectionViewLayout()
        })
    }
    
    // MARK: SCROLL VIEW
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCollectionViewLayout()
    }
    
    // MARK: ORIENTATION CHANGES
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        print("Did rotate device...")
        
        // Set colletion view section inset
        collectionView?.reloadData()
        updateCollectionViewLayout()
        
        // Draw timeline
        drawTimelineTo(view: collectionView!)
        
    }
    
    // MARK: POPOVER DELEGATE
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        collectionView?.reloadData()
        updateCollectionViewLayout()
    }
    
    func popoverDismissed() {
        collectionView?.reloadData()
        self.updateCollectionViewLayout()
    }
    
    // MARK: UTILITIES
    func updateCollectionViewLayout() {
        print("Updating sequence collection view layout...")
        //collectionView?.reloadData()
        let sideMargin = self.view.bounds.width / 5
        sequenceCollectionViewLayout.sectionInset = UIEdgeInsets(top: 15, left: sideMargin, bottom: 15, right: sideMargin)
        collectionView?.collectionViewLayout.invalidateLayout()
        collectionView?.performBatchUpdates(nil, completion: nil)
        collectionView?.layoutIfNeeded()
        mainQueue.saveQueue()
    }
}







