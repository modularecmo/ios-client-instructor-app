//
//  SequenceCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 4/2/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
//import Dance
import anim

class WaitForActionRunningCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var moduleStatusLabel: UILabel!
    @IBOutlet weak var successButton: UIButton!
    @IBOutlet weak var failureButton: UIButton!

    @IBOutlet weak var subModuleView: UIView!
    @IBOutlet weak var nextButton: ModuleButton!
    @IBOutlet weak var subModuleLabel: UILabel!
    @IBOutlet weak var subModuleDetailLabel: UILabel!
    
    var constraint = NSLayoutConstraint()
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = moduleCornerRadius * 1.5
        self.layer.masksToBounds = true
    }
    
    func initSetup() {
        //subModuleView.layer.position.y = 3 * self.bounds.height
        self.constraint = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        //self.addConstraint(constraint)
        subModuleView.isHidden = true
        
        subModuleView.backgroundColor = waitForActionSubModuleLoadingBackgroundColor
        subModuleLabel.text = waitForActionSubModuleLoadingMessage
        subModuleDetailLabel.text = nil
    }
    
    func showSubModule() {
        /*self.dance.animate(duration: 1, curve: .easeOut) {
        }/*.addCompletion { (position) in
            self.dance.animate(duration: 0.5, curve: .easeOut) {_ in
                self.subModuleView.isHidden = false
                self.layoutSubviews()
            }.start(after: 0.1)
        }*/
        self.dance.start()*/
        anim {
            self.subModuleView.isHidden = false
            self.layoutSubviews()
        }
    }
    
    func transitionToNextSubModule(backgroundColor: UIColor, setupClosure: @escaping () -> Void) {
       /* self.dance.animate(duration: 0.5, curve: .linear) { _ in
            self.subModuleView.backgroundColor = backgroundColor
            self.subModuleLabel.alpha = 0
            self.subModuleDetailLabel.alpha = 0
            self.nextButton.alpha = 0
        }.addCompletion { (position) in
            setupClosure()
            self.nextButton.setTitle("Next", for: .normal)
            self.nextButton.hideLoading()
            self.dance.animate(duration: 0.4, curve: .linear) { _ in
                self.subModuleLabel.alpha = 1
                self.subModuleDetailLabel.alpha = 1
                self.nextButton.alpha = 1
            }.start(after: 0.1)
        }
        self.dance.start()*/
        
        anim {
            self.subModuleView.backgroundColor = backgroundColor
            self.subModuleLabel.alpha = 0
            self.subModuleDetailLabel.alpha = 0
            self.nextButton.alpha = 0
            }.then {
                setupClosure()
                self.nextButton.setTitle("Next", for: .normal)
                self.nextButton.hideLoading()
                anim {
                    self.subModuleLabel.alpha = 1
                    self.subModuleDetailLabel.alpha = 1
                    self.nextButton.alpha = 1
                }
        }
    }
    
    override func layoutSubviews() {
        successButton.backgroundColor = successTintColor
        failureButton.backgroundColor = failureTintColor
    }
    
}
