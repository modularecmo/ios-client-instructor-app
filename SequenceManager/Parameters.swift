//
//  Parameters.swift
//  AssistedControl
//
//  Created by Abdullah Alsalemi on 10/20/16.
//  Copyright © 2016 QU. All rights reserved.
//

import Foundation
import RealmSwift

class Parameters: Object {
    @objc dynamic var id: String = UUID().uuidString
    var rpm = RealmOptional<Float>() // pump speed
    var Pven = RealmOptional<Float>() // venous pressure
    var Part = RealmOptional<Float>() // arterial pressure
    var Pint = RealmOptional<Float>()
    var deltaP = RealmOptional<Float>() // Pint - Part
    var svO2 = RealmOptional<Float>() // oxygen saturation
    var Tart = RealmOptional<Float>() // arterial side temperature
    var Tven = RealmOptional<Float>() // venous side temperature
    var Hb = RealmOptional<Float>() // hemoglobin concentration
    var Hct = RealmOptional<Float>() // % volume of red blood cells in body, related to Hb
    var V = RealmOptional<Float>() // flowrate (leters of blood per minute (depends on rpm and fluid status of patient))
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func names() -> Array<String> {
        return ["rpm", "Pven", "Part", "Pint", "deltaP", "svO2", "Tart", "Tven", "Hb", "Hct", "V", "deltaP"]
    }
    
    func dictionary() -> [String: Float?] {
        return ["rpm": rpm.value != nil ? Float(Int(rpm.value!)) : 0.0, "Pven": Pven.value, "Part": Part.value, "Pint": Pint.value, "svO2": svO2.value, "Tart": Tart.value, "Tven": Tven.value, "Hb": Hb.value, "Hct": Hct.value, "V": V.value, "deltaP": deltaP.value]
    }

    // MARK: Parameter Normal Operating Ranges
    static let rpmRange = 1500.0...3700.0
    static let PvenRange = -100.0 ... -60.0
    static let PartRange = 150.0...300.0
    static let PintRange = 180.0...300.0
    static let svO2Range = 50.0...75.0
    static let TartRange = 35.5...36.5
    static let TvenRange = 36...36.8
    static let HbRange = 8.0...10.0
    static let HctRange = 24.0...30.0
    static let VRange = 1.5...5.0
}
