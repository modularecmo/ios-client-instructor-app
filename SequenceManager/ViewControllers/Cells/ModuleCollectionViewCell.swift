//
//  ModuleCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 3/16/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class ModuleCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var reorderButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = moduleShadowRadius
        self.layer.shadowOpacity = moduleShadowOpacity
        self.layer.masksToBounds = false;
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: moduleCornerRadius).cgPath
        
        self.layer.cornerRadius = moduleCornerRadius
    }
}
