//
//  PowerDisconnection.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class PowerDisconnection: Module {
    let batteryLevel = RealmOptional<Float>()
    @objc dynamic var fullBatteryLife: Int = 0 // in minutes
    static var timeRemainingRange: ClosedRange<Float> = 0...15  // in minutes
    
    required override init() {
        super.init()
        self.label = "Power Disconnection"
        self.name = "powerDisconnection"
        self.descriptionString = "Simulate power disconnection to the ECMO machine for a specified period (in seconds), battery level, and remaining its corresponding remaining time (in minutes)."
        self.tintColorString = "#167B6E" // "#69306D" //"#666666"
        self.batteryLevel.value = nil
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil", "batteryLevel": batteryLevel.value ?? "nil", "fullBatteryLife": fullBatteryLife]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    static func getBatteryLevel(index: Int) -> Float {
        switch index {
        case 0:
            return 0
        case 1:
            return 0.25
        case 2:
            return 0.5
        case 3:
            return 0.75
        case 4:
            return 1
        default:
            return 0
        }
    }
    
    static func getIndexFrom(value: Float?) -> Int {
        switch value {
        case 0?:
            return 0
        case 0.25?:
            return 1
        case 0.5?:
            return 2
        case 0.75?:
            return 3
        case 1?:
            return 4
        default:
            return 0
        }
    }
}
