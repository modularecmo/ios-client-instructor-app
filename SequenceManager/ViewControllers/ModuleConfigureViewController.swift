//
//  ModuleConfigureViewController.swift
//  SequenceManager
//
//  Created by Abdol on 3/20/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Hue
import ColorPicKit
import anim

class ModuleConfigureViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, PopoverDelegate {
   
    // Views
    @IBOutlet weak var moduleView: UIView!
    @IBOutlet weak var masterStackView: UIStackView!
    @IBOutlet weak var durationStackView: UIStackView!
    @IBOutlet weak var moduleStackView: UIStackView!
    @IBOutlet weak var messageStackView: UIStackView!
    @IBOutlet weak var priorityStackView: UIStackView!
    @IBOutlet weak var alarmStackView: UIStackView!
    @IBOutlet weak var barMessageStackView: UIStackView!
    @IBOutlet weak var volumeStackView: UIStackView!
    @IBOutlet weak var batteryLevelStackView: UIStackView!
    @IBOutlet weak var timeRemainingStackView: UIStackView!
    @IBOutlet weak var flowRateStackView: UIStackView!
    @IBOutlet weak var colorStackView: UIStackView!
    @IBOutlet weak var brightnessStackView: UIStackView!
    @IBOutlet weak var lineShatteringModeStackView: UIStackView!
    @IBOutlet weak var matrixStackView: UIStackView!
//    @IBOutlet weak var clottingSetupStackView: UIStackView!
    @IBOutlet weak var clottingPatternLoadStackView: UIStackView!
    @IBOutlet weak var lineShatteringCenterStackView: UIStackView!
    @IBOutlet weak var hrStackView: UIStackView!
    @IBOutlet weak var spO2StackView: UIStackView!
    @IBOutlet weak var artStackView: UIStackView!
    @IBOutlet weak var rrStackView: UIStackView!
    @IBOutlet weak var co2StackView: UIStackView!
    @IBOutlet weak var temperatureStackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Labels
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var manualStopLabel: UILabel!
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var barMessageLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var alarmLabel: UILabel!
    @IBOutlet weak var timeUnitLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var minimumVolumeLabel: UILabel!
    @IBOutlet weak var maximumVolumeLabel: UILabel!
    @IBOutlet weak var batteryLevelLabel: UILabel!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var minimumTimeRemainingLabel: UILabel!
    @IBOutlet weak var maximumTimeRemainingLabel: UILabel!
    @IBOutlet weak var timeRemainingUnitLabel: UILabel!
    @IBOutlet weak var flowRateLabel: UILabel!
    @IBOutlet weak var minimumFlowRateLabel: UILabel!
    @IBOutlet weak var maximumFlowRateLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var brightnessLabel: UILabel!
    @IBOutlet weak var minimumBrightnessLabel: UILabel!
    @IBOutlet weak var maximumBrightnessLabel: UILabel!
    @IBOutlet weak var matrixLabel: UILabel!
    @IBOutlet weak var lineShatteringModeLabel: UILabel!
    @IBOutlet weak var lineShatteringCenterLabel: UILabel!
    @IBOutlet weak var hrLabel: UILabel!
    @IBOutlet weak var spO2Label: UILabel!
    @IBOutlet weak var artLabel: UILabel!
    @IBOutlet weak var rrLabel: UILabel!
    @IBOutlet weak var co2Label: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    // Fields and Controls
    @IBOutlet weak var durationField: UITextField!
    @IBOutlet weak var manualStopSwitch: UISwitch!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var barMessageField: UITextField!
    @IBOutlet weak var prioritySegmentedControl: UISegmentedControl!
    @IBOutlet weak var alarmSwitch: UISwitch!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var addParallelModuleButton: ModuleButton!
    @IBOutlet weak var removeParallelModuleButton: ModuleButton!
    @IBOutlet weak var batteryLevelSegmentedControl: UISegmentedControl!
    @IBOutlet weak var timeRemainingSlider: UISlider!
    @IBOutlet weak var flowRateSlider: UISlider!
    @IBOutlet weak var colorWheel: HSBASpectum!
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var matrixCollectionView: UICollectionView!
    @IBOutlet weak var inverseClottingMatrixButton: ModuleButton!
    @IBOutlet weak var allOffClottingMatrixButton: ModuleButton!
    @IBOutlet weak var clottingSegmentedControl: UISegmentedControl!
    @IBOutlet weak var loadClottingPatternButton: ModuleButton!
    @IBOutlet weak var saveClottingPatternButton: ModuleButton!
    @IBOutlet weak var lineShatteringModeSegementedControl: UISegmentedControl!
    @IBOutlet weak var lineShatteringCenterSwitch: UISwitch!
    @IBOutlet weak var hrField: UITextField!
    @IBOutlet weak var spO2Field: UITextField!
    @IBOutlet weak var artField: UITextField!
    @IBOutlet weak var rrField: UITextField!
    @IBOutlet weak var co2Field: UITextField!
    @IBOutlet weak var temperatureField: UITextField!
    
    // Clotting Setup
    var clotMode = true
    var clotColorPickerPosition: CGPoint!
    var defaultColorPickerPosition: CGPoint!
    var clotBrightness: Float = 1
    
    // Meta
    var selectedModuleLabel: String?
    private var selectedModule: Module?
    var isSuccessFailureAction: Bool? = nil
    var editModuleIndex: Int?
    var parallelModuleParentIndex: Int?
    var delegate: PopoverDelegate?
    
    // MARK: INITIALIZATION
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // By default, hide Display Message controls
        messageStackView.isHidden = true
        barMessageStackView.isHidden = true
        priorityStackView.isHidden = true
        alarmStackView.isHidden = true
        volumeStackView.isHidden = true
        batteryLevelStackView.isHidden = true
        flowRateStackView.isHidden = true
        colorStackView.isHidden = true
        brightnessStackView.isHidden = true
        matrixStackView.isHidden = true
        timeRemainingStackView.isHidden = true
        clottingPatternLoadStackView.isHidden = true
        lineShatteringModeStackView.isHidden = true
        lineShatteringCenterStackView.isHidden = true
        matrixStackView.isHidden = true
        colorStackView.isHidden = true
        brightnessStackView.isHidden = true
        clottingPatternLoadStackView.isHidden = true
        hrStackView.isHidden = true
        rrStackView.isHidden = true
        artStackView.isHidden = true
        co2StackView.isHidden = true
        spO2StackView.isHidden = true
        temperatureStackView.isHidden = true
        
        // Setup volume slider
        volumeSlider.minimumValue = OxygenatorNoise.volumeRange.lowerBound
        volumeSlider.maximumValue = OxygenatorNoise.volumeRange.upperBound
        volumeSlider.tintColor = moduleButtonBackgroundColor
        
        // Change labels according to selected module
        if selectedModuleLabel != nil {
            self.title = "Configure " + selectedModuleLabel!
            switch selectedModuleLabel! {
            case "Delay":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? Module
            case "Wait for Action":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? WaitForAction
            case "Change Parameters":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? ChangeParameters
            case "Display Message":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? DisplayMessage
                messageStackView.isHidden = false
                barMessageStackView.isHidden = false
                priorityStackView.isHidden = false
                alarmStackView.isHidden = false
            case "Stop Simulation":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? StopSimulation
            case "Bleeding":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? Bleeding
                flowRateStackView.isHidden = false
                flowRateLabel.text = "Flow Rate \(Int(flowRateSlider.value)) " + flowRateUnit
            case "Oxygenation Failure":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? Deoxygenation
            case "Pump Noise":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? OxygenatorNoise
                volumeStackView.isHidden = false
                volumeSlider.value = OxygenatorNoise.volumeRange.upperBound
                volumeSliderValueDidChange(volumeSlider)
            case "Power Disconnection":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? PowerDisconnection
                batteryLevelStackView.isHidden = false
                timeRemainingStackView.isHidden = false
                timeRemainingSliderDidChange(timeRemainingSlider)
            case "Line Shattering":
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? LineShattering
                lineShatteringModeStackView.isHidden = false
                lineShatteringCenterStackView.isHidden = false
            case "Clotting":
                //clottingSetupStackView.isHidden = false
                matrixStackView.isHidden = false
                colorStackView.isHidden = false
                brightnessStackView.isHidden = false
                clottingPatternLoadStackView.isHidden = false
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? Clotting
            case "Vital Signs":
                hrStackView.isHidden = false
                rrStackView.isHidden = false
                artStackView.isHidden = false
                co2StackView.isHidden = false
                spO2StackView.isHidden = false
                temperatureStackView.isHidden = false
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? VitalSigns
            default:
                selectedModule = Module.moduleFromLabel(moduleLabel: selectedModuleLabel!) as? Module
            }
            
            moduleLabel.text = selectedModule?.label
            descriptionLabel.text = selectedModule?.descriptionString
            //moduleView.backgroundColor = UIColor(hex: (selectedModule?.tintColorString!)!)
            moduleView.backgroundColor = gradientFrom(color: UIColor(hex: (selectedModule?.tintColorString!)!), frame: self.view.frame)
            
            // Setup clotting matrix collection view
            matrixCollectionView.backgroundColor = moduleView.backgroundColor
            matrixCollectionView.allowsMultipleSelection = true
            matrixCollectionView.transform = matrixCollectionView.transform.rotated(by: clotMatrixCollectionViewRotationAngle)
            matrixCollectionView.transform = matrixCollectionView.transform.scaledBy(x: clotMatrixCollectionViewScaleFactor, y: clotMatrixCollectionViewScaleFactor)
            
            if selectedModule?.label == "Delay" {
                manualStopSwitch.onTintColor = altSwitchOnTintColor
            }
            
            // Clotting matrix buttons setup
            inverseClottingMatrixButton.setTitleColor(moduleButtonTextColor, for: .normal)
            inverseClottingMatrixButton.backgroundColor = moduleButtonBackgroundColor
            allOffClottingMatrixButton.setTitleColor(moduleButtonTextColor, for: .normal)
            allOffClottingMatrixButton.backgroundColor = moduleButtonBackgroundColor
            loadClottingPatternButton.backgroundColor = moduleButtonBackgroundColor
            loadClottingPatternButton.setTitleColor(moduleButtonTextColor, for: .normal)
            saveClottingPatternButton.backgroundColor = moduleButtonBackgroundColor
            saveClottingPatternButton.setTitleColor(moduleButtonTextColor, for: .normal)
            
            // Clotting segmented control setup
            clotMode = clottingSegmentedControl.selectedSegmentIndex == 0 ? true : false
            defaultColorPickerPosition = CGPoint(x: colorWheel.bounds.width, y: 0)
            clottingCellDeactivatedColor = UIColor.black
            
            // Setup brightness slider
            brightnessSlider.tintColor = moduleButtonBackgroundColor
            
            // Setup time remaining slider
            timeRemainingSlider.tintColor = moduleButtonBackgroundColor
            timeRemainingSlider.minimumValue = PowerDisconnection.timeRemainingRange.lowerBound
            timeRemainingSlider.maximumValue = PowerDisconnection.timeRemainingRange.upperBound
            minimumTimeRemainingLabel.text = String(Int(PowerDisconnection.timeRemainingRange.lowerBound))
            maximumTimeRemainingLabel.text = String(Int(PowerDisconnection.timeRemainingRange.upperBound))
            timeRemainingUnitLabel.text = "min"
            
            // Setup edit mode
            if editModuleIndex != nil {
                print("Setting up edit mode with index \(editModuleIndex)...")
                // Enable swipe back gesture
                navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                setupEditMode()
            } else {
                // Disable swipe back gesture
                navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                // Setup flow rate slider
                setupFlowRateSlider()
            }
            
            // Add parallel module button
            if isSuccessFailureAction == nil && selectedModule?.parallelModule == nil && editModuleIndex != nil && parallelModuleParentIndex == nil {
                addParallelModuleButton.isHidden = false
            } else {
                addParallelModuleButton.isHidden = true
            }
            
            // Show/hide remove parallel module button
            removeParallelModuleButton.backgroundColor = removeParallelModuleBackgroundColor
            if parallelModuleParentIndex != nil && editModuleIndex != nil && selectedModule?.label != "Delay" {
                removeParallelModuleButton.isHidden = false
            } else {
                removeParallelModuleButton.isHidden = true
            }
            
            // Hide duration controls in parallel module
            if parallelModuleParentIndex != nil {
                durationStackView.isHidden = true
                descriptionLabel.text = descriptionLabel.text! + "\n\nNote that the duration of this parallel module reflects its parent module."
            } else {
                durationStackView.isHidden = false
            }
            
            // Setup rounded corners
            moduleView.layer.cornerRadius = moduleConfigurationCornerRadius
            moduleView.layer.masksToBounds = true
            colorWheel.layer.cornerRadius = moduleConfigurationCornerRadius
            
            // Setup labels text color
            textColor = (UIColor.black.isContrasting(with: moduleView.backgroundColor!)) ? UIColor.black : UIColor.white
            
            fieldTextColor = textColor
            fieldBackgroundColor = UIColor(hex: (selectedModule?.tintColorString)!).add(hue: 0, saturation: -0.3, brightness: 0, alpha: 0)
            
            moduleLabel.textColor = textColor
            durationLabel.textColor = textColor
            manualStopLabel.textColor = textColor
            durationField.textColor = fieldTextColor
            messageLabel.textColor = textColor
            barMessageLabel.textColor = textColor
            messageField.textColor = fieldTextColor
            barMessageField.textColor = fieldTextColor
            priorityLabel.textColor = textColor
            alarmLabel.textColor = textColor
            timeUnitLabel.textColor = textColor
            descriptionLabel.textColor = textColor
            volumeLabel.textColor = textColor
            minimumVolumeLabel.textColor = textColor
            maximumVolumeLabel.textColor = textColor
            batteryLevelLabel.textColor = textColor
            timeRemainingLabel.textColor = textColor
            minimumTimeRemainingLabel.textColor = textColor
            maximumTimeRemainingLabel.textColor = textColor
            timeRemainingUnitLabel.textColor = textColor
            flowRateLabel.textColor = textColor
            minimumFlowRateLabel.textColor = textColor
            maximumFlowRateLabel.textColor = textColor
            colorLabel.textColor = textColor
            brightnessLabel.textColor = textColor
            minimumBrightnessLabel.textColor = textColor
            maximumBrightnessLabel.textColor = textColor
            matrixLabel.textColor = textColor
            lineShatteringModeLabel.textColor = textColor
            lineShatteringCenterLabel.textColor = textColor
            hrLabel.textColor = textColor
            rrLabel.textColor = textColor
            artLabel.textColor = textColor
            co2Label.textColor = textColor
            spO2Label.textColor = textColor
            temperatureLabel.textColor = textColor
            hrField.textColor = fieldTextColor
            rrField.textColor = fieldTextColor
            artField.textColor = fieldTextColor
            co2Field.textColor = fieldTextColor
            spO2Field.textColor = fieldTextColor
            temperatureField.textColor = fieldTextColor
            
            // Setup fields background color
            durationField.backgroundColor = fieldBackgroundColor
            messageField.backgroundColor = fieldBackgroundColor
            barMessageField.backgroundColor = fieldBackgroundColor
            hrField.backgroundColor = fieldBackgroundColor
            rrField.backgroundColor = fieldBackgroundColor
            artField.backgroundColor = fieldBackgroundColor
            co2Field.backgroundColor = fieldBackgroundColor
            spO2Field.backgroundColor = fieldBackgroundColor
            temperatureField.backgroundColor = fieldBackgroundColor
            
            // Setup field placeholder labels
            let durationPlaceholder = NSAttributedString(string: "enter time", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let messagePlaceholder = NSAttributedString(string: "enter message", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let hrPlaceholder = NSAttributedString(string: "enter HR", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let spO2Placeholder = NSAttributedString(string: "enter SPO2", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let rrPlaceholder = NSAttributedString(string: "enter RR", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let artPlaceholder = NSAttributedString(string: "enter ART", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let co2Placeholder = NSAttributedString(string: "enter CO2", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            let temperaturePlaceholder = NSAttributedString(string: "enter temperature", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
            durationField.attributedPlaceholder = durationPlaceholder
            messageField.attributedPlaceholder = messagePlaceholder
            barMessageField.attributedPlaceholder = messagePlaceholder
            hrField.attributedPlaceholder = hrPlaceholder
            spO2Field.attributedPlaceholder = spO2Placeholder
            artField.attributedPlaceholder = artPlaceholder
            rrField.attributedPlaceholder = rrPlaceholder
            co2Field.attributedPlaceholder = co2Placeholder
            temperatureField.attributedPlaceholder = temperaturePlaceholder
            
            // Add button setup
            addButton.setTitleColor(moduleButtonTextColor, for: .normal)
            addButton.backgroundColor = moduleButtonBackgroundColor
            
            // Add done buttons to keyboards
            addDoneButtonOnKeyboard(durationField, target: self, action: #selector(ModuleConfigureViewController.doneButtonAction))
            addDoneButtonOnKeyboard(messageField, target: self, action: #selector(ModuleConfigureViewController.doneButtonAction))
            addDoneButtonOnKeyboard(barMessageField, target: self, action: #selector(ModuleConfigureViewController.doneButtonAction))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Timer.after(0.1.seconds, {
            let scrollViewHeight: CGFloat = CGFloat(self.masterStackView.bounds.height + 50)
            self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: scrollViewHeight)
            self.preferredContentSize = self.scrollView.contentSize
            
            // Storing clotting color picker position
            if self.editModuleIndex == nil {
                self.colorWheel.position = CGPoint(x: self.colorWheel.bounds.midX, y: 15)
                self.clotColorPickerPosition = self.colorWheel.position
                clottingCellActivatedColor = self.colorWheel.color
            }
        })
    }

    // MARK: CONTROL ACTIONS
    @IBAction func stopSwitchDidChange(_ sender: UISwitch) {
        let color = !sender.isOn ? textColor : disabledFieldColor
        let fieldColor = !sender.isOn ? fieldTextColor : disabledFieldColor

        let alpha: CGFloat = !sender.isOn ? 1 : disabledFieldAlpha
        durationField.isEnabled = !sender.isOn
        
//        if selectedModuleLabel != "Delay" {
//            manualStopLabel.textColor = !sender.isOn ? textColor : UIColor(hex: "#00D871")
//        }
        
        durationLabel.textColor = color
        durationField.textColor = fieldColor
        timeUnitLabel.textColor = color
        
        durationLabel.alpha = alpha
        durationField.alpha = alpha
        timeUnitLabel.alpha = alpha
    }
    
    @IBAction func timeRemainingSliderDidChange(_ slider: UISlider) {
        timeRemainingLabel.text = "Time Remaining \(Int(slider.value))"
    }
    
    @IBAction func volumeSliderValueDidChange(_ slider: UISlider) {
        volumeLabel.text = "Sound Level \(Int(slider.value / OxygenatorNoise.volumeRange.upperBound * 100))%"
    }
    
    @IBAction func flowRateSliderValueDidChange(_ slider: UISlider) {
        flowRateLabel.text = "Flow Rate \(String(format: Vformat, slider.value)) " + flowRateUnit
    }
    
    @objc func doneButtonAction() {
        self.durationField.resignFirstResponder()
        self.messageField.resignFirstResponder()
        self.barMessageField.resignFirstResponder()
    }
    
    // MARK: CLOTTING COLOR PICKER
    @IBAction func colorWheelValueDidChange(_ sender: HSBASpectum) {
        
        if clotMode {
            clotColorPickerPosition = sender.position
            clottingCellActivatedColor = sender.color
            
            if let selectedIndexPaths = matrixCollectionView.indexPathsForSelectedItems {
                for indexPath in selectedIndexPaths {
                    collectionView(matrixCollectionView, didSelectItemAt: indexPath)
                }
            }
        } else {
            defaultColorPickerPosition = sender.position
            clottingCellDeactivatedColor = sender.color
            
            for index in 0...matrixCollectionView.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(row: index, section: 0)
                guard let selectedIndexPaths = matrixCollectionView.indexPathsForSelectedItems
                else { return }
                
                if !selectedIndexPaths.contains(indexPath) {
                    collectionView(self.matrixCollectionView, didDeselectItemAt: indexPath)
                }
            }
        }
    }
    
    @IBAction func colorWheelTouchDown(_ sender: Any) {
        print("Selecting color drag did start.")
        self.scrollView.isScrollEnabled = false
    }
    
    @IBAction func colorWheelTouchUpInside(_ sender: Any) {
        print("Selecting color drag did end.")
        self.scrollView.isScrollEnabled = true
    }
    
    @IBAction func turnOffClottingMatrix(_ sender: Any) {
        matrixCollectionView.reloadData()
    }
    
    func lightClottingMatrix(from LEDMatrixString: String) {
        let LEDMatrix = Clotting.LEDMatrix(from: LEDMatrixString)
        for (index, item) in LEDMatrix.enumerated() {
            if item == 1 {
                let indexPath = IndexPath(row: index, section: 0)
                self.matrixCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.init(rawValue: 0))
                self.collectionView(self.matrixCollectionView, didSelectItemAt: indexPath)
            }
        }
    }
    
    @IBAction func inverseClottingMatrix(_ sender: Any) {
        let inverseLEDMatrix = Clotting.LEDMatrix(from: matrixCollectionView, inverse: true)
        matrixCollectionView.reloadData()
        lightClottingMatrix(from: Clotting.LEDMatrixString(from: inverseLEDMatrix))
    }
    
    @IBAction func switchClottingMode(_ sender: UISegmentedControl) {
        // Switches matrix color selection from clotting to default color and vice versa
        clotMode = sender.selectedSegmentIndex == 0 ? true : false
       anim {
            self.brightnessSlider.value = self.clotBrightness
            self.brightnesseSliderValueDidChange(self.brightnessSlider)
            self.colorWheel.position = self.clotMode ? self.clotColorPickerPosition : self.defaultColorPickerPosition
            self.colorWheelValueDidChange(self.colorWheel)
        }
    }
    
    // MARK: CLOTTING PATTERN LOAD/SAVE
    @IBAction func saveClottingPattern(_ sender: Any) {
        try! realm.write {
            updateClotting(currentClotting)
            realm.add(currentClotting, update: .all)
        }
    }
    
    func updateClotting(_ clotting: Clotting) {
        // Clot color
        colorWheel.position = clotColorPickerPosition
        clotting.red = Int(colorWheel.color.redComponent * 255)
        clotting.green = Int(colorWheel.color.greenComponent * 255)
        clotting.blue = Int(colorWheel.color.blueComponent * 255)
        clotting.brightness = Int(clotBrightness * 255)
        clotting.normalizedColorSpectrumX = Float(clotColorPickerPosition.x / self.colorWheel.bounds.width)
        clotting.normalizedColorSpectrumY = Float(clotColorPickerPosition.y / self.colorWheel.bounds.height)
        
        // Default color
        colorWheel.position = defaultColorPickerPosition
        clotting.defaultRed = Int(colorWheel.color.redComponent * 255)
        clotting.defaultGreen = Int(colorWheel.color.greenComponent * 255)
        clotting.defaultBlue = Int(colorWheel.color.blueComponent * 255)
        clotting.defaultNormalizedColorSpectrumX = Float(defaultColorPickerPosition.x / self.colorWheel.bounds.width)
        clotting.defaultNormalizedColorSpectrumY = Float(defaultColorPickerPosition.y / self.colorWheel.bounds.height)
        
        clotting.LEDMatrix = Clotting.LEDMatrixString(from: Clotting.LEDMatrix(from: matrixCollectionView))
    }
    
    func applyClotting(from clotting: Clotting) {
        debugPrint(clotting)
        matrixCollectionView.reloadData()
        
        let x = CGFloat(clotting.normalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
        let y = CGFloat(clotting.normalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
        
        let defaultX = CGFloat(clotting.defaultNormalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
        let defaultY = CGFloat(clotting.defaultNormalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
        
        clotBrightness = Float(clotting.brightness) / 255.0
        
        clotColorPickerPosition = CGPoint(x: x, y: y)
        defaultColorPickerPosition = CGPoint(x: defaultX, y: defaultY)
        
        clottingCellActivatedColor = UIColor(red: CGFloat(clotting.red) / 255, green: CGFloat(clotting.green) / 255, blue: CGFloat(clotting.blue) / 255, alpha: 1)
        clottingCellDeactivatedColor = UIColor(red: CGFloat(clotting.defaultRed) / 255, green: CGFloat(clotting.defaultGreen) / 255, blue: CGFloat(clotting.defaultBlue) / 255, alpha: 1)
        lightClottingMatrix(from: clotting.LEDMatrix)
        colorWheel.position = clottingSegmentedControl.selectedSegmentIndex == 0 ? clotColorPickerPosition : defaultColorPickerPosition
        
        brightnessSlider.value = clotBrightness
        brightnesseSliderValueDidChange(brightnessSlider)
    }
    
    // MARK: MATRIX COLLECTION VIEW DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfClottingMatrixCells
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clottingCell", for: indexPath)
        cell.layer.cornerRadius = clottingCellCornerRadius
        cell.layer.masksToBounds = true
        cell.backgroundColor = cell.isSelected ? clottingCellActivatedColor : clottingCellDeactivatedColor
        return cell
    }

    // MARK: MATRIX COLLECTION VIEW DELEGATE
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.backgroundColor = clottingCellActivatedColor
        }
        print("Selected \(indexPath.row)")
        print(Clotting.LEDMatrix(from: collectionView, inverse: false))
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.backgroundColor = clottingCellDeactivatedColor
        }
        print(Clotting.LEDMatrix(from: collectionView, inverse: false))
    }
    
    @IBAction func brightnesseSliderValueDidChange(_ slider: UISlider) {
        clotBrightness = slider.value
        brightnessLabel.text = "Brightness \(Int(slider.value * 100))%"
    }
    
    // MARK: NAVIGATION
    @IBAction func addModule(_ sender: Any) {
        if isSuccessFailureAction == nil {
            self.performSegue(withIdentifier: "addToSequence", sender: self)
        } else {
            self.performSegue(withIdentifier: "addToWaitForAction", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addParallelModule" {
            if let destination = segue.destination as? ModuleCollectionViewController {
                destination.parallelModuleParentIndex = editModuleIndex
        }
        } else if segue.identifier == "loadClottingMatrixPatternsFromModuleConfiguration" {
            if let destination = segue.destination as? ClottingPatternViewController {
                destination.delegate = self
            }
        } else {
            print("Adding selected module to sequence...")
            // Setting duration
            if !manualStopSwitch.isOn {
                let duration = Float(durationField.text!)
                try! realm.write {
                    selectedModule?.duration.value = duration
                }
            } else {
                try! realm.write {
                    selectedModule?.duration.value = nil
                }
            }
            
            // Other parameters
            switch selectedModuleLabel! {
            case "Display Message":
                try! realm.write {
                    (selectedModule as? DisplayMessage)?.message = !(messageField.text?.isEmpty)! ? messageField.text : "nil"
                    (selectedModule as? DisplayMessage)?.barMessage = !(barMessageField.text?.isEmpty)! ? barMessageField.text : "nil"
                    (selectedModule as? DisplayMessage)?.alarm = alarmSwitch.isOn
                    (selectedModule as? DisplayMessage)?.priority = prioritySegmentedControl.selectedSegmentIndex + 1
                }
            case "Bleeding":
                try! realm.write {
                    (selectedModule as? Bleeding)?.flowRate = flowRateSlider.value
                }
            case "Pump Noise":
                try! realm.write {
                    (selectedModule as? OxygenatorNoise)?.volume = volumeSlider.value
                }
            case "Power Disconnection":
                try! realm.write {
                    let batteryLevel =  PowerDisconnection.getBatteryLevel(index: batteryLevelSegmentedControl.selectedSegmentIndex)
                    (selectedModule as? PowerDisconnection)?.batteryLevel.value = batteryLevel
                    (selectedModule as? PowerDisconnection)?.fullBatteryLife = batteryLevel != 0 ? Int(timeRemainingSlider.value / batteryLevel) : 0
                }
            case "Line Shattering":
                try! realm.write {
                    (selectedModule as? LineShattering)?.mode = lineShatteringModeSegementedControl.selectedSegmentIndex + 2
                    (selectedModule as? LineShattering)?.center = lineShatteringCenterSwitch.isOn
                }
            case "Clotting":
                try! realm.write {
                    updateClotting(selectedModule as! Clotting)
                    if currentClotting.LEDMatrix == (selectedModule as! Clotting).LEDMatrix {
                        (selectedModule as! Clotting).showInLoadList = false
                    }
                }
            case "Vital Signs":
                try! realm.write {
                    (selectedModule as! VitalSigns).respiratoryRate = Float(rrField.text!) != nil ? Float(rrField.text!)! : 0
                    (selectedModule as! VitalSigns).heartRate = Float(hrField.text!) != nil ? Float(hrField.text!)! : 0
                    (selectedModule as! VitalSigns).co2 = Float(co2Field.text!) != nil ? Float(co2Field.text!)! : 0
                    (selectedModule as! VitalSigns).bloodPressure = artField.text!
                    (selectedModule as! VitalSigns).spO2 = Float(spO2Field.text!) != nil ? Float(spO2Field.text!)! : 0
                    (selectedModule as! VitalSigns).temperature = Float(temperatureField.text!) != nil ? Float(temperatureField.text!)! : 0
                }
            default:
                break
            }
            
            // Adding to queue
            if editModuleIndex == nil {
                if parallelModuleParentIndex != nil {
                    mainQueue.updateParallelModuleAt(index: parallelModuleParentIndex!, module: selectedModule!)
                } else {
                    switch isSuccessFailureAction {
                    case nil:
                        mainQueue.addToQueue(module: selectedModule!)
                    case true?:
                        mainQueue.addToSuccessQueue(module: selectedModule!, waitForAction: currentWaitForAction)
                    case false?:
                        mainQueue.addToFailureQueue(module: selectedModule!, waitForAction: currentWaitForAction)
                    }
                }
                
                if let destination = segue.destination as? SequenceCollectionViewController {
                    destination.justAdded = true
                } else if let destination = segue.destination as? WaitForActionViewController {
                    destination.justAdded = true
                }
            } else {
                if parallelModuleParentIndex != nil {
                    mainQueue.updateParallelModuleAt(index: parallelModuleParentIndex!, module: selectedModule!)
                } else {
                    switch isSuccessFailureAction {
                    case nil:
                        mainQueue.updateModuleAt(index: editModuleIndex!, module: selectedModule!)
                    case true?, false?:
                        mainQueue.updateModuleAt(index: editModuleIndex!, module: selectedModule!, fromSuccessFailure: isSuccessFailureAction!, waitForAction: currentWaitForAction)
                    }
                }
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        print("Removing keyboard observers...")
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @IBAction func removeParallelModule(_ sender: Any) {
        print("Removing parallel module of parent module \(parallelModuleParentIndex)...")
        if parallelModuleParentIndex != nil {
            mainQueue.deleteParallelModuleAt(parentModuleIndex: parallelModuleParentIndex!)
        }
        self.dismiss(animated: true) {
            self.delegate?.popoverDismissed()
        }
    }
    
    // MARK: POPOVER DELEGATE
    func popoverDismissed() {
        applyClotting(from: currentClotting)
    }
    
    // MARK: EDIT MODE
    func setupEditMode() {
        addButton.setTitle("Update", for: .normal)
        
        if selectedModuleLabel != nil {
            switch isSuccessFailureAction {
            case nil:
                if parallelModuleParentIndex != nil {
                   selectedModule = mainQueue.parallelModuleAt(parentModuleIndex: editModuleIndex!)
                } else {
                    selectedModule = mainQueue.moduleAt(index: editModuleIndex!)
                }
            case true?, false?:
                selectedModule = mainQueue.moduleAt(index: editModuleIndex!, fromSuccessFailure: isSuccessFailureAction, waitForAction: currentWaitForAction)
            }
            
            if selectedModule != nil {
                switch selectedModuleLabel! {
                case "Delay", "Oxygenation Failure":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                case "Line Shattering":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    if let mode = (selectedModule as? LineShattering)?.mode {
                        lineShatteringModeSegementedControl.selectedSegmentIndex = mode - 2
                    }
                    if let center = (selectedModule as? LineShattering)?.center {
                        lineShatteringCenterSwitch.isOn = center
                    }
                case "Bleeding":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    if let flowRate = (selectedModule as? Bleeding)?.flowRate {
                        // Setup flow rate slider
                        setupFlowRateSlider(flowRate)
                    }
                case "Display Message":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    if selectedModule != nil {
                        messageField.text = (selectedModule as! DisplayMessage).message != "nil" ? (selectedModule as! DisplayMessage).message : nil
                        barMessageField.text = (selectedModule as! DisplayMessage).barMessage != "nil" ? (selectedModule as! DisplayMessage).barMessage : nil
                        alarmSwitch.isOn = (selectedModule as! DisplayMessage).alarm
                        prioritySegmentedControl.selectedSegmentIndex = (selectedModule as! DisplayMessage).priority - 1
                    }
                case "Pump Noise":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    if let volume = (selectedModule as? OxygenatorNoise)?.volume {
                        volumeSlider.value = volume
                        volumeLabel.text = "Sound Level \(Int(volumeSlider.value / OxygenatorNoise.volumeRange.upperBound * 100))%"
                    }
                case "Power Disconnection":
                    if let duration = selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    if let batteryLevel = (selectedModule as? PowerDisconnection)?.batteryLevel.value {
                        batteryLevelSegmentedControl.selectedSegmentIndex = PowerDisconnection.getIndexFrom(value: batteryLevel)
                        if let fullPowerLife = (selectedModule as? PowerDisconnection)?.fullBatteryLife {
                            timeRemainingSlider.value = batteryLevel * Float(fullPowerLife)
                            timeRemainingSliderDidChange(timeRemainingSlider)
                        }
                    }
                case "Clotting":
                    if let duration = self.selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    
                    if let clotting = (self.selectedModule as? Clotting) {
                        clotBrightness = Float(clotting.brightness) / 255
                        brightnessSlider.value = clotBrightness
                        self.brightnesseSliderValueDidChange(brightnessSlider)
                        
                        Timer.after(0.1, {
                            let x = CGFloat(clotting.normalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
                            let y = CGFloat(clotting.normalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
                            
                            let defaultX = CGFloat(clotting.defaultNormalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
                            let defaultY = CGFloat(clotting.defaultNormalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
                            
                            
                            //UIColor.init(red: CGFloat(clotting.red) / 255, green: CGFloat(clotting.green) / 255, blue: CGFloat(clotting.blue) / 255, alpha: 1)
                            
                            self.defaultColorPickerPosition = CGPoint(x: defaultX, y: defaultY)
                            self.colorWheel.position = self.defaultColorPickerPosition
                            clottingCellDeactivatedColor = self.colorWheel.color
                            self.matrixCollectionView.reloadData()
                            
                            self.colorWheel.position = CGPoint(x: x, y: y)
                            self.clotColorPickerPosition = self.colorWheel.position
                            self.colorWheelValueDidChange(self.colorWheel)
                            
                            self.lightClottingMatrix(from: clotting.LEDMatrix)
                        })
                    }
                case "Vital Signs":
                    if let duration = self.selectedModule?.duration.value {
                        durationField.text = String(describing: duration)
                    } else {
                        manualStopSwitch.isOn = true
                        self.stopSwitchDidChange(manualStopSwitch)
                    }
                    
                    if let vitalSigns = (self.selectedModule as? VitalSigns) {
                        hrField.text = String(vitalSigns.heartRate)
                        rrField.text = String(vitalSigns.respiratoryRate)
                        spO2Field.text = String(vitalSigns.spO2)
                        artField.text = String(vitalSigns.bloodPressure)
                        co2Field.text = String(vitalSigns.co2)
                        temperatureField.text = String(vitalSigns.temperature)
                    }
                default:
                    break
                }
            }
        }
        debugPrint("Module to edit:")
        debugPrint(selectedModule as Any)
    }
    
    // MARK: UTILITIES
    func setupFlowRateSlider(_ flowRate: Float? = nil) {
        // Tint color
        flowRateSlider.tintColor = moduleButtonBackgroundColor
        
        // Retrieve Max/min values from cloud
        Bleeding.getMinimumMaximumBleedFlowRate(completion: {
            // Set slider max/min
            self.flowRateSlider.minimumValue = minimumFlowRate.floatValue
            self.flowRateSlider.maximumValue = maximumFlowRate.floatValue
            if flowRate != nil {
                self.flowRateSlider.value = flowRate!
            } else {
                self.flowRateSlider.value = self.flowRateSlider.maximumValue
            }
            self.flowRateSliderValueDidChange(self.flowRateSlider)
        })
        
        // Set labels
        self.flowRateSliderValueDidChange(self.flowRateSlider)
        self.minimumFlowRateLabel.text = String(format: Vformat, minimumFlowRate.floatValue)
        self.maximumFlowRateLabel.text = String(format: Vformat, maximumFlowRate.floatValue)
        
        print("Min flow rate: \(minimumFlowRate)")
        print("Max flow rate: \(maximumFlowRate)")
    }
    
    func createFieldWith(labelString: String, placeholderString: String) -> UIStackView {
        let label = UILabel()
        label.text = labelString
        label.textColor = textColor
        let field = UITextField()
        field.placeholder = placeholderString
        field.textColor = textColor
        let stackView = UIStackView(arrangedSubviews: [label, field])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 10
        
        return stackView
    }
}

protocol PopoverDelegate {
    func popoverDismissed()
}
