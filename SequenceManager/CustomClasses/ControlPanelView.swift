//
//  ControlPanelView.swift
//  SequenceManager
//
//  Created by Abdol on 4/14/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class ControlPanelView: UIView {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    
    override required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = moduleCornerRadius
        self.layer.masksToBounds = true
        //        self.layer.shadowColor = UIColor.lightGray.cgColor
        //        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        //        self.layer.shadowRadius = moduleShadowRadius
        //        self.layer.shadowOpacity = moduleShadowOpacity
        //        self.layer.masksToBounds = false;
        //        self.layer.shadowPath = UIBezierPath(roundedRect: self.frame, cornerRadius: moduleCornerRadius).cgPath
        //        self.layer.cornerRadius = moduleConfigurationCornerRadius
    }
    
}
