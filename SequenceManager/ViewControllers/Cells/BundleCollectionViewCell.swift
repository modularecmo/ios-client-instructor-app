//
//  BundleModuleCollectionViewCell.swift
//  
//
//  Created by Abdol on 5/12/17.
//
//

import UIKit

class BundleCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var stackView: UIStackView!
    
    // First Module
    @IBOutlet weak var firstModuleView: UIView!
    @IBOutlet weak var firstModuleLabel: UILabel!
    @IBOutlet weak var firstDetailLabel: UILabel!
    
    // Second Module
    @IBOutlet weak var secondModuleView: UIView!
    @IBOutlet weak var secondModuleLabel: UILabel!
    @IBOutlet weak var secondDetailLabel: UILabel!
    @IBOutlet weak var secondModuleWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var reorderButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.clipsToBounds = true
        self.layer.cornerRadius = moduleCornerRadius
    }

}
