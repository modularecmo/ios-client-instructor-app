//
//  RunningNavigationController.swift
//  SequenceManager
//
//  Created by Abdol on 3/26/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class RunningNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
