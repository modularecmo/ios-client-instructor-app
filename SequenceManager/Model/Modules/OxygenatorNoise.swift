//
//  OxygenatorNoise.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class OxygenatorNoise: Module {
    @objc dynamic var volume: Float = 30
    static var volumeRange: ClosedRange<Float> = 0...30
    
    required override init() {
        super.init()
        self.label = "Pump Noise"
        self.name = "oxygenatorNoise"
        self.descriptionString = "Simulate air in oxygenator by introducing noise for a specified period (in seconds) with specified sound level."
//        self.tintColorString = "#F2994A"
        self.tintColorString = "#F265B0"
    }
    
//    override static func ignoredProperties() -> [String] {
//        return ["volumeRange"]
//    }

    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["volume": Int(volume), "duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
