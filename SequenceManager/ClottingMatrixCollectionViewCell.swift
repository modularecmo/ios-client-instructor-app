//
//  ClottingMatrixCollectionViewCell.swift
//  ClottingPatterns
//
//  Created by Abdol on 7/26/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import RealmSwift

class ClottingMatrixCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @objc var clottingID: String!
    @objc var clotting: Clotting!
    @IBOutlet weak var matrixCollectionView: UICollectionView!
    
    @objc var clottingActivatedColor: UIColor!
    @objc var clottingDeactivatedColor: UIColor!
    @objc var LEDMatrix: [Int]!
    @IBOutlet weak var deletePatternButton: ModuleButton!
    
    override func layoutSubviews() {
        // Setup clotting matrix collection view
        clotting = realm.object(ofType: Clotting.self, forPrimaryKey: clottingID)
        matrixCollectionView.backgroundColor = UIColor(hex: clotting.tintColorString!)
        self.clottingActivatedColor = UIColor(red: CGFloat(self.clotting.red) / 255, green: CGFloat(self.clotting.green) / 255, blue: CGFloat(self.clotting.blue) / 255, alpha: 1)
        self.clottingDeactivatedColor = UIColor(red: CGFloat(self.clotting.defaultRed) / 255, green: CGFloat(self.clotting.defaultGreen) / 255, blue: CGFloat(self.clotting.defaultBlue) / 255, alpha: 1)
        
      //Timer.after(0.0001, {
        
            //self.lightClottingMatrix(from: self.clotting.LEDMatrix)
            self.matrixCollectionView.allowsMultipleSelection = true
            self.matrixCollectionView.transform = CGAffineTransform(rotationAngle: clotMatrixCollectionViewRotationAngle) //self.matrixCollectionView.transform.rotated(by: clotMatrixCollectionViewRotationAngle)
            self.matrixCollectionView.transform =  self.matrixCollectionView.transform.scaledBy(x: clotMatrixCollectionViewScaleFactor, y: clotMatrixCollectionViewScaleFactor)

     // })
    }
    
    // MARK: MATRIX COLLECTION VIEW DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfClottingMatrixCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clottingCell", for: indexPath)
        cell.layer.cornerRadius = clottingCellCornerRadius
        cell.layer.masksToBounds = true
        cell.backgroundColor = clottingDeactivatedColor
        if LEDMatrix[indexPath.row] == 1 {
            cell.backgroundColor = clottingActivatedColor
        }
//        cell.backgroundColor = UIColor.clear
//        cell.backgroundColor = cell.isSelected ? clottingActivatedColor : clottingDeactivatedColor
//        cell.backgroundColor = UIColor.black
        return cell
    }
    
//    func lightClottingMatrix(from LEDMatrixString: String) {
//        let LEDMatrix = Clotting.LEDMatrix(from: LEDMatrixString)
//        for (index, item) in LEDMatrix.enumerated() {
//            let indexPath = IndexPath(row: index, section: 0)
//            if item == 1 {
//                self.matrixCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionViewScrollPosition.init(rawValue: 0))
//                self.collectionView(self.matrixCollectionView, didSelectItemAt: indexPath)
//            }
//        }
//        
//        for index in 0...matrixCollectionView.numberOfItems(inSection: 0) {
//            let indexPath = IndexPath(row: index, section: 0)
//            guard let selectedIndexPaths = matrixCollectionView.indexPathsForSelectedItems
//                else { return }
//            
//            if !selectedIndexPaths.contains(indexPath) {
//                collectionView(self.matrixCollectionView, didDeselectItemAt: indexPath)
//            }
//        }
//    }
}
