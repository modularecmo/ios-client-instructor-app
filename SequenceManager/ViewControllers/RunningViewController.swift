//
//  RunningViewController.swift
//  SequenceManager
//
//  Created by Abdol on 3/19/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Firebase
import SwiftyTimer
//import Dance
import anim


private let reuseIdentifier = "blockCell"
private let waitForActionReuseIdentifier = "waitForActionCell"
public var isSequenceRunning = false

class RunningViewController: UIViewController, UICollectionViewDataSource {

    @IBOutlet weak var sequenceCollectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var stopButton: UIBarButtonItem!
    
    // Current Job Index and Running Flag
    var currentJobIndex = 0
    var currentWaitForActionJobIndex = -1
    var currentWaitForActionModule: Module?
    var successFailureTriggered: Bool? = nil
    var liveMode = true
    
    // Duration Timer
    var timer = Timer()
    var startTime = TimeInterval()
    
    // Firebase Observer Handles
    internal var connectingObserverHandle: DatabaseHandle!
    internal var sequenceObserverHandle: DatabaseHandle!
    
    // MARK: INITIALIZATION
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let parameterControlVC = self.splitViewController?.viewControllers.first as? ParameterControlViewController {
            let minimumWidth = min((self.splitViewController?.view.bounds.size.width)!,(self.splitViewController?.view.bounds.height)!)
            parameterControlVC.minimumPrimaryColumnWidth = minimumWidth / splitViewWidthConstant
        }
        
        // Reset indecies
        currentJobIndex = 0
        currentWaitForActionJobIndex = -1
        if connectingObserverHandle != nil {
            databaseReference.removeObserver(withHandle: connectingObserverHandle)
        }
        if sequenceObserverHandle != nil {
            databaseReference.removeObserver(withHandle: sequenceObserverHandle)
        }
        
        // Listen to current job index change
        connectAndStart()
        
        // Set collection view item size
        collectionViewLayout.itemSize = CGSize(width: runningModuleCellHeight, height: runningModuleCellWidth)
        
        // Set background colors
        self.view.backgroundColor = runningBackgroundColor
        sequenceCollectionView.backgroundColor = self.view.backgroundColor
        
        // Change navigation bar tint and text color
        self.navigationController?.navigationBar.barTintColor = connectingTabBarBackgroundColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: runningTextColor]
        navigationController?.navigationBar.tintColor = runningTextColor
        
        // Hide back button
        self.navigationItem.setHidesBackButton(true, animated: false);
        
        // Check internet connection 
        checkInternetConnection(target: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let lastIndex = mainQueue.array().count - 1
        if let lastModule = (mainQueue.array() as! [Module])[lastIndex] as? Module {
            if lastModule.label != "Stop Simulation" {
                print("No stop simulation module found. Adding...")
                mainQueue.addToQueue(module: StopSimulation())
            }
        }
        
        // Run sequence
        mainQueue.runMainQueue()
        sequenceCollectionView.reloadData()
        
        // Configure split view master view width
        anim { settings -> (animClosure) in
            settings.duration = 0.4
            return {
                if self.liveMode {
                    self.splitViewController?.preferredDisplayMode = .allVisible
                    self.splitViewController?.preferredPrimaryColumnWidthFraction = splitViewPreferredPrimaryColumnWidthFraction
                    let minimumWidth = min((self.splitViewController?.view.bounds.size.width)!,(self.splitViewController?.view.bounds.height)!)
                    self.splitViewController?.minimumPrimaryColumnWidth = minimumWidth / splitViewWidthConstant
                    self.splitViewController?.maximumPrimaryColumnWidth = minimumWidth / splitViewWidthConstant
                    
                    /* if let leftNavController = self.splitViewController?.viewControllers.first as? RunningViewController {
                        leftNavController.view.frame = CGRect(x: leftNavController.view.frame.origin.x, y: leftNavController.view.frame.origin.y, width: (minimumWidth / splitViewWidthConstant), height: leftNavController.view.frame.height)
                    }*/
                    
                    if let rightNavController = self.splitViewController?.viewControllers.first as? ParameterControlViewController {
                        rightNavController.collapsed = false
                        rightNavController.setupDatabase()
                    }
                    
                    // Set collection view section inset
                    self.collectionViewLayout.sectionInset = UIEdgeInsets(top: 30, left: self.sequenceCollectionView.frame.width / 3, bottom: 30, right: self.sequenceCollectionView.frame.width / 3)
                }
            }
        }
    }
    
    // MARK: DATABASE
    func connectAndStart() {
        connectingObserverHandle = databaseReference.observe(DataEventType.value, with: { (snapshot) in
            self.title = "Connecting..."
            self.sequenceCollectionView.isUserInteractionEnabled = false
            
            /*self.sequenceCollectionView.dance.animate(duration: 0.3, curve: .linear, { (dance) in
                self.sequenceCollectionView.alpha = disabledModuleAlpha
            })
            self.sequenceCollectionView.dance.start()*/
            
            anim { (settings) -> (animClosure) in
                settings.duration = 0.3
                return {
                    self.sequenceCollectionView.alpha = disabledModuleAlpha
                }
            }
            
            if let database = snapshot.value as? NSDictionary {
                guard let sequence = database["sequence"] as? [String:Any]
                    else {
                        print("No current current sequence.")
                        return
                    }
                if let connected = sequence["connected"] as? Bool {
                    if connected == true {
                        // Setup view title
                        self.title = "Running Scenario"
                        self.sequenceCollectionView.isUserInteractionEnabled = true
                        self.sequenceCollectionView.reloadData()
                        isSequenceRunning = true
                        
                        // Hide tab bar when running a sequence
                        self.hideTabBar()
                        
                        /*self.view.dance.animate(duration: 1.5, curve: .linear, { (dance) in
                            self.navigationController?.navigationBar.barTintColor = runningTabBarBackgroundColor
                            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: runningTextColor]
                            self.navigationController?.navigationBar.tintColor = runningTextColor
                            self.sequenceCollectionView.alpha = 1.0
                        })
                        self.view.dance.start()*/
                        
                        anim {
                            self.navigationController?.navigationBar.barTintColor = runningTabBarBackgroundColor
                            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: runningTextColor]
                            self.navigationController?.navigationBar.tintColor = runningTextColor
                            self.sequenceCollectionView.alpha = 1.0
                        }
                        
                        
                        // Setup duration counter
                        self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(RunningViewController.updateTime), userInfo: nil, repeats: true)
                        self.startTime = NSDate.timeIntervalSinceReferenceDate
                        databaseReference.removeObserver(withHandle: self.connectingObserverHandle)
                        self.listenToCurrentJobIndex()
                    }
                }
            }
        })
    }
    
    func listenToCurrentJobIndex(waitForActionIndex: Int? = nil, successFailure: Bool? = nil) {
        sequenceObserverHandle = databaseReference.observe(DataEventType.value, with: { (snapshot) in
            if let database = snapshot.value as? NSDictionary {
                guard let sequence = database["sequence"] as? [String:Any]
                    else {
                        print("No current current sequence.")
                        return
                    }
                
                if waitForActionIndex == nil {
                    if let index = sequence["currentJobIndex"] as? Int {
                        print("currentJobIndex:")
                        print(index)
                        
                        if self.currentJobIndex != index {
                            self.currentJobIndex = index
                            
                            // Reset wait for action current module and index
                            self.successFailureTriggered = nil
                            self.currentWaitForActionJobIndex = -1
                            self.currentWaitForActionModule = nil
                            self.updateCollectionView()
                        }
                    }
                    
                    if let running = sequence["running"] as? Bool, let connected = sequence["connected"] as? Bool {
                        isSequenceRunning = running
                        if !isSequenceRunning && connected && self.currentJobIndex != 0 {
                            self.simulationDidStop()
                        }
                    }
                } else {
                    print("Listening to wait for action queue changes...")
                    debugPrint(sequence)
                    if waitForActionIndex != nil {
                        guard let sequence = sequence["main"] as? [Any]
                            else { return }
                        if waitForActionIndex! < sequence.count && waitForActionIndex! == self.currentJobIndex {
                            if let currentJob = sequence[waitForActionIndex!] as? [String: Any] {
                                print("Wait for action current job:")
                                debugPrint(currentJob)
                                let successFailureQueueString = successFailure == true ? "successQueue" : "failureQueue"
                                guard let index = ((currentJob["waitForAction"] as? [String: Any])?[successFailureQueueString] as? [String: Any])?["currentJobIndex"] as? Int
                                else {
                                    print("Wait for action currentJobIndex not found.")
                                    return
                                }
                                print("Wait for action current job index:")
                                print(index)
                                self.currentWaitForActionJobIndex = index
                                if let waitForAction = mainQueue.array()[waitForActionIndex!] as? WaitForAction {
                                    if successFailure == true {
                                        if index < waitForAction.successModules.count {
                                            self.currentWaitForActionModule = (waitForAction.successModules[index]).value
                                        }
                                    } else if successFailure == false {
                                        if index < waitForAction.failureModules.count {
                                            self.currentWaitForActionModule = (waitForAction.failureModules[index]).value
                                        }
                                    }
                                    debugPrint(self.currentWaitForActionModule)
                                    self.updateCollectionView()
                                }
                            }
                        }
                    }
                }
            } else {
                print("Database retrieval failed.")
                return
            }
        })
    }
    
    func updateCollectionView() {
        if currentJobIndex < sequenceCollectionView.numberOfItems(inSection: 0) {
            sequenceCollectionView.scrollToItem(at: IndexPath(row: currentJobIndex, section: 0), at: .top, animated: true)
            sequenceCollectionView.reloadData()
        }
    }
    
    // MARK: COLLECTION VIEW DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainQueue.array().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RunningModuleCollectionViewCell
        cell.frame = CGRect(origin: cell.frame.origin, size: collectionViewLayout.itemSize)
        cell.contentView.frame = CGRect(origin: cell.contentView.frame.origin, size: collectionViewLayout.itemSize)
        let defaultFont = cell.detailLabel.font
        let vitalSignsFont = UIFont(name: defaultFont!.fontName, size: vitalSignsRunningCellDetailLabelFontSize)

        // Empty cell text
        cell.detailLabel.text = ""
        
        if let module = mainQueue.array()[indexPath.row] as? Module {
            // Module Cell
            if module.label != "Wait for Action" {
                cell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
                
                if module.parallelModule != nil {
                    cell.backgroundColor = gradientFrom(firstColor: UIColor(hex: module.tintColorString!), secondColor: UIColor(hex: module.parallelModule!.value.tintColorString!), frame: cell.frame)
                }
                
                let textColor = (UIColor.black.isContrasting(with: cell.backgroundColor!)) ? UIColor.black : UIColor.white
                cell.detailLabel.textColor = textColor
                cell.moduleLabel.textColor = textColor
                cell.moduleStatusLabel.textColor = textColor
                
                cell.moduleLabel.text = {
                    if module.parallelModule == nil {
                        return module.label
                    } else {
                        return module.label + " + " + module.parallelModule!.value.label
                    }
                }()
                
                if module.parallelModule?.value.label != "Vital Signs" {
                    cell.detailLabel.text = Module.detailTextFromModule(module: module)
                } else { // When the parallel module is Vital Signs
                    if module.duration.value != nil {
                        cell.detailLabel.text = String(describing: module.duration.value!) + " sec\n"
                    }
                    cell.detailLabel.text?.append(Module.detailTextFromModule(module: module.parallelModule!.value)!)
                }
                
                if module.label == "Vital Signs" || module.parallelModule?.value.label == "Vital Signs" {
                    cell.detailLabel.font = vitalSignsFont
                } else {
                    cell.detailLabel.font = defaultFont
                }
                
                // Next button setup
                cell.nextButton.hideLoading()
                cell.nextButton.setTitleColor(moduleButtonTextColor, for: .normal)
                cell.nextButton.backgroundColor = moduleButtonBackgroundColor
                
                cell.nextButton.isHidden = true
                cell.moduleStatusLabel.isHidden = true
                
                cell.alpha = disabledModuleAlpha
                cell.nextButton.isEnabled = false
                cell.moduleStatusLabel.text = "Now Playing"
                
                if module.duration.value == nil && module.label != "Change Parameters" && module.label != "Stop Simulation" {
                    cell.nextButton.isHidden = false
                    cell.nextButton.tag = indexPath.row
                    cell.nextButton.addTarget(self, action: #selector(RunningViewController.goToNextJob), for: .touchUpInside)
                }
                
                if indexPath.row == currentJobIndex {
                    cell.alpha = 1
                    cell.nextButton.isEnabled = true
                    cell.moduleStatusLabel.isHidden = false
                } else if indexPath.row == (currentJobIndex + 1) {
                    cell.moduleStatusLabel.isHidden = false
                    cell.moduleStatusLabel.text = "Up Next"
                }
            // Wait for Action Cell Setup
            } else {
                let waitForActionCell = collectionView.dequeueReusableCell(withReuseIdentifier: waitForActionReuseIdentifier, for: indexPath) as! WaitForActionRunningCollectionViewCell
                waitForActionCell.frame = CGRect(origin: waitForActionCell.frame.origin, size: collectionViewLayout.itemSize)
                waitForActionCell.contentView.frame = CGRect(origin: waitForActionCell.contentView.frame.origin, size: collectionViewLayout.itemSize)
                // Initial cell setup
                if currentWaitForActionJobIndex < 0 { waitForActionCell.initSetup() }
                waitForActionCell.alpha = disabledModuleAlpha
                waitForActionCell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: waitForActionCell.frame)
                let textColor = (UIColor.black.isContrasting(with: waitForActionCell.backgroundColor!)) ? UIColor.black : UIColor.white
                waitForActionCell.moduleLabel.textColor = textColor
                waitForActionCell.moduleStatusLabel.textColor = textColor
                waitForActionCell.subModuleLabel.textColor = textColor
                waitForActionCell.subModuleDetailLabel.textColor = textColor
                waitForActionCell.successButton.isEnabled = true
                waitForActionCell.failureButton.isEnabled = true
                waitForActionCell.nextButton.isEnabled = true
                waitForActionCell.successButton.isHidden = false
                waitForActionCell.failureButton.isHidden = false
                waitForActionCell.moduleStatusLabel.isHidden = false
                waitForActionCell.subModuleView.isHidden = true
                waitForActionCell.subModuleDetailLabel.isHidden = true
                
                // Next button setup
                waitForActionCell.nextButton.setTitleColor(moduleButtonTextColor, for: .normal)
                waitForActionCell.nextButton.backgroundColor = moduleButtonBackgroundColor
                
                // Module label and detail label
                waitForActionCell.moduleLabel.text = module.label
                if let actionLabelString = (module as! WaitForAction).actionLabel {
                    waitForActionCell.moduleLabel.text = module.label + ": " + actionLabelString
                }
                
                if successFailureTriggered != nil && currentJobIndex == indexPath.row {
                    if currentWaitForActionJobIndex < 0 {
                        waitForActionCell.showSubModule()
                    } else {
                        waitForActionCell.subModuleView.isHidden = false
                    }
                    
                    if self.currentWaitForActionModule != nil {
                        if waitForActionCell.subModuleLabel.text != self.currentWaitForActionModule?.label {
                            waitForActionCell.transitionToNextSubModule(backgroundColor: UIColor(hex: (self.currentWaitForActionModule?.tintColorString)!), setupClosure: {
                                waitForActionCell.subModuleLabel.text = self.currentWaitForActionModule?.label
                                if self.currentWaitForActionModule?.label == "Vital Signs" {
                                    waitForActionCell.subModuleLabel.text?.append("\n" + Module.detailTextFromModule(module: self.currentWaitForActionModule!)!)
                                }
                                
                                if self.currentWaitForActionModule?.duration.value != nil {
                                    waitForActionCell.subModuleDetailLabel.isHidden = false
                                    waitForActionCell.subModuleDetailLabel.text = "\(self.currentWaitForActionModule!.duration.value!) sec"
                                    waitForActionCell.nextButton.isHidden = true
                                } else if self.currentWaitForActionModule?.label == "Change Parameters" || self.currentWaitForActionModule?.label == "Stop Simulation" {
                                    waitForActionCell.subModuleDetailLabel.isHidden = true
                                    waitForActionCell.nextButton.isHidden = true
                                } else {
                                    waitForActionCell.subModuleDetailLabel.isHidden = true
                                    waitForActionCell.nextButton.isHidden = false
                                    waitForActionCell.nextButton.addTarget(self, action: #selector(RunningViewController.goToWaitForActionNextJob), for: .touchUpInside)
                                }
                            })
                        } else {
                            waitForActionCell.subModuleLabel.text = self.currentWaitForActionModule?.label
                            if self.currentWaitForActionModule?.duration.value != nil {
                                waitForActionCell.subModuleDetailLabel.isHidden = false
                                waitForActionCell.subModuleDetailLabel.text = "\(self.currentWaitForActionModule!.duration.value!) sec"
                                waitForActionCell.nextButton.isHidden = true
                            } else if self.currentWaitForActionModule?.label == "Change Parameters" || self.currentWaitForActionModule?.label == "Stop Simulation" {
                                waitForActionCell.subModuleDetailLabel.isHidden = true
                                waitForActionCell.nextButton.isHidden = true
                            } else {
                                waitForActionCell.subModuleDetailLabel.isHidden = true
                                waitForActionCell.nextButton.isHidden = false
                                waitForActionCell.nextButton.addTarget(self, action: #selector(RunningViewController.goToWaitForActionNextJob), for: .touchUpInside)
                            }
                        }
                    }
                    
                    if successFailureTriggered == true {
                        waitForActionCell.failureButton.isEnabled = false
                        waitForActionCell.failureButton.alpha = disabledFieldAlpha
                    } else {
                        waitForActionCell.successButton.isEnabled = false
                        waitForActionCell.successButton.alpha = disabledFieldAlpha
                    }
                }
                
                waitForActionCell.successButton.tag = indexPath.row
                waitForActionCell.failureButton.tag = indexPath.row
                waitForActionCell.successButton.addTarget(self, action: #selector(RunningViewController.executeSuccessQueue), for: .touchUpInside)
                waitForActionCell.failureButton.addTarget(self, action: #selector(RunningViewController.executeFailureQueue), for: .touchUpInside)
                waitForActionCell.moduleStatusLabel.text = "Now Playing"

                if indexPath.row == currentJobIndex {
                    waitForActionCell.alpha = 1
                } else if indexPath.row == (currentJobIndex + 1) {
                    waitForActionCell.moduleStatusLabel.isHidden = false
                    waitForActionCell.moduleStatusLabel.text = "Up Next"
                    waitForActionCell.successButton.isEnabled = false
                    waitForActionCell.failureButton.isEnabled = false
                } else {
                    waitForActionCell.failureButton.isEnabled = false
                    waitForActionCell.successButton.isEnabled = false
                }

                waitForActionCell.layoutSubviews()
                waitForActionCell.updateConstraints()
                return waitForActionCell
            }
        }
        cell.layoutIfNeeded()
        cell.updateConstraints()
        return cell
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        mainQueue.reset()
        timer.invalidate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Show tab bar
        self.showTabBar()
        
        databaseReference.child("sequence/currentJobIndex").setValue(0)
        
        print("Removing current database observers...")
        if connectingObserverHandle != nil {
            databaseReference.removeObserver(withHandle: connectingObserverHandle)
        }
        if sequenceObserverHandle != nil {
            databaseReference.removeObserver(withHandle: sequenceObserverHandle)
        }
    }
    
    @IBAction func promptStopSimulation(_ sender: Any) {
        let alertController = UIAlertController(title: "Confirm Stop", message: "Are you sure you want to stop simulation?", preferredStyle: .alert)
        
        let stopAction = UIAlertAction(title: "Yes", style: .destructive, handler: {
            action in
            if self.connectingObserverHandle != nil {
                databaseReference.removeObserver(withHandle: self.connectingObserverHandle)
            }
            if self.sequenceObserverHandle != nil {
                databaseReference.removeObserver(withHandle: self.sequenceObserverHandle)
            }
            
            // Stop database sync of parameter view controller
            if let parametersVC = self.splitViewController?.viewControllers.first as? ParameterControlViewController {
                parametersVC.couchDBclientParameters.cancelAllRequests()
            }
            
            databaseReference.child("sequence/running").setValue(false)
            databaseReference.child("sequence/currentJobIndex").setValue(0)
            self.performSegue(withIdentifier: "stopSequence", sender: self)
        })
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertController.addAction(stopAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func goToNextJob(sender: ModuleButton) {
        print("Moving to next job...")
        
        // Indicate button pressed
//        sender.setTitle("...", for: .normal)
        sender.showLoading()
        sender.isEnabled = false
        
        let indexPathRow = sender.tag
        if let module = mainQueue.array()[indexPathRow] as? Module {
            let path = "sequence/main/\(currentJobIndex)/\(module.name)/duration"
            let currentIndex = currentJobIndex
            databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
            Timer.after(moduleChangeThreshold.seconds) {
                if currentIndex == self.currentWaitForActionJobIndex {
                    print("Current module didn't change. Resending call...")
                    databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
                } else {
                    print("Moved to next module.")
                }
            }
        }
    }
    
    @objc func goToWaitForActionNextJob(sender: ModuleButton) {
        print("Moving to wait for action's next job...")
        sender.showLoading()
        sender.isEnabled = false
        if successFailureTriggered == true {
            let path = "sequence/main/\(currentJobIndex)/waitForAction/successQueue/\(currentWaitForActionJobIndex)/\(currentWaitForActionModule!.name)/duration"
            let currentIndex = currentWaitForActionJobIndex
            databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
            Timer.after(waitForActionChangeThreshold.seconds) {
                if currentIndex == self.currentWaitForActionJobIndex {
                    print("Current wait for action module didn't change. Resending call...")
                    databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
                } else {
                    print("Moved to next module.")
                }
            }
        } else if successFailureTriggered == false {
            let path = "sequence/main/\(currentJobIndex)/waitForAction/failureQueue/\(currentWaitForActionJobIndex)/\(currentWaitForActionModule!.name)/duration"
            let currentIndex = currentWaitForActionJobIndex
            databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
            Timer.after(waitForActionChangeThreshold.seconds) {
                if currentIndex == self.currentWaitForActionJobIndex {
                    print("Current wait for action module didn't change. Resending call...")
                    databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
                } else {
                    print("Moved to next module.")
                }
            }

        }
    }
    
    @objc func executeSuccessQueue(sender: UIButton) {
        print("Executing success queue...")
        let indexPathRow = sender.tag
        if let module = mainQueue.array()[indexPathRow] as? Module {
            let path = "sequence/main/\(currentJobIndex)/\(module.name)/success"
            databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
            databaseReference.child(path).setValue(true)
            let currentIndex = currentWaitForActionJobIndex
            
            // If sequence didn't move on to next module within a certain threshold period, attempt to move to next module again
            Timer.after(waitForActionChangeThreshold.seconds) {
                if currentIndex == self.currentWaitForActionJobIndex {
                    print("Current wait for action module didn't change. Resending call...")
                    databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
                    databaseReference.child(path).setValue(true)
                } else {
                    print("Moved to next module.")
                }
            }
        }
        listenToCurrentJobIndex(waitForActionIndex: indexPathRow, successFailure: true)
        
        successFailureTriggered = true
        sequenceCollectionView.reloadData()
    }
    
    @objc func executeFailureQueue(sender: UIButton) {
        print("Executing failure queue...")
        let indexPathRow = sender.tag
        if let module = mainQueue.array()[indexPathRow] as? Module {
            let path = "sequence/main/\(currentJobIndex)/\(module.name)/success"
            databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
            databaseReference.child(path).setValue(false)
            let currentIndex = currentWaitForActionJobIndex
            
            // If sequence didn't move on to next module within a certain threshold period, attempt to move to next module again
            Timer.after(waitForActionChangeThreshold.seconds) {
                if currentIndex == self.currentWaitForActionJobIndex {
                    print("Current wait for action module didn't change. Resending call...")
                    databaseReference.child(path).setValue(Int(arc4random_uniform(150)))
                    databaseReference.child(path).setValue(false)
                } else {
                    print("Moved to next module.")
                }
            }

        }
        
        listenToCurrentJobIndex(waitForActionIndex: indexPathRow, successFailure: false)
        
        successFailureTriggered = false
        sequenceCollectionView.reloadData()
    }
    
    func simulationDidStop() {
        let alertController = UIAlertController(title: "Simulation Stopped", message: "Scenario is complete.", preferredStyle: .alert)
        
        databaseReference.child(mainQueue.sequenceNode + "/connected").setValue(false)
        
        let stopAction = UIAlertAction(title: "OK", style: .destructive, handler: {
            action in
            // Stop database sync of parameter view controller
            // TODO: Check the below when the sequence manager is rewritten
            if let parametersVC = self.splitViewController?.viewControllers.first as? ParameterControlViewController {
                parametersVC.couchDBclientParameters.cancelAllRequests()
            }
            self.performSegue(withIdentifier: "stopSequence", sender: self)
        })
        alertController.addAction(stopAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: DURATION COUNTER
    @objc func updateTime() {
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        //Find the difference between current time and start time.
        var elapsedTime: TimeInterval = currentTime - startTime
        //calculate the minutes in elapsed time.
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (TimeInterval(minutes) * 60)
        //calculate the seconds in elapsed time.
        let seconds = UInt8(elapsedTime)
        elapsedTime -= TimeInterval(seconds)
        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        
        //concatenate minutes, seconds and milliseconds as assign it to the UILabel
        self.title = "Running Scenario: \(strMinutes):\(strSeconds)"
    }
 
    // MARK: ORIENTATION CHANGES
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        // Set collection view section inset
        self.collectionViewLayout.sectionInset = UIEdgeInsets(top: 30, left: self.sequenceCollectionView.frame.width / 3, bottom: 30, right: self.sequenceCollectionView.frame.width / 3)
    }
    
    // MARK: UTILITIES
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func hideTabBar() {
        anim {
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func showTabBar() {
        anim {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
}
