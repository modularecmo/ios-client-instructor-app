//
//  RunningModuleCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 3/26/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class RunningModuleCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var nextButton: ModuleButton!
    @IBOutlet weak var moduleStatusLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = moduleCornerRadius * 1.5
        self.layer.masksToBounds = true
    }

}
