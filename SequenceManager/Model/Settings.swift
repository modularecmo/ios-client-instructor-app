//
//  Settings.swift
//  SequenceManager
//
//  Created by Abdol on 8/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Settings: Object {
    @objc dynamic var minimumFlowRate: Float = 0
    @objc dynamic var maximumFlowRate: Float = 0
}

func saveSettings() {
    try! realm.write {
        print("Saving app settings...")
        settings.maximumFlowRate = maximumFlowRate.floatValue
        settings.minimumFlowRate = minimumFlowRate.floatValue
        debugPrint(settings)
    }
}

func loadSettings() {
    print("Loading app settings...")
    debugPrint(settings)
    minimumFlowRate = NSNumber(value: settings.minimumFlowRate)
    maximumFlowRate = NSNumber(value: settings.maximumFlowRate)
}
