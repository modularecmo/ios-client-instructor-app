//
//  Delay.swift
//  SequenceManager
//
//  Created by Abdol on 3/8/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Delay: Module {    
    required override init() {
        super.init()
        self.label = "Delay"
        self.name = "delay"
        self.descriptionString = "Pause simulation for a specified period of time (in seconds)."
        self.tintColorString = "#26D26E"
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
