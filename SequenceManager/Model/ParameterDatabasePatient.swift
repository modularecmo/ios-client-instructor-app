//
//  Database.swift
//  AssistedControl
//
//  Created by Abdol on 2/1/17.
//  Copyright © 2017 QU. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import RealmSwift

// MARK: PARAMETER METHODS
func initParametersPatient(dictionary: [String:Float]? = nil, couchDB: minimalCouch) {
    parametersPatient = ParametersPatient()
    if dictionary == nil {
        print("Initializing parameters locally and remotely...")
        
        for parameter in Parameters.names() {
            updateParameter(key: parameter, value: 0.0, couchDB: couchDB)
        }
    } else {
        print("Initializing parameters from dictionary...")
        for item in dictionary! {
            updateParameter(key: item.key, value: item.value, couchDB: couchDB)
        }
    }
    debugPrint(parametersPatient)
}

func updateParameterPatient(key: String, value: Float, updateRemote: Bool = false, couchDB: minimalCouch) {
    try! realm.write {
        parametersPatient[key] = value
        //debugPrint(parameters)
        switch key {
        case "ecg":
            parametersPatient.ecg = RealmOptional<Float>(value)
        case "spo2":
            parametersPatient.spo2 = RealmOptional<Float>(value)
        case "resp":
            parametersPatient.resp = RealmOptional<Float>(value)
        case "temp":
            parametersPatient.temp = RealmOptional<Float>(value)
        case "SBP":
            parametersPatient.SBP = RealmOptional<Float>(value)
        case "DBP":
            parametersPatient.DBP = RealmOptional<Float>(value)
        case "NISBP":
            parametersPatient.NISBP = RealmOptional<Float>(value)
        case "NIDBP":
            parametersPatient.NIDBP = RealmOptional<Float>(value)
        case "BIS":
            parametersPatient.BIS = RealmOptional<Float>(value)
        default:
            break
        }
        print("Parameter \(key) updated to \(value).")
    }
    
    // TODO: Update one parameter instead of sending all
    if updateRemote {
        let path = "parameters"
        let value = parametersPatient.dictionary()
        if couchDBEnabled {
            couchDB.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
}
