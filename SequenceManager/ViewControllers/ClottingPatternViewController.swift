//
//  ClottingPatternViewController.swift
//  SequenceManager
//
//  Created by Abdol on 7/26/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import RealmSwift

class ClottingPatternViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var clottings: Results<Clotting>!
    @IBOutlet weak var patternCollectionView: UICollectionView!
    var delegate: PopoverDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clottings = realm.objects(Clotting.self).filter("showInLoadList = true")
        self.view.backgroundColor = UIColor(hex: Clotting().tintColorString!)
        patternCollectionView.backgroundColor = self.view.backgroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //patternCollectionView.reloadData()
    }
    
    // MARK: PATTERNS COLLECTION VIEW DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return clottings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clottingMatrixCell", for: indexPath) as! ClottingMatrixCollectionViewCell
        cell.clottingID = clottings[indexPath.row].id
        cell.deletePatternButton.addTarget(self, action: #selector(ClottingPatternViewController.deletePattern), for: .touchUpInside)
        cell.deletePatternButton.tag = indexPath.row
        cell.LEDMatrix = Clotting.LEDMatrix(from: clottings[indexPath.row].LEDMatrix)
        cell.layoutSubviews()
        return cell
    }
    
    @objc func deletePattern(sender: UIButton) {
        try! realm.write {
            clottings[sender.tag].showInLoadList = false
        }
        patternCollectionView.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
        
//        let tempClottings = clottings
//        clottings = nil
//        patternCollectionView.reloadData()
//        clottings = tempClottings
//        Timer.after(0.3, {
//            self.patternCollectionView.reloadData()
//        })
    }
    
    // MARK:PATTERNS COLLECTION VIEW DELEGATE
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        debugPrint(clottings[indexPath.row])
        currentClotting = Clotting()
        
        currentClotting.id = clottings[indexPath.row].id
        currentClotting.blue = clottings[indexPath.row].blue
        currentClotting.red = clottings[indexPath.row].red
        currentClotting.green = clottings[indexPath.row].green
        currentClotting.defaultBlue = clottings[indexPath.row].defaultBlue
        currentClotting.defaultRed = clottings[indexPath.row].defaultRed
        currentClotting.defaultGreen = clottings[indexPath.row].defaultGreen
        currentClotting.LEDMatrix = clottings[indexPath.row].LEDMatrix
        currentClotting.normalizedColorSpectrumX = clottings[indexPath.row].normalizedColorSpectrumX
        currentClotting.normalizedColorSpectrumY = clottings[indexPath.row].normalizedColorSpectrumY
        currentClotting.defaultNormalizedColorSpectrumX = clottings[indexPath.row].defaultNormalizedColorSpectrumX
        currentClotting.defaultNormalizedColorSpectrumY = clottings[indexPath.row].defaultNormalizedColorSpectrumY
        currentClotting.brightness = clottings[indexPath.row].brightness
        
        self.dismiss(animated: true) {
            print("Dimissing pattern collection view...")
            self.delegate?.popoverDismissed()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
