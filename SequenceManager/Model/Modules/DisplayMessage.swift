//
//  DisplayMessage.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class DisplayMessage: Module {
    @objc dynamic var message: String?
    @objc dynamic var barMessage: String?
    @objc dynamic var alarm: Bool = false
    @objc dynamic var priority: Int = 0
    var priorityRange = 1...2
    
    required override init() {
        super.init()
        self.label = "Display Message"
        self.name = "displayMessage"
        self.descriptionString = "Display a message on ECMO machine's console for a specified period (in seconds)."
//        self.tintColorString = "#457B9D"
        self.tintColorString = "#DD9818"
    }
//    
//    override static func ignoredProperties() -> [String] {
//        return ["priorityRange"]
//    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["message": message ?? "nil", "barMessage": barMessage ?? "nil", "alarm": alarm, "priority": priority, "duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    func dictionaryForRemote() -> [String: Any?] {
        return ["message": message, "barMessage": barMessage, "alarm": alarm, "priority": priority]
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
