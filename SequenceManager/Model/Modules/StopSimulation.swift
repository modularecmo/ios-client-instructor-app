//
//  StopSimulation.swift
//  SequenceManager
//
//  Created by Abdol on 2/28/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class StopSimulation: Module {
    @objc dynamic var stop: Bool = true
    
    required override init() {
        super.init()
        self.label = "Stop Simulation"
        self.name = "stop"
        self.tintColorString = "#000000"
    }
    
    override func dictionary() -> [String : Any] {
        return ["stop": stop]
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}


