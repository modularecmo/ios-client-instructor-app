//
//  Sequence.swift
//  SequenceManager
//
//  Created by Abdol on 2/28/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift

protocol ModuleDelegate: class {
    
    func dictionary() -> [String: Any]
}

class Module: Object, ModuleDelegate {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var label: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var descriptionString: String = ""
    @objc dynamic var tintColorString: String?
    let duration = RealmOptional<Float>()
    @objc dynamic var parallelModule: AnyModule?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    internal func dictionary() -> [String: Any] {
        return ["": ""]
    }
        
    static func subclasses() -> [AnyObject] {
        let subclasses = [Delay(), WaitForAction(), ChangeParameters(), DisplayMessage(), StopSimulation(), Bleeding(), Deoxygenation(), OxygenatorNoise(), PowerDisconnection(), LineShattering(), Clotting(), VitalSigns()] as [AnyObject]
        return subclasses
    }
    
    static func nonParallelModules() -> [String] {
        return ["Wait for Action", "Stop Simulation", "Delay", "Change Parameters", "Vital Signs"]
    }

    static func moduleFromLabel(moduleLabel: String) -> AnyObject {
        switch moduleLabel {
        case "Delay":
            return Delay()
        case "Wait for Action":
            return WaitForAction()
        case "Change Parameters":
            let changeParameters = ChangeParameters()
            let parameters = Parameters()
            parameters.rpm = RealmOptional<Float>(4000)
            parameters.Hb = RealmOptional<Float>(70.0)
            changeParameters.parameters = parameters
            return changeParameters
        case "Display Message":
            return DisplayMessage()
        case "Stop Simulation":
            return StopSimulation()
        case "Bleeding":
            return Bleeding()
        case "Oxygenation Failure":
            return Deoxygenation()
        case "Pump Noise":
            return OxygenatorNoise()
        case "Power Disconnection":
            return PowerDisconnection()
        case "Line Shattering":
            return LineShattering()
        case "Clotting":
            return Clotting()
        case "Vital Signs":
            return VitalSigns()
        default:
            return Delay()
        }
    }
    
    static func detailTextFromModule(module: Module) -> String? {
        var detailString: String? = nil
        switch module.label {
        case "Delay", "Oxygenation Failure":
            if let duration = module.duration.value {
                detailString = "\(duration) sec"
            }
        case "Line Shattering":
            detailString = ""
            if let duration = module.duration.value {
                detailString = "\(duration) sec\n"
            }
            let modeString = (module as! LineShattering).mode == 2 ? "soft" : "hard"
            detailString?.append(modeString + " mode")
        case "Bleeding":
            detailString = ""
            if let duration = module.duration.value {
                detailString = "\(duration) sec\n"
            }
            detailString?.append("flow rate " + String(format: Vformat, (module as? Bleeding)!.flowRate) + " " + flowRateUnit)
        case "Wait for Action":
            detailString = ""
            if let wait = module as? WaitForAction {
                detailString?.append("Success: ")
                for (index, subModule) in wait.successModules.enumerated() {
                    detailString?.append((subModule.value).label)
                    if index != wait.successModules.count - 1 {
                        detailString?.append(", ")
                    }
                }
                detailString?.append("\nFailure: ")
                for (index, subModule) in wait.failureModules.enumerated() {
                    detailString?.append((subModule.value).label)
                    if index != wait.failureModules.count - 1 {
                        detailString?.append(", ")
                    }
                }
            }
        case "Change Parameters":
            if let changeParameters = module as? ChangeParameters {
                detailString = ""
                for (key, value) in (changeParameters.parameters?.dictionary())! {
                    detailString = detailString! + key + ": " + String(format: "%.1f", value != nil ? value! : 0) + " "
                }
            }
        case "Display Message":
            if let displayMessage = module as? DisplayMessage {
                detailString = ""
                if let duration = module.duration.value {
                    detailString = "\(duration) sec\n"
                }
                print(displayMessage.message!)
                var message = ""
                if displayMessage.message != nil {
                    message = (displayMessage.message! != "nil" ? displayMessage.message! : "") + "\n"
                }
                let alarm = (displayMessage.alarm ? "ALARM ON" : "")
                let priority = (displayMessage.priority == 1 ? "red" : "yellow")
                detailString?.append(message + alarm + " " + priority)
            }
        case "Pump Noise":
            if let oxygenatorNoise = module as? OxygenatorNoise {
                detailString = ""
                if let duration = oxygenatorNoise.duration.value {
                    detailString = "\(duration) sec\n"
                }
                detailString?.append("volume \(Int(oxygenatorNoise.volume / OxygenatorNoise.volumeRange.upperBound * 100))%")
            }
        case "Power Disconnection":
            if let powerDisconnection = module as? PowerDisconnection {
                detailString = ""
                if let duration = powerDisconnection.duration.value {
                    detailString = "\(duration) sec\n"
                }
                if let batteryLevel = powerDisconnection.batteryLevel.value {
                    detailString?.append("battery \(Int(batteryLevel * 100))%")
                }
            }
        case "Clotting":
            if let clotting = module as? Clotting {
                detailString = ""
                if let duration = clotting.duration.value {
                    detailString = "\(duration) sec\n"
                }
                let brightness = clotting.brightness
                let red = clotting.red
                let green = clotting.green
                let blue = clotting.blue
                detailString?.append("brightness \(brightness)\n")
                detailString?.append("r: \(red) g: \(green) b: \(blue)\n")
            }
        case "Vital Signs":
            if let vitalSigns = module as? VitalSigns {
                detailString = ""
                
                if let duration = vitalSigns.duration.value {
                    detailString = "\(duration) sec\n"
                }
                
                if vitalSigns.heartRate != 0 { detailString?.append(" HR: " + String(Int(vitalSigns.heartRate))) }
                if vitalSigns.spO2 != 0 && vitalSigns.heartRate != 0 { detailString?.append(", ") }
                if vitalSigns.spO2 != 0 { detailString?.append(" SPO2: " + String(Int(vitalSigns.spO2)) + "%") }
                
                if vitalSigns.spO2 != 0 || vitalSigns.heartRate != 0 { detailString?.append("\n") }
                
                if vitalSigns.bloodPressure != "" { detailString?.append(" ART: " + vitalSigns.bloodPressure) }
                if vitalSigns.bloodPressure != "" && vitalSigns.respiratoryRate != 0 { detailString?.append(", ") }
                if vitalSigns.respiratoryRate != 0 { detailString?.append(" RR: " + String(Int(vitalSigns.respiratoryRate))) }
                
                if vitalSigns.bloodPressure != "" || vitalSigns.respiratoryRate != 0 { detailString?.append("\n") }
                
                if vitalSigns.co2 != 0 { detailString?.append(" CO2: " + String(Int(vitalSigns.co2))) }
                if vitalSigns.co2 != 0 && vitalSigns.temperature != 0 { detailString?.append(", ") }
                if vitalSigns.temperature != 0 { detailString?.append(" Temp: " + String(vitalSigns.temperature)) }
            }
        default:
            break
        }
        return detailString
    }
}

