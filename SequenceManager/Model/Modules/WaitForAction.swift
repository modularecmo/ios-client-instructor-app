//
//  WaitForAction.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class WaitForAction: Module {
    var success = RealmOptional<Bool>()
    var successModules = List<AnyModule>()
    var failureModules = List<AnyModule>()
    @objc dynamic var actionLabel: String?
    
    required override init() {
        super.init()
        self.label = "Wait for Action"
        self.name = "waitForAction"
        self.descriptionString = "Wait for trainee's action–on a specific issue–and trigger a sequence based success or failure in solving the speicified issue."
        //self.tintColorString = "#E8C253"
//        self.tintColorString = "#46D6BE"
        self.tintColorString = "#3A6EA5"
    }
    
    override func dictionary() -> [String: Any] {
        var successSequence: [String:[String:[String:Any]]] = [:]
        var failureSequence: [String:[String:[String:Any]]] = [:]
        
        for (index, module) in successModules.enumerated() {
            successSequence[String(index)] = [module.value.name:module.value.dictionary()]
        }
        
        for (index, module) in failureModules.enumerated() {
            failureSequence[String(index)] = [module.value.name:module.value.dictionary()]
        }
        return ["success": success.value ?? "nil", "successQueue": successSequence, "failureQueue": failureSequence]
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
