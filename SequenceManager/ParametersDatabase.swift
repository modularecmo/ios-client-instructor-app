//
//  Database.swift
//  AssistedControl
//
//  Created by Abdol on 2/1/17.
//  Copyright © 2017 QU. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import RealmSwift

// MARK: PARAMETER METHODS
func initParameters(dictionary: [String:Float]? = nil, couchDB: minimalCouch) {
    parameters = Parameters()
    if dictionary == nil {
        print("Initializing parameters locally and remotely...")
        
        for parameter in Parameters.names() {
            updateParameter(key: parameter, value: 0.0, couchDB: couchDB)
        }
    } else {
        print("Initializing parameters from dictionary...")
        for item in dictionary! {
            updateParameter(key: item.key, value: item.value, couchDB: couchDB)
        }
    }
    debugPrint(parameters)
}

func updateParameter(key: String, value: Float, updateRemote: Bool = false, couchDB: minimalCouch) {
    try! realm.write {
        parameters[key] = value
        //debugPrint(parameters)
        switch key {
        case "rpm":
            parameters.rpm = RealmOptional<Float>(value)
        case "Pven":
            parameters.Pven = RealmOptional<Float>(value)
        case "Part":
            parameters.Part = RealmOptional<Float>(value)
        case "Pint":
            parameters.Pint = RealmOptional<Float>(value)
        case "svO2":
            parameters.svO2 = RealmOptional<Float>(value)
        case "Tart":
            parameters.Tart = RealmOptional<Float>(value)
        case "Tven":
            parameters.Tven = RealmOptional<Float>(value)
        case "Hb":
            parameters.Hb = RealmOptional<Float>(value)
        case "Hct":
            parameters.Hct = RealmOptional<Float>(value)
        case "V":
            parameters.V = RealmOptional<Float>(value)
        case "deltaP":
            parameters.deltaP = RealmOptional<Float>(value)
        default:
            break
        }
        print("Parameter \(key) updated to \(value).")
    }
    
    // TODO: Update one parameter instead of sending all
    if updateRemote {
        let path = "parameters"
        let value = parameters.dictionary()
        if couchDBEnabled {
            couchDB.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
}
