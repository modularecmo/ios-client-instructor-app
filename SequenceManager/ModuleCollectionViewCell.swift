//
//  ModuleCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 3/16/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class ModuleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = moduleCornerRadius
        self.layer.masksToBounds = true
    }
}
