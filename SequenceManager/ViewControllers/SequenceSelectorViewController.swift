//
//  SequenceSelectorViewController.swift
//  SequenceManager
//
//  Created by Abdol on 4/2/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import RealmSwift
import Intents


class SequenceSelectorViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var sequenceSelectorCollectionView: UICollectionView!
    @IBOutlet weak var sequenceSequenceCollectionViewLayout: UICollectionViewFlowLayout!
    
    var sequences: Results<Sequence>?
    @IBOutlet weak var editButton: UIBarButtonItem!
    @objc var editMode = false
    @objc var liveMode = false
    
    // MARK: INITIALIZATION
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Retreiving sequences...")
        sequences = realm.objects(Sequence.self).sorted(byKeyPath: "createdAt")
        debugPrint(sequences as Any)
        
        // Activate live mode when view controller is in a split view
//        if self.splitViewController != nil {
//            liveMode = true
//        }

        sequenceSequenceCollectionViewLayout.itemSize = CGSize(width: sequenceCellHeight, height: sequenceCellWidth)
        
        // Configure split view master view width
        if let parameterControlVC = splitViewController?.viewControllers.first as? ParameterControlViewController {
            parameterControlVC.minimumPrimaryColumnWidth = 0
        }
        splitViewController?.preferredPrimaryColumnWidthFraction = 0
        splitViewController?.minimumPrimaryColumnWidth = 0
        splitViewController?.maximumPrimaryColumnWidth = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        sequenceSelectorCollectionView.reloadData()
    }
    
    // MARK: COLLECTION VIEW DATASOURCE 
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return !(sequences?.isEmpty)! ? sequences!.count + 1 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("Creating sequence cell...")
        if indexPath.row < (collectionView.numberOfItems(inSection: 0) - 1)  {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sequenceCell", for: indexPath) as! SequenceCollectionViewCell
            cell.frame = CGRect(origin: cell.frame.origin, size: sequenceSequenceCollectionViewLayout.itemSize)
            cell.contentView.frame = CGRect(origin: cell.contentView.frame.origin, size: sequenceSequenceCollectionViewLayout.itemSize)
            cell.label.text = sequences?[indexPath.row].name
            cell.backgroundColor = UIColor.white
            cell.deleteButton.isHidden = true
            cell.modulesStackView.isHidden = false
            
            if editMode {
                cell.deleteButton.isHidden = false
                cell.deleteButton.setTitleColor(deleteSequenceButtonTintColor, for: .normal)
                cell.deleteButton.tag = indexPath.row
                cell.deleteButton.addTarget(self, action: #selector(SequenceSelectorViewController.deleteSequence), for: .touchUpInside)
            }
            
            // Add indicator
            for view in cell.modulesStackView.subviews {
                view.removeFromSuperview()
            }
            debugPrint(cell.modulesStackView.subviews)
            
            if sequences?[indexPath.row].modules !=  nil {
                for module in (sequences?[indexPath.row].modules)! {
                    let label = UILabel()
                    label.text = "•"
                    label.font = UIFont.boldSystemFont(ofSize: moduleIndicatorFontSize)
                    label.textColor = UIColor(hex: module.value.tintColorString!)
                    cell.modulesStackView.addArrangedSubview(label)
                }
                debugPrint(cell.modulesStackView.subviews)
            }
            cell.layoutSubviews()
            cell.updateConstraints()
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addSequenceCell", for: indexPath) as! SequenceCollectionViewCell
            cell.frame = CGRect(origin: cell.frame.origin, size: sequenceSequenceCollectionViewLayout.itemSize)
            cell.contentView.frame = CGRect(origin: cell.contentView.frame.origin, size: sequenceSequenceCollectionViewLayout.itemSize)
            cell.label.text = "+\n new sequence"
            cell.backgroundColor = addSequenceCellBackgroundColor
            cell.layoutSubviews()
            cell.updateConstraints()
            return cell
        }
    }
    
    // Rename sequence
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if editMode {
            renameSequenceAt(indexPath: indexPath)
        } else if liveMode {
            performSegue(withIdentifier: "runSequenceFromSelector", sender: self)
        }
    }

    // MARK: EDIT MODE
    @IBAction func toggleEditMode(_ sender: Any) {
        editMode = !editMode
        editButton.title = editMode ? "Done" : "Edit"
        sequenceSelectorCollectionView?.reloadData()

    }
    
    @objc func deleteSequence(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Delete Sequence", message: "Are you sure you want to delete this sequence?", preferredStyle: .alert)
        
        let stopAction = UIAlertAction(title: "Delete", style: .destructive, handler: {
            action in
            
            print("Deleting sequence \(sender.tag)...")
            mainQueue.deleteSequence(at: sender.tag)
            self.sequenceSelectorCollectionView.reloadData()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(stopAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func renameSequenceAt(indexPath: IndexPath) {
        if let sequence: Sequence = sequences?[indexPath.row] {
            let alertController = UIAlertController(title: "Rename Sequence", message: "Enter new sequence name:", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Simulation name"
                textField.text = !sequence.name.isEmpty ? sequence.name : nil
            }
            let stopAction = UIAlertAction(title: "Rename", style: .default, handler: {
                action in
                let textField = alertController.textFields![0] as UITextField
                print("Renaming \(sequence.name) to \(textField.text)...")
                try! realm.write {
                    sequence.name = textField.text!
                }
                self.sequenceSelectorCollectionView.reloadData()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(stopAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func didLongTapCell(_ gesture: UIGestureRecognizer) {
        print("Did long tap cell.")
        let point = gesture.location(in: sequenceSelectorCollectionView)
        
        if let indexPath = sequenceSelectorCollectionView.indexPathForItem(at: point) {
            if indexPath.row < sequenceSelectorCollectionView.numberOfItems(inSection: 0) - 1 {
                renameSequenceAt(indexPath: indexPath)
            }
        } else {
            print("couldn't find index path")
        }
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !editMode
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = sequenceSelectorCollectionView.indexPathsForSelectedItems?.first {
            switch segue.identifier {
            case "openSequence"?:
                if indexPath.row < sequenceSelectorCollectionView.numberOfItems(inSection: 0) - 1 {
                    print("Openning seqeunce...")
                    print("Openning seqeunce \(sequences?[indexPath.row].id)...")
                    if let destination = segue.destination as? SequenceCollectionViewController {
                        destination.sequenceID = sequences?[indexPath.row].id
                    }
                }
            case "newSequence"?:
                mainQueue.currentSequenceID = nil
            case "runSequenceFromSelector"?:
                if let sequenceID = sequences?[indexPath.row].id {
                    mainQueue = QueueManager(sequenceID: sequenceID)
                }
            default:
                break
            }
        }
    }
}
