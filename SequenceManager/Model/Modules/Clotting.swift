//
//  Clotting.swift
//  SequenceManager
//
//  Created by Abdol on 7/12/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Clotting: Module {
    
    @objc dynamic var LEDMatrix: String = "000000000000000000000000000000" // a matrix of 1's and 0's of 30 digits
    
    @objc dynamic var brightness: Int = 0 // 0 - 225

    // Clotting color
    @objc dynamic var blue: Int = 0 // 0 - 255
    @objc dynamic var red: Int = 0 // 0 - 255
    @objc dynamic var green: Int = 0 // 0 - 225
    @objc dynamic var normalizedColorSpectrumX: Float = 0 // used to set color in color picker
    @objc dynamic var normalizedColorSpectrumY: Float = 0
    
    // Default color
    @objc dynamic var defaultBlue: Int = 0
    @objc dynamic var defaultRed: Int = 0
    @objc dynamic var defaultGreen: Int = 0
    @objc dynamic var defaultNormalizedColorSpectrumX: Float = 0
    @objc dynamic var defaultNormalizedColorSpectrumY: Float = 0
    
    @objc dynamic var showInLoadList: Bool = true
    
    required override init() {
        super.init()
        self.label = "Clotting"
        self.name = "clotting"
        self.tintColorString = "#454745" //"#D90368" 
        self.descriptionString = "Simulate oxygenator clotting for a specified period (in seconds) and specified brightness. You can select which areas exhibit clotting using the matrix below. A non-clot (default) color can be also specified."
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["M": LEDMatrix, "RGB": "\(red),\(green),\(blue)", "DRGB": "\(defaultRed),\(defaultGreen),\(defaultBlue)", "Br": brightness, "duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    static func LEDMatrix(from collectionView: UICollectionView, inverse: Bool = false) -> [Int] {
        var LEDMatrix: [Int] = inverse ? [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1] : [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        let key = inverse ? 0 : 1
        
        if let selectedIndices: [IndexPath] = collectionView.indexPathsForSelectedItems {
            for indexPath in selectedIndices {
                switch indexPath.row {
                case 0...11, 18...29:
                    LEDMatrix[indexPath.row] = key
                case 12...17:
                    let index = swapIndex(indexPath.row)
                    LEDMatrix[index] = key
                default:
                    break
                }
            }
        }
        return LEDMatrix
    }
    
    static func LEDMatrix(from matrixString: String) -> [Int] {
        var LEDMatrix: [Int] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        
        for (index, character) in matrixString.characters.enumerated() {
            switch index {
            case 12...17:
                if character == "0" {
                    LEDMatrix[swapIndex(index, inverse: true)] = 0
                } else if character == "1" {
                    LEDMatrix[swapIndex(index, inverse: true)] = 1
                }
            default:
                if character == "0" {
                    LEDMatrix[index] = 0
                } else if character == "1" {
                    LEDMatrix[index] = 1
                }
            }
            
        }
        return LEDMatrix
    }
    
    
    static func LEDMatrixString(from matrix: [Int]) -> String {
        var LEDMatrixString = ""
        for item in matrix {
            LEDMatrixString += String(item)
        }
        return LEDMatrixString
    }
    
    static func swapIndex(_ index: Int, inverse: Bool = false) -> Int{
        if inverse {
            switch index {
            case 17:
                return 12
            case 16:
                return 13
            case 15:
                return 14
            case 14:
                return 15
            case 13:
                return 16
            case 12:
                return 17
            default:
                return index
            }
        } else {
            switch index {
            case 12:
                return 17
            case 13:
                return 16
            case 14:
                return 15
            case 15:
                return 14
            case 16:
                return 13
            case 17:
                return 12
            default:
                return index
            }
        }
    }
}
