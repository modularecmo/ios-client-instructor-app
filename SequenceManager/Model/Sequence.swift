//
//  Sequence.swift
//  SequenceManager
//
//  Created by Abdol on 3/28/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift

class Sequence: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var descriptionString: String = ""
    @objc dynamic var createdAt: Date = Date()
    var modules = List<AnyModule>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
