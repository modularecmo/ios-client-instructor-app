//
//  LiveControlPanelViewController.swift
//  SequenceManager
//
//  Created by Abdol on 4/14/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SwiftyTimer
import anim
import ColorPicKit
import RealmSwift


class LiveControlPanelViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, PopoverDelegate {

    // Scroll view
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var masterStackView: UIStackView!
    
    // Module views
    @IBOutlet weak var bleedingView: ControlPanelView!
    @IBOutlet weak var oxygenatorNoiseView: ControlPanelView!
    @IBOutlet weak var deoxygenationView: ControlPanelView!
    @IBOutlet weak var powerDisconnectionView: ControlPanelView!
    @IBOutlet weak var lineShatteringView: ControlPanelView!
    @IBOutlet weak var displayMessageView: ControlPanelView!
    @IBOutlet weak var pumpView: ControlPanelView!
    @IBOutlet weak var clottingView: ControlPanelView!
    @IBOutlet weak var clottingDualStackView: UIStackView!
    @IBOutlet weak var dualStackView: UIStackView!
    
    // Module switches
    @IBOutlet weak var deoxygenationSwitch: UISwitch!
    @IBOutlet weak var powerDisconnectionSwitch: UISwitch!
    @IBOutlet weak var displayMessageSwitch: UISwitch!
    @IBOutlet weak var bleedingSwitch: UISwitch!
    @IBOutlet weak var lineShatteringSwitch: UISwitch!
    @IBOutlet weak var oxygenatorNoiseSwitch: UISwitch!
    @IBOutlet weak var pumpSwitch: UISwitch!
    @IBOutlet weak var clottingSwitch: UISwitch!
    @IBOutlet weak var masterTurnOffButton: UIButton!
    
    // Display message outlets
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var barMessageField: UITextField!
    @IBOutlet weak var alarmSwitch: UISwitch!
    @IBOutlet weak var prioritySegmentedControl: UISegmentedControl!
    @IBOutlet weak var sendMessageButton: ModuleButton!
    @IBOutlet weak var saveMessageButton: ModuleButton!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var displayMessageStackView: UIStackView!
    @IBOutlet weak var removeMessageButton: ModuleButton!
    
    // Oxygenator noise outlets
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var volumeLabel: UILabel!
    var lastVolume: Float = 1.0
    
    // Pump outlet
    @IBOutlet weak var pumpSegmentedControl: UISegmentedControl!
    
    // Bleeding outlets
    @IBOutlet weak var flowRateLabel: UILabel!
    @IBOutlet weak var flowRateSlider: UISlider!
    @IBOutlet weak var calibrateBleedingButton: ModuleButton!
    @IBOutlet weak var bleedingLabel: UILabel!
    
    // Power disconnection outlets
    @IBOutlet weak var batteryLevelSegmentedControl: UISegmentedControl!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var timeRemainingSlider: UISlider!
    
    // Line shattering outlets
    @IBOutlet weak var lineShatteringSegmentedControl: UISegmentedControl!
    @IBOutlet weak var centerLineShatteringButton: ModuleButton!
    
    // Clotting outlets
    @IBOutlet weak var matrixCollectionView: UICollectionView!
    @IBOutlet weak var colorWheel: HSBASpectum!
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var allOffClottingMatrixButton: ModuleButton!
    @IBOutlet weak var clottingSegmentedControl: UISegmentedControl!
    @IBOutlet weak var brightnessLabel: UILabel!
    @IBOutlet weak var loadClottingPatternButton: ModuleButton!
    @IBOutlet weak var saveClottingPatternButton: ModuleButton!
    @IBOutlet weak var newClottingPatternButton: ModuleButton!
    
    // Clotting Setup
    var clotMode = true
    var clotColorPickerPosition: CGPoint!
    var defaultColorPickerPosition: CGPoint!
    var clotBrightness: Float = 1
    var clotting: Clotting! {
        didSet {
            applyClotting(from: clotting)
        }
    }
    
    // Other outlets
    @IBOutlet weak var offlineLabel: UILabel!
    
    // Firebase Observer Handles
    internal var moduleObserverHandle: DatabaseHandle!
    
    // MARK: INITIALIZATION
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupModules()
        SetupDatabase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Setup layout according to orientation
        adaptToOrientation()
        Timer.after(0.1.seconds, {
            // Setup bleeding
            Bleeding.getMinimumMaximumBleedFlowRate(completion: {
                self.flowRateSlider.maximumValue = maximumFlowRate.floatValue
                self.flowRateSlider.minimumValue = minimumFlowRate.floatValue
                self.flowRateSliderDidChange(self.flowRateSlider)
            })
            
            self.setupBackgrounds()
            self.setupScrollViewContentSize()
            
            // Storing clotting color picker position
            self.colorWheel.position = CGPoint(x: self.colorWheel.bounds.midX, y: 10)
            self.clotColorPickerPosition = self.colorWheel.position
            clottingCellActivatedColor = self.colorWheel.color
        })
        
        // Check internet connection
        checkInternetConnection(target: self, handler: { connected in
            //anim {
                self.offlineLabel.isHidden = connected
           // }
        })
        
//        if isSequenceRunning {
//            let alert = UIAlertController(title: "Sequence Running", message: "A sequence is currenlty running. To enable the live control panel, stop the sequence.", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
    }
    
    fileprivate func SetupDatabase() {
        // Listen to module changes
        if couchDBEnabled {
            initCouchDB()
            listenForModuleChangesCouch()
        } else {
            listenForModuleChanges()
        }
    }
    
    func setupScrollViewContentSize() {
        let scrollViewHeight = masterStackView.frame.height
        scrollView.contentSize = CGSize(width: self.view.bounds.width, height: scrollViewHeight)
    }
    
    // MARK: MODULES
    fileprivate func setupModules() {
        // Clotting view configuration
        clottingView.isHidden = !clottingEnabled
        
        // Setup oxygenator noise controls
        volumeSlider.isEnabled = oxygenatorNoiseSwitch.isOn
        volumeSlider.alpha = oxygenatorNoiseSwitch.isOn ? 1 : disabledFieldAlpha
        volumeSlider.minimumValue = OxygenatorNoise.volumeRange.lowerBound
        volumeSlider.maximumValue = OxygenatorNoise.volumeRange.upperBound
        lastVolume = OxygenatorNoise.volumeRange.upperBound
        
        // Bleeding button setup
        calibrateBleedingButton.backgroundColor = moduleButtonBackgroundColor
        calibrateBleedingButton.setTitleColor(moduleButtonTextColor, for: .normal)
        
        // Line shattering button setup
        centerLineShatteringButton.backgroundColor = moduleButtonBackgroundColor
        centerLineShatteringButton.setTitleColor(moduleButtonTextColor, for: .normal)
        
        // Setup pump control
        pumpSegmentedControl.isEnabled = pumpSwitch.isOn
        pumpSegmentedControl.alpha = pumpSwitch.isOn ? 1 : disabledFieldAlpha
        pumpSegmentedControl.selectedSegmentIndex = 0
        
        // Setup power disconnection control
        batteryLevelSegmentedControl.isEnabled = powerDisconnectionSwitch.isOn
        batteryLevelSegmentedControl.alpha = powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
        timeRemainingSlider.isEnabled = powerDisconnectionSwitch.isOn
        timeRemainingSlider.alpha = powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
        timeRemainingSlider.minimumValue = PowerDisconnection.timeRemainingRange.lowerBound
        timeRemainingSlider.maximumValue = PowerDisconnection.timeRemainingRange.upperBound
        timeRemainingSliderValueDidChange(timeRemainingSlider)
        
        // Setup display message controls
        messageTableView.layer.cornerRadius = moduleCornerRadius
        messageTableView.layer.masksToBounds = true
        removeMessageButton.isHidden = true
        displayMessageSwitch.isHidden = true // disabled feature
        sendMessageButton.backgroundColor = moduleButtonBackgroundColor
        sendMessageButton.setTitleColor(moduleButtonTextColor, for: .normal)
        
        // Setup clotting object
        clotting = currentClotting
        
        // Setup clotting matrix collection view
        matrixCollectionView.allowsMultipleSelection = true
        matrixCollectionView.transform = matrixCollectionView.transform.rotated(by: clotMatrixCollectionViewRotationAngle)
        matrixCollectionView.transform = matrixCollectionView.transform.scaledBy(x: clotMatrixCollectionViewScaleFactor, y: clotMatrixCollectionViewScaleFactor)
        lightClottingMatrix(from: clotting.LEDMatrix)
        
        // Clotting matrix buttons setup
        allOffClottingMatrixButton.setTitleColor(moduleButtonTextColor, for: .normal)
        allOffClottingMatrixButton.backgroundColor = moduleButtonBackgroundColor
        loadClottingPatternButton.backgroundColor = moduleButtonBackgroundColor
        loadClottingPatternButton.setTitleColor(moduleButtonTextColor, for: .normal)
        saveClottingPatternButton.backgroundColor = moduleButtonBackgroundColor
        saveClottingPatternButton.setTitleColor(moduleButtonTextColor, for: .normal)
        newClottingPatternButton.backgroundColor = moduleButtonBackgroundColor
        newClottingPatternButton.setTitleColor(moduleButtonTextColor, for: .normal)
        
        // Clotting segmented control setup
        clotMode = clottingSegmentedControl.selectedSegmentIndex == 0 ? true : false
        defaultColorPickerPosition = CGPoint(x: colorWheel.bounds.width, y: 0)
        clottingCellDeactivatedColor = UIColor.black
        
        // Clotting brightness slider setup
        brightnessSlider.value = brightnessSlider.maximumValue
        
        // Setup brightness slider
        brightnessSlider.tintColor = moduleButtonBackgroundColor
        
        // Color wheel setup
        colorWheel.layer.cornerRadius = moduleConfigurationCornerRadius
    }
    
    // MARK: DATABASE
    func initCouchDB() {
        couchDBclient = minimalCouch(databaseName: databaseName, mainDocumentID: documentID, databaseURL: databaseURL, databaseUsername: databaseUsername, databasePassword: databasePassword)
    }
    
    func listenForModuleChanges() {
        print("Listening for module changes.")
        moduleObserverHandle = databaseReference.observe(DataEventType.value, with: { (snapshot) in
            print("Listened for module changes.")
            
            // Hide offline label
            //anim {
                self.offlineLabel.isHidden = true
            //}
            
            if let modules = (snapshot.value as? NSDictionary)?[modulesMasterPath] as? [String: Any] {
                // MARK: - Patient modules
                if let bleedingDictionary = modules[patientMasterPath] as? NSDictionary {
                    if let bleedingState = bleedingDictionary[bleedingPath] as? Int {
                        self.bleedingSwitch.isOn = bleedingState == 1 ? true : false
                        
                        if let patientDiagnosisDictionary = modules[patientDiagnosticsMasterPath] as? NSDictionary {
                            if let diagnosticFlowRate = patientDiagnosisDictionary[bleedDiagnosticFlowRate] as? Float {
                                if diagnosticFlowRate == -1 {
                                    self.bleedingSwitch.isOn = false
                                    self.bleedingSwitch.isEnabled = false
                                    self.bleedingSwitchDidChange(self.bleedingSwitch)
                                    self.flowRateSlider.isEnabled = false
                                    self.calibrateBleedingButton.isEnabled = false
                                    self.calibrateBleedingButton.alpha = disabledFieldAlpha
                                    //anim {
                                        self.bleedingSwitch.isHidden = true
                                        //self.calibrateBleedingButton.isHidden = true
                                        self.bleedingLabel.text = "Bleeding Tank Empty. Please Refill."
                                    //}
                                } else {
                                    self.bleedingSwitch.isEnabled = true
                                    self.flowRateSlider.isEnabled = true
                                    //self.calibrateBleedingButton.isHidden = false
                                    self.calibrateBleedingButton.isEnabled = true
                                    self.calibrateBleedingButton.alpha = 1
                                    self.bleedingSwitch.isHidden = false
                                    self.bleedingLabel.text = "Bleeding"
                                }
                            }
                            
                            if let bleedingHigh = patientDiagnosisDictionary[bleedMaximumFlowRatePath] as? Float, let bleedingLow = patientDiagnosisDictionary[bleedMinimumFlowRatePath] as? Float {
                                if (bleedingHigh != settings.maximumFlowRate || bleedingLow != settings.minimumFlowRate) && bleedingHigh != -1 && bleedingLow != -1 {
                                    print("Bleed calibration complete.")
                                    
                                    if bleedingState == 2 {
                                        databaseReference.child(modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath).setValue(0)
                                    }
                                    
                                    Bleeding.getMinimumMaximumBleedFlowRate(completion: {
                                        print("minimumFlowRate: \(minimumFlowRate), maximumFlowRate: \(maximumFlowRate)")
                                        print("minimumFlowRate: \(minimumFlowRate.floatValue), maximumFlowRate: \(maximumFlowRate.floatValue)")
                                        self.flowRateSlider.maximumValue = maximumFlowRate.floatValue
                                        self.flowRateSlider.minimumValue = minimumFlowRate.floatValue
                                        self.flowRateSlider.value = self.flowRateSlider.maximumValue
                                        self.flowRateSliderDidChange(self.flowRateSlider)
                                        self.flowRateSliderDidTouchUpInside(self.flowRateSlider)
                                        self.calibrateBleedingButton.hideLoading()
                                    })
                                }
                                
                                if (bleedingHigh == -1 || bleedingLow == -1) && (settings.maximumFlowRate == 0 || settings.maximumFlowRate == 0) {
                                    print("No bleeding settings located locally and in the cloud. Forcing calibration.")
                                    self.bleedingSwitch.isOn = false
                                    self.bleedingSwitch.isEnabled = false
                                    self.bleedingSwitchDidChange(self.bleedingSwitch)
                                    self.flowRateSlider.isEnabled = false
                                    self.calibrateBleedingButton.isEnabled = true
                                    //anim {
                                        self.bleedingSwitch.isHidden = true
                                        self.bleedingLabel.text = "Please Calibate Bleeding."
                                    //}
                                }
                            }
                        }
                    }
                    if let flowRate = bleedingDictionary[bleedingFlowRatePath] as? Float {
                        self.flowRateSlider.value = flowRate
                        self.flowRateSliderDidChange(self.flowRateSlider)
                    }
                }
                
                if let lineShatteringState = (modules[patientMasterPath] as? NSDictionary)?[lineShatteringPath] as? Int {
                    self.lineShatteringSwitch.isOn = lineShatteringState > 1 ? true : false
                }
                
                if let pumpState = (modules[patientMasterPath] as? NSDictionary)?[pumpPath] as? Int {
                    self.pumpSwitch.isOn = (pumpState > 0)
                    
                    self.pumpSegmentedControl.isEnabled = self.pumpSwitch.isOn
                    self.pumpSegmentedControl.alpha = self.pumpSwitch.isOn ? 1 : disabledFieldAlpha
                    
                    switch pumpState {
                    case 1:
                        self.pumpSegmentedControl.selectedSegmentIndex = 0
                    case 2:
                        self.pumpSegmentedControl.selectedSegmentIndex = 1
                    default:
                        self.pumpSegmentedControl.selectedSegmentIndex = 0
                    }
                }
                
                // MARK: - Oxygenator modules
                if let oxygenatorNoiseState = (modules[oxygenatorMasterPath] as? NSDictionary)?[oxygenatorNoisePath] as? Int {
                    self.oxygenatorNoiseSwitch.isOn = oxygenatorNoiseState > 0
                    self.volumeSlider.value = Float(oxygenatorNoiseState)
                    self.volumeLabel.text = "Sound Level \(Int(self.volumeSlider.value / OxygenatorNoise.volumeRange.upperBound * 100))%"
                    if self.volumeSlider.value > 0 {
                        self.lastVolume = self.volumeSlider.value
                    }
                    self.volumeSlider.isEnabled = self.oxygenatorNoiseSwitch.isOn
                    self.volumeSlider.alpha = self.oxygenatorNoiseSwitch.isOn ? 1.0 : disabledFieldAlpha
                }

                // MARK: - Heater modules
                if let deoxygenationState = (modules[heaterMasterPath] as? NSDictionary)?[deoxygenationPath] as? Int {
                    self.deoxygenationSwitch.isOn = deoxygenationState == 0 ? false : true
                }

                // MARK: - Other modules
                if let powerDisconnection = modules[powerDisconnectionPath] as? NSDictionary {
                    print("Power disconnection:")
                    debugPrint(powerDisconnection)
                    self.powerDisconnectionSwitch.isOn = true
                    
                    if let batteryLevel = powerDisconnection["batteryLevel"] as? Float, let fullBatteryLife = powerDisconnection["fullBatteryLife"] as? Int {
                        self.batteryLevelSegmentedControl.selectedSegmentIndex = PowerDisconnection.getIndexFrom(value: batteryLevel)
                        self.batteryLevelSegmentedControl.isEnabled = self.powerDisconnectionSwitch.isOn
                        self.batteryLevelSegmentedControl.alpha = self.powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
                        
                        self.timeRemainingSlider.isEnabled = self.powerDisconnectionSwitch.isOn
                        self.timeRemainingSlider.alpha = self.powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
                        self.timeRemainingSlider.value = Float(fullBatteryLife) * batteryLevel
                        self.timeRemainingLabel.text = "Remaining " + String(Int(self.timeRemainingSlider.value))
                    }
                } else {
                    self.powerDisconnectionSwitch.isOn = false
                    self.powerDisconnectionSwitchDidChange(self.powerDisconnectionSwitch)
                }
                
                if let message = (modules[displayMessagePath] as? [String: Any]) {
                    //anim {
                        self.removeMessageButton.isHidden = false
                    //}

                    self.displayMessageSwitch.isOn = true
//                    self.sendMessageButton.isEnabled = self.displayMessageSwitch.isOn
//                    self.saveMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    if let alarm = message["alarm"] as? Bool {
                        self.alarmSwitch.isOn = alarm
                    }
                    
                    if let messageString = message["message"] as? String {
                        self.messageField.text = messageString
                    }
                    
                    if let barMessageString = message["barMessage"] as? String {
                        self.barMessageField.text = barMessageString
                    }
                    
                    if let priority = message["priority"] as? Int {
                        self.prioritySegmentedControl.selectedSegmentIndex = (priority - 1)
                    }
                } else {
                    //anim {
                        self.removeMessageButton.isHidden = true
                    //}
                    self.displayMessageSwitch.isOn = false
//                    self.sendMessageButton.isEnabled = self.displayMessageSwitch.isOn
//                    self.saveMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    self.alarmSwitch.isOn = false
                    //self.messageField.text = nil
                    //self.barMessageField.text = nil
                }
            }
        })
    }
    
    func listenForModuleChangesCouch() {
        print("Listening for module changes...")
        couchDBclient.startLongPolling {
            print("Listened for module changes.")
            self.offlineLabel.isHidden = true
            
            if let modules = couchDBclient.database[modulesMasterPath] as? [String: Any] {

                // MARK: - Patient modules
                if let bleedingDictionary = modules[patientMasterPath] as? NSDictionary {
                    if let bleedingState = bleedingDictionary[bleedingPath] as? Int {
                        self.bleedingSwitch.isOn = bleedingState == 1 ? true : false
                        
                        if let patientDiagnosisDictionary = modules[patientDiagnosticsMasterPath] as? NSDictionary {
                            if let diagnosticFlowRate = patientDiagnosisDictionary[bleedDiagnosticFlowRate] as? Float {
                                if diagnosticFlowRate == -1 {
                                    self.bleedingSwitch.isOn = false
                                    self.bleedingSwitch.isEnabled = false
                                    self.bleedingSwitchDidChange(self.bleedingSwitch)
                                    self.flowRateSlider.isEnabled = false
                                    self.calibrateBleedingButton.isEnabled = false
                                    self.calibrateBleedingButton.alpha = disabledFieldAlpha
                                    //anim {
                                    self.bleedingSwitch.isHidden = true
                                    //self.calibrateBleedingButton.isHidden = true
                                    self.bleedingLabel.text = "Bleeding Tank Empty. Please Refill."
                                    //}
                                } else {
                                    self.bleedingSwitch.isEnabled = true
                                    self.flowRateSlider.isEnabled = true
                                    //self.calibrateBleedingButton.isHidden = false
                                    self.calibrateBleedingButton.isEnabled = true
                                    self.calibrateBleedingButton.alpha = 1
                                    self.bleedingSwitch.isHidden = false
                                    self.bleedingLabel.text = "Bleeding"
                                }
                            }
                            
                            if let bleedingHigh = patientDiagnosisDictionary[bleedMaximumFlowRatePath] as? Float, let bleedingLow = patientDiagnosisDictionary[bleedMinimumFlowRatePath] as? Float {
                               // if (bleedingHigh != settings.maximumFlowRate || bleedingLow != settings.minimumFlowRate) && bleedingHigh != -1 && bleedingLow != -1 {
                                    print("Bleed calibration complete.")
                                    
                                    if bleedingState == 2 {
                                        databaseReference.child(modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath).setValue(0)
                                    }
                                    
                                    //Bleeding.getMinimumMaximumBleedFlowRate(completion: {
                                        print("minimumFlowRate: \(bleedingHigh), maximumFlowRate: \(bleedingLow)")
                                        self.flowRateSlider.maximumValue = bleedingHigh
                                        self.flowRateSlider.minimumValue = bleedingLow
                                        self.flowRateSlider.value = self.flowRateSlider.maximumValue
                                        self.flowRateSliderDidChange(self.flowRateSlider)
                                        //self.flowRateSliderDidTouchUpInside(self.flowRateSlider)
                                        self.calibrateBleedingButton.hideLoading()
                                   // })
                             //   }
                                
                               /* if (bleedingHigh == -1 || bleedingLow == -1) && (settings.maximumFlowRate == 0 || settings.maximumFlowRate == 0) {
                                    print("No bleeding settings located locally and in the cloud. Forcing calibration.")
                                    self.bleedingSwitch.isOn = false
                                    self.bleedingSwitch.isEnabled = false
                                    self.bleedingSwitchDidChange(self.bleedingSwitch)
                                    self.flowRateSlider.isEnabled = false
                                    self.calibrateBleedingButton.isEnabled = true
                                    //anim {
                                    self.bleedingSwitch.isHidden = true
                                    self.bleedingLabel.text = "Please Calibate Bleeding."
                                    //}
                                } */
                            }
                        }
                    }
                    if let flowRate = bleedingDictionary[bleedingFlowRatePath] as? Float {
                        self.flowRateSlider.value = flowRate
                        self.flowRateSliderDidChange(self.flowRateSlider)
                    }
                }
                
                if let lineShatteringState = (modules[patientMasterPath] as? NSDictionary)?[lineShatteringPath] as? Int {
                    self.lineShatteringSwitch.isOn = lineShatteringState > 1 ? true : false
                    self.lineShatteringSegmentedControl.selectedSegmentIndex = lineShatteringState <= 2 ? 0 : 1
                }
                
                if let pumpState = (modules[patientMasterPath] as? NSDictionary)?[pumpPath] as? Int {
                    self.pumpSwitch.isOn = (pumpState > 0)
                    
                    self.pumpSegmentedControl.isEnabled = self.pumpSwitch.isOn
                    self.pumpSegmentedControl.alpha = self.pumpSwitch.isOn ? 1 : disabledFieldAlpha
                    
                    switch pumpState {
                    case 1:
                        self.pumpSegmentedControl.selectedSegmentIndex = 0
                    case 2:
                        self.pumpSegmentedControl.selectedSegmentIndex = 1
                    default:
                        self.pumpSegmentedControl.selectedSegmentIndex = 0
                    }
                }
                
                // MARK: - Oxygenator modules
                if let oxygenatorNoiseState = (modules[oxygenatorMasterPath] as? NSDictionary)?[oxygenatorNoisePath] as? Int {
                    self.oxygenatorNoiseSwitch.isOn = oxygenatorNoiseState > 0
                    self.volumeSlider.value = Float(oxygenatorNoiseState)
                    self.volumeLabel.text = "Sound Level \(Int(self.volumeSlider.value / OxygenatorNoise.volumeRange.upperBound * 100))%"
                    if self.volumeSlider.value > 0 {
                        self.lastVolume = self.volumeSlider.value
                    }
                    self.volumeSlider.isEnabled = self.oxygenatorNoiseSwitch.isOn
                    self.volumeSlider.alpha = self.oxygenatorNoiseSwitch.isOn ? 1.0 : disabledFieldAlpha
                }
                
                // MARK: - Heater modules
                if let deoxygenationState = (modules[heaterMasterPath] as? NSDictionary)?[deoxygenationPath] as? Int {
                    self.deoxygenationSwitch.isOn = deoxygenationState == 0 ? false : true
                }
                
                // MARK: - Other modules
                if let powerDisconnection = modules[powerDisconnectionPath] as? NSDictionary {
                    print("Power disconnection:")
                    debugPrint(powerDisconnection)
                    self.powerDisconnectionSwitch.isOn = true
                    
                    if let batteryLevel = powerDisconnection["batteryLevel"] as? Float, let fullBatteryLife = powerDisconnection["fullBatteryLife"] as? Int {
                        self.batteryLevelSegmentedControl.selectedSegmentIndex = PowerDisconnection.getIndexFrom(value: batteryLevel)
                        self.batteryLevelSegmentedControl.isEnabled = self.powerDisconnectionSwitch.isOn
                        self.batteryLevelSegmentedControl.alpha = self.powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
                        
                        self.timeRemainingSlider.isEnabled = self.powerDisconnectionSwitch.isOn
                        self.timeRemainingSlider.alpha = self.powerDisconnectionSwitch.isOn ? 1 : disabledFieldAlpha
                        self.timeRemainingSlider.value = Float(fullBatteryLife) * batteryLevel
                        self.timeRemainingLabel.text = "Remaining " + String(Int(self.timeRemainingSlider.value))
                    }
                } else {
                    self.powerDisconnectionSwitch.isOn = false
                    //self.powerDisconnectionSwitchDidChange(self.powerDisconnectionSwitch)
                }
                
                if let message = (modules[displayMessagePath] as? [String: Any]) {
                    //anim {
                    self.removeMessageButton.isHidden = false
                    //}
                    
                    self.displayMessageSwitch.isOn = true
                    //                    self.sendMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    //                    self.saveMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    if let alarm = message["alarm"] as? Bool {
                        self.alarmSwitch.isOn = alarm
                    }
                    
                    if let messageString = message["message"] as? String {
                        self.messageField.text = messageString
                    }
                    
                    if let barMessageString = message["barMessage"] as? String {
                        self.barMessageField.text = barMessageString
                    }
                    
                    if let priority = message["priority"] as? Int {
                        self.prioritySegmentedControl.selectedSegmentIndex = (priority - 1)
                    }
                } else {
                    //anim {
                    self.removeMessageButton.isHidden = true
                    //}
                    self.displayMessageSwitch.isOn = false
                    //                    self.sendMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    //                    self.saveMessageButton.isEnabled = self.displayMessageSwitch.isOn
                    self.alarmSwitch.isOn = false
                    //self.messageField.text = nil
                    //self.barMessageField.text = nil
                }
            }
        }
    }
    
    // MARK: CONTROL
    
    // MARK: MASTER
    @IBAction func turnOffAllModules(_ sender: UIButton) {
        pumpSwitch.isOn = false
        bleedingSwitch.isOn = false
        powerDisconnectionSwitch.isOn = false
        lineShatteringSwitch.isOn = false
        oxygenatorNoiseSwitch.isOn = false
        deoxygenationSwitch.isOn = false
        displayMessageSwitch.isOn = false
        clottingSwitch.isOn = false
        
        powerDisconnectionSwitchDidChange(powerDisconnectionSwitch)
        pumpSwitchDidChange(pumpSwitch)
        bleedingSwitchDidChange(bleedingSwitch)
        lineShatteringSwitchDidChange(lineShatteringSwitch)
        oxygenatorNoiseSwitchDidChange(oxygenatorNoiseSwitch)
        deoxygenationSwitchDidChange(deoxygenationSwitch)
        displayMessageSwitchDidChange(displayMessageSwitch)
        clottingSwitchDidChange(clottingSwitch)
    }
    
    // MARK: BLEEDING
    @IBAction func bleedingSwitchDidChange(_ sender: UISwitch) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath
        let value = sender.isOn ? 1 : 0
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
//        flowRateSlider.isEnabled = sender.isOn
//        flowRateSlider.alpha = sender.isOn ? 1.0 : disabledFieldAlpha
    }
    
    @IBAction func flowRateSliderDidChange(_ slider: UISlider) {
        //if bleedingSwitch.isOn {
        //}
        flowRateLabel.text = "Flow \(String(format: Vformat, slider.value)) " + flowRateUnit
    }
    
    @IBAction func flowRateSliderDidTouchUpInside(_ sender: Any) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + bleedingFlowRatePath
        let value = flowRateSlider.value
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    @IBAction func flowRateSliderDidTouchUpOutside(_ sender: Any) {
        flowRateSliderDidTouchUpInside(sender)
    }
    

    @IBAction func calibrateBleeding(_ sender: ModuleButton) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath
        sender.showLoading()

        if couchDBEnabled {
            couchDBclient.update(path: path, value: 2)
            Timer.after(10, {
                sender.hideLoading()
                // TODO: make observe single value in couch db library
                // TODO: write read at a specific node method
            })
        } else {
            databaseReference.child(path).setValue(2)
            Timer.after(10, {
                sender.hideLoading()
                databaseReference.child(modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath).observeSingleEvent(of: .value, with: { (snapshot) in
                    debugPrint(snapshot)
                    if let bleedingState = snapshot.value as? Float {
                        if bleedingState == 2 {
                            databaseReference.child(modulesMasterPath + "/" + patientMasterPath + "/" + bleedingPath).setValue(0)
                        }
                    }
                })
            })
        }
    }
    
    // MARK: OXYGENATOR NOISE
    @IBAction func oxygenatorNoiseSwitchDidChange(_ sender: UISwitch) {
        if Int(lastVolume) <= 0 {
            lastVolume = OxygenatorNoise.volumeRange.upperBound
        }
        
        let path = modulesMasterPath + "/" + oxygenatorMasterPath + "/" + oxygenatorNoisePath
        let value: Int = sender.isOn ? Int(lastVolume) : 0
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
        
        volumeSlider.isEnabled = sender.isOn
        volumeSlider.alpha = sender.isOn ? 1.0 : disabledFieldAlpha
    }
    
    @IBAction func volumeSliderValueDidChange(_ sender: Any) {
        if oxygenatorNoiseSwitch.isOn {
            volumeLabel.text = "Sound Level \(Int(volumeSlider.value / OxygenatorNoise.volumeRange.upperBound * 100))%"
            if volumeSlider.value > 0 {
                lastVolume = volumeSlider.value
            }
        }
    }
    
    @IBAction func volumeSliderDidTouchUpInside(_ sender: Any) {
        let path = modulesMasterPath + "/" + oxygenatorMasterPath + "/" + oxygenatorNoisePath
        let value: Int = Int(volumeSlider.value)
        
        if couchDBEnabled {
            if oxygenatorNoiseSwitch.isOn {
                couchDBclient.update(path: path, value: value)
            }
        } else {
            if oxygenatorNoiseSwitch.isOn {
                databaseReference.child(path).setValue(value)
            }
        }
    }
    
    @IBAction func volumeSliderDidTouchUpOutside(_ sender: Any) {
        volumeSliderDidTouchUpInside(sender)
    }
    
    // MARK: DEOXYGENATION
    @IBAction func deoxygenationSwitchDidChange(_ sender: UISwitch) {
        let path = modulesMasterPath + "/" + heaterMasterPath + "/" + deoxygenationPath
        let value = sender.isOn ? 1 : 0
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    // MARK: POWER DISCONNECTION
    @IBAction func powerDisconnectionSwitchDidChange(_ sender: UISwitch) {
        let path1 = modulesMasterPath + "/" + powerDisconnectionPath + "/batteryLevel"
        let path2 = modulesMasterPath + "/" + powerDisconnectionPath + "/fullBatteryLife"
        let path3 = modulesMasterPath + "/" + powerDisconnectionPath
        let value1 = PowerDisconnection.getBatteryLevel(index: batteryLevelSegmentedControl.selectedSegmentIndex)
        let value2 = Int(batteryLevelSegmentedControl.selectedSegmentIndex != 0 ? timeRemainingSlider.value / PowerDisconnection.getBatteryLevel(index: batteryLevelSegmentedControl.selectedSegmentIndex) : 0)
        let value3: String? = nil
        
        if couchDBEnabled {
            if sender.isOn {
                couchDBclient.update(path: path1, value: value1)
                couchDBclient.update(path: path2, value: value2)
            } else {
                couchDBclient.update(path: path3, value: value3)
            }
        } else {
            if sender.isOn {
                databaseReference.child(path1).setValue(value1)
                databaseReference.child(path2).setValue(value2)
            } else {
                databaseReference.child(path3).setValue(value3)
            }
        }
        
        batteryLevelSegmentedControl.isEnabled = sender.isOn
        batteryLevelSegmentedControl.alpha = sender.isOn ? 1 : disabledFieldAlpha
        
        timeRemainingSlider.isEnabled = sender.isOn
        timeRemainingSlider.alpha = sender.isOn ? 1 : disabledFieldAlpha
    }
    
    @IBAction func batteyLevelSegmentedControlDidChange(_ sender: UISegmentedControl) {
        let path1 = modulesMasterPath + "/" + powerDisconnectionPath + "/batteryLevel"
        let path2 = modulesMasterPath + "/" + powerDisconnectionPath + "/fullBatteryLife"
        let value1 = PowerDisconnection.getBatteryLevel(index: sender.selectedSegmentIndex)
        let value2 = Int(batteryLevelSegmentedControl.selectedSegmentIndex != 0 ? timeRemainingSlider.value / PowerDisconnection.getBatteryLevel(index: batteryLevelSegmentedControl.selectedSegmentIndex) : 0)
        
        if couchDBEnabled {
            couchDBclient.update(path: path1, value: value1)
            couchDBclient.update(path: path2, value: value2)
        } else {
            databaseReference.child(path1).setValue(value1)
            databaseReference.child(path2).setValue(value2)
        }
    }
    
    @IBAction func timeRemainingSliderValueDidChange(_ slider: UISlider) {
        timeRemainingLabel.text = "Remaining " + String(Int(slider.value))
    }
    
    @IBAction func timeRemainingSliderDidTouchUpInside(_ slider: UISlider) {
        let path = modulesMasterPath + "/" + powerDisconnectionPath + "/fullBatteryLife"
        let value = Int(batteryLevelSegmentedControl.selectedSegmentIndex != 0 ? slider.value / PowerDisconnection.getBatteryLevel(index: batteryLevelSegmentedControl.selectedSegmentIndex) : 0)
        
        if couchDBEnabled {
            if powerDisconnectionSwitch.isOn {
                couchDBclient.update(path: path, value: value)
            }
        } else {
            if powerDisconnectionSwitch.isOn {
                databaseReference.child(path).setValue(value)
            }
        }
    }
    
    @IBAction func timeRemainingSliderDidTouchUpOutside(_ slider:UISlider) {
        timeRemainingSliderDidTouchUpInside(slider)
    }
    
    
    // MARK: LINE SHATTERING
    @IBAction func lineShatteringSwitchDidChange(_ sender: UISwitch) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + lineShatteringPath
        let value = sender.isOn ? lineShatteringSegmentedControl.selectedSegmentIndex == 0 ? 2 : 3 : 0
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    @IBAction func lineShatteringSegmentedControlDidChange(_ sender: UISegmentedControl) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + lineShatteringPath
        let value = sender.selectedSegmentIndex == 0 ? 2 : 3
        
        if couchDBEnabled {
            if lineShatteringSwitch.isOn {
                couchDBclient.update(path: path, value: value)
            }
        } else {
            if lineShatteringSwitch.isOn {
                databaseReference.child(path).setValue(value)
            }
        }
    }
    
    @IBAction func centerLineShattering(_ sender: ModuleButton) {
        sender.showLoading()
        Timer.after(2, {
            sender.hideLoading()
        })
        
        let path1 = modulesMasterPath + "/" + patientMasterPath + "/" + lineShatteringPath
        let path2 = modulesMasterPath + "/" + patientMasterPath + "/" + lineShatteringPath
        let value1 = 1
        let value2 = 0
        
        if couchDBEnabled {
            couchDBclient.update(path: path1, value: value1)
            Timer.after(10, {
                couchDBclient.update(path: path2, value: value2)
            })
        } else {
            databaseReference.child(path1).setValue(value1)
            Timer.after(10, {
                databaseReference.child(path2).setValue(value2)
            })
        }
    }
    
    // MARK: DISPLAY MESSAGE
    @IBAction func displayMessageSwitchDidChange(_ sender: UISwitch) {
        //sendMessageButton.isEnabled = sender.isOn
        //saveMessageButton.isEnabled = displayMessageSwitch.isOn
        let path = modulesMasterPath + "/" + displayMessagePath
        let value: String? = nil
        
        if couchDBEnabled {
            if !sender.isOn {
                couchDBclient.update(path: path, value: value)
            }
        } else {
            if !sender.isOn {
                databaseReference.child(path).setValue(value)
            }
        }
    }
    
    @IBAction func displayMessageMessageFieldDidBeginEditing(_ sender: Any) {
        if !displayMessageSwitch.isOn {
            displayMessageSwitch.isOn = true
        }
    }
    
    @IBAction func displayBarMessageMessageFieldDidBeginEditing(_ sender: Any) {
        if !displayMessageSwitch.isOn {
            displayMessageSwitch.isOn = true
        }
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        let message = DisplayMessage()
        message.alarm = alarmSwitch.isOn
        message.priority = prioritySegmentedControl.selectedSegmentIndex + 1
        message.message = (messageField.text?.isEmpty)! ? nil : messageField.text
        message.barMessage = (barMessageField.text?.isEmpty)! ? nil : barMessageField.text
        
        sender.backgroundColor = sendMessageHighlightColor
        sender.setTitleColor(UIColor.white, for: .normal)
        sender.backgroundColor = UIColor.white
        sender.setTitleColor(UIColor.black, for: .normal)
        
        let path = modulesMasterPath + "/" + displayMessagePath
        let value = message.dictionaryForRemote()
    
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    func getMessageTitle(_ message: DisplayMessage) -> String
    {
        var title = ""
        if message.message != nil && message.message != "nil" {
            title = message.message!
        } else if  message.barMessage != nil && message.barMessage != "nil" {
            title = message.barMessage!
        } else {
            title = message.priority == 1 ? "Red Alert" : "Yellow Alert"
        }
        return title
    }
    
    func getMessageDetails(_ message: DisplayMessage) -> String?
    {
        var detail: String? = ""
        detail = message.alarm ? "alarm on" : "alarm off"
        if (message.message != nil && message.message != "nil") || (message.barMessage != nil && message.barMessage != "nil") {
            detail?.append(", " + (message.priority == 1 ? "red alert" : "yellow alert"))
        }
        return detail
    }
    
    @IBAction func saveMessage(_ sender: Any) {
        let message = DisplayMessage()
        message.message = messageField.text != nil ? messageField.text : "nil"
        message.barMessage = barMessageField.text != nil ? barMessageField.text : "nil"
        message.alarm = alarmSwitch.isOn
        message.priority = prioritySegmentedControl.selectedSegmentIndex + 1
        
        try! realm.write {
            realm.add(message)
        }
        
        messageTableView.reloadData()
    }
    
    @IBAction func removeMessage(_ sender: Any) {
        let path = modulesMasterPath + "/" + displayMessagePath
        let value: String? = nil
            
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    // MARK: PUMP
    @IBAction func pumpSwitchDidChange(_ sender: UISwitch) {
        pumpSegmentedControl.isEnabled = pumpSwitch.isOn
        pumpSegmentedControl.alpha = pumpSwitch.isOn ? 1 : disabledFieldAlpha
        let speed = pumpSegmentedControl.selectedSegmentIndex + 1
        
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + pumpPath
        let value = sender.isOn ? speed : 0
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    @IBAction func pumpSegmentedControlDidChange(_ sender: Any) {
        let path = modulesMasterPath + "/" + patientMasterPath + "/" + pumpPath
        let value = pumpSegmentedControl.selectedSegmentIndex + 1
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: value)
        } else {
            databaseReference.child(path).setValue(value)
        }
    }
    
    
    // MARK: MESSAGES TABLE VIEW DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realm.objects(DisplayMessage.self).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath)
        let message = realm.objects(DisplayMessage.self)[indexPath.row]
        cell.textLabel?.text = getMessageTitle(message)
        cell.detailTextLabel?.text = getMessageDetails(message)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        displayMessageSwitch.isOn = true
//        sendMessageButton.isEnabled = displayMessageSwitch.isOn
//        saveMessageButton.isEnabled = displayMessageSwitch.isOn
        
        let message = realm.objects(DisplayMessage.self)[indexPath.row]
        self.alarmSwitch.isOn = message.alarm
        self.messageField.text =  message.message != nil && message.message != "nil" ? message.message : nil
        self.barMessageField.text = message.barMessage != nil && message.barMessage != "nil" ? message.barMessage : nil
        self.prioritySegmentedControl.selectedSegmentIndex = (message.priority - 1)
    }
    
    // MARK: CLOTTING
    @IBAction func clottingSwitchDidChange(_ sender: UISwitch) {
        let path = modulesMasterPath + "/" + oxygenatorMasterPath + "/" + clottingPath
        var clottingDictionary = clotting.dictionary()
        clottingDictionary.removeValue(forKey: "duration")
        if !sender.isOn {
            clottingDictionary["M"] = "000000000000000000000000000000"
        }
        
        if couchDBEnabled {
            couchDBclient.update(path: path, value: clottingDictionary)
        } else {
            databaseReference.child(path).setValue(clottingDictionary)
        }
    }
    
    func updateClotting(updateCloud: Bool = true) {
        try! realm.write {
            // Clot color
            clotting.red = Int(clottingCellActivatedColor.redComponent * 255)
            clotting.green = Int(clottingCellActivatedColor.greenComponent * 255)
            clotting.blue = Int(clottingCellActivatedColor.blueComponent * 255)
            clotting.brightness = Int(brightnessSlider.value * 255)
            clotting.normalizedColorSpectrumX = Float(clotColorPickerPosition.x / self.colorWheel.bounds.width)
            clotting.normalizedColorSpectrumY = Float(clotColorPickerPosition.y / self.colorWheel.bounds.height)
            
            // Default color
            clotting.defaultRed = Int(clottingCellDeactivatedColor.redComponent * 255)
            clotting.defaultGreen = Int(clottingCellDeactivatedColor.greenComponent * 255)
            clotting.defaultBlue = Int(clottingCellDeactivatedColor.blueComponent * 255)
            clotting.defaultNormalizedColorSpectrumX = Float(defaultColorPickerPosition.x / self.colorWheel.bounds.width)
            clotting.defaultNormalizedColorSpectrumY = Float(defaultColorPickerPosition.y / self.colorWheel.bounds.height)
            
            clotting.LEDMatrix = Clotting.LEDMatrixString(from: Clotting.LEDMatrix(from: matrixCollectionView))
            
            if updateCloud {
                clottingSwitchDidChange(clottingSwitch)
            }
        }
    }
    
    @IBAction func saveClotting(_ sender: Any) {
        try! realm.write {
            realm.add(clotting, update: .all)
        }
    }
    
    func applyClotting(from clotting: Clotting) {
        debugPrint(clotting)
        matrixCollectionView.reloadData()
        
        let x = CGFloat(clotting.normalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
        let y = CGFloat(clotting.normalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
        
        let defaultX = CGFloat(clotting.defaultNormalizedColorSpectrumX) * CGFloat(self.colorWheel.bounds.width)
        let defaultY = CGFloat(clotting.defaultNormalizedColorSpectrumY) * CGFloat(self.colorWheel.bounds.height)
        
        clotBrightness = Float(clotting.brightness) / 255.0
        
        clotColorPickerPosition = CGPoint(x: x, y: y)
        defaultColorPickerPosition = CGPoint(x: defaultX, y: defaultY)
        
        clottingCellActivatedColor = UIColor(red: CGFloat(clotting.red) / 255, green: CGFloat(clotting.green) / 255, blue: CGFloat(clotting.blue) / 255, alpha: 1)
        clottingCellDeactivatedColor = UIColor(red: CGFloat(clotting.defaultRed) / 255, green: CGFloat(clotting.defaultGreen) / 255, blue: CGFloat(clotting.defaultBlue) / 255, alpha: 1)
        lightClottingMatrix(from: clotting.LEDMatrix)
        colorWheel.position = clottingSegmentedControl.selectedSegmentIndex == 0 ? clotColorPickerPosition : defaultColorPickerPosition
        
        brightnessSlider.value = clotBrightness
        brightnesseSliderValueDidChange(brightnessSlider)
    }
    
    @IBAction func newClotting(_ sender: Any) {
        clotting = Clotting()
        matrixCollectionView.reloadData()
        clotColorPickerPosition = CGPoint(x: colorWheel.bounds.width / 2, y: 5)
        
        defaultColorPickerPosition = CGPoint(x: colorWheel.bounds.width, y: 5)
        colorWheel.position = clottingSegmentedControl.selectedSegmentIndex == 0 ? clotColorPickerPosition : clotColorPickerPosition
        
        clottingCellDeactivatedColor = UIColor.black
        clottingCellActivatedColor = UIColor.red
        brightnessSlider.value = brightnessSlider.maximumValue
    }
    
    
    // MARK: CLOTTING COLOR PICKER
    @IBAction func colorWheelValueDidChange(_ sender: HSBASpectum) {
        
        if clotMode {
            clotColorPickerPosition = sender.position
            clottingCellActivatedColor = sender.color
            
            if let selectedIndexPaths = matrixCollectionView.indexPathsForSelectedItems {
                for indexPath in selectedIndexPaths {
                    matrixCollectionView.tag = 1
                    collectionView(matrixCollectionView, didSelectItemAt: indexPath)
                }
            }
        } else {
            defaultColorPickerPosition = sender.position
            clottingCellDeactivatedColor = sender.color
            
            for index in 0...matrixCollectionView.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(row: index, section: 0)
                guard let selectedIndexPaths = matrixCollectionView.indexPathsForSelectedItems
                    else { return }
                
                if !selectedIndexPaths.contains(indexPath) {
                    matrixCollectionView.tag = 1
                    collectionView(self.matrixCollectionView, didDeselectItemAt: indexPath)
                }
            }
        }
    }
    
    @IBAction func colorWheelTouchDown(_ sender: Any) {
        print("Selecting color drag did start.")
        self.scrollView.isScrollEnabled = false
    }
    
    @IBAction func colorWheelTouchUpInside(_ sender: Any) {
        print("Selecting color drag did end.")
        self.scrollView.isScrollEnabled = true
        updateClotting()
    }
    
    @IBAction func turnOffClottingMatrix(_ sender: Any) {
        matrixCollectionView.reloadData()
        updateClotting()
    }
    
    func lightClottingMatrix(from LEDMatrixString: String) {
        let LEDMatrix = Clotting.LEDMatrix(from: LEDMatrixString)
        for (index, item) in LEDMatrix.enumerated() {
            if item == 1 {
                let indexPath = IndexPath(row: index, section: 0)
                self.matrixCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.init(rawValue: 0))
                self.collectionView(self.matrixCollectionView, didSelectItemAt: indexPath)
            }
        }
    }
    
    @IBAction func switchClottingMode(_ sender: UISegmentedControl) {
        // Switches matrix color selection from clotting to default color and vice versa
        clotMode = sender.selectedSegmentIndex == 0 ? true : false
        //anim {
            self.colorWheel.position = self.clotMode ? self.clotColorPickerPosition : self.defaultColorPickerPosition
            // self.colorWheelValueDidChange(self.colorWheel)
        //}
    }
    
    // MARK: MATRIX COLLECTION VIEW DATASOURCE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfClottingMatrixCells
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "clottingCell", for: indexPath)
        cell.layer.cornerRadius = clottingCellCornerRadius
        cell.layer.masksToBounds = true
        cell.backgroundColor = cell.isSelected ? clottingCellActivatedColor : clottingCellDeactivatedColor
        return cell
    }
    
    // MARK: MATRIX COLLECTION VIEW DELEGATE
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.backgroundColor = clottingCellActivatedColor
        }
        if collectionView.tag != 1 {
            updateClotting()
        } else {
            updateClotting(updateCloud: false)
        }
        collectionView.tag = 0
        
        print("Selected \(indexPath.row)")
        print(Clotting.LEDMatrix(from: collectionView, inverse: false))
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            cell.backgroundColor = clottingCellDeactivatedColor
        }
        if collectionView.tag != 1 {
            updateClotting()
        } else {
            updateClotting(updateCloud: false)
        }
        collectionView.tag = 0
        
        print("Deselected \(indexPath.row)")
        print(Clotting.LEDMatrix(from: collectionView, inverse: false))
    }
    
    @IBAction func brightnesseSliderValueDidChange(_ slider: UISlider) {
        clotBrightness = slider.value
        brightnessLabel.text = "Brightness \(Int(slider.value * 100))%"
    }
    
    @IBAction func brightnessSliderDidTouchUpInside(_ sender: Any) {
        updateClotting()
    }
    
    @IBAction func brightnessSliderDidTouchUpOutside(_ sender: Any) {
        brightnessSliderDidTouchUpInside(sender)
    }
    
    // MARK: COLORS
    func setupBackgrounds() {
        bleedingView.backgroundColor = gradientFrom(color: UIColor(hex: Bleeding().tintColorString!), frame: bleedingView.frame)
        oxygenatorNoiseView.backgroundColor = gradientFrom(color: UIColor(hex: OxygenatorNoise().tintColorString!), frame: oxygenatorNoiseView.frame)
        deoxygenationView.backgroundColor = gradientFrom(color: UIColor(hex: Deoxygenation().tintColorString!), frame: deoxygenationView.frame)
        powerDisconnectionView.backgroundColor = gradientFrom(color: UIColor(hex: PowerDisconnection().tintColorString!), frame: powerDisconnectionView.frame)
        lineShatteringView.backgroundColor = gradientFrom(color: UIColor(hex: LineShattering().tintColorString!), frame: lineShatteringView.frame)
        displayMessageView.backgroundColor = gradientFrom(color: UIColor(hex: DisplayMessage().tintColorString!), frame: displayMessageView.frame)
        pumpView.backgroundColor = gradientFrom(color: pumpModuleBackgroundColor, frame: pumpView.frame)
        clottingView.backgroundColor = gradientFrom(color: UIColor(hex: Clotting().tintColorString!), frame: clottingView.frame)
        matrixCollectionView.backgroundColor = clottingView.backgroundColor
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loadClottingMatrixPatterns" {
            if let destination = segue.destination as? ClottingPatternViewController {
                destination.delegate = self
            }
        }
    }
    
    // MARK: POPOVER DELEGATE
    func popoverDismissed() {
        print("Pattern view controller did dismiss.")
        clotting = currentClotting
    }
    
    // MARK: ORIENTATION CHANGES
    func adaptToOrientation() {
        print("Adapting layout to orientation...")
        switch UIApplication.shared.statusBarOrientation {
        case .portrait, .portraitUpsideDown:
            print("Switched to portrait orientation.")
            dualStackView.axis = .vertical
            clottingDualStackView.axis = .vertical
            clottingDualStackView.alignment = .center
            clottingDualStackView.distribution = .fillProportionally
            displayMessageStackView.axis = .vertical
        case  .landscapeLeft, .landscapeRight:
            print("Switched to landscape orientation.")
            dualStackView.axis = .horizontal
            clottingDualStackView.axis = .horizontal
            clottingDualStackView.alignment = .center
            displayMessageStackView.axis = .horizontal
        default:
            break
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        print("Will rotate device...")
        if toInterfaceOrientation.isPortrait {
            dualStackView.axis = .vertical
            displayMessageStackView.axis = .vertical
            clottingDualStackView.axis = .vertical
            clottingDualStackView.alignment = .center
        } else {
            dualStackView.axis = .horizontal
            clottingDualStackView.axis = .horizontal
            clottingDualStackView.alignment = .fill
            clottingDualStackView.distribution = .fillProportionally
            displayMessageStackView.axis = .horizontal
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        print("Did rotate device...")
        setupBackgrounds()
        setupScrollViewContentSize()
    }
}













