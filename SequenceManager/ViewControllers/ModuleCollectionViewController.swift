//
//  ModuleCollectionViewController.swift
//  SequenceManager
//
//  Created by Abdol on 3/16/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Hue

private let reuseIdentifier = "moduleCell"

class ModuleCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var isSuccessFailureAction: Bool? = nil
    var parallelModuleParentIndex: Int?
    
    override func viewDidLoad() {
        self.title = "Select Module"
        if isSuccessFailureAction != nil {
            self.view.backgroundColor = waitForActionBackgroundColor
            self.collectionView?.backgroundColor = waitForActionBackgroundColor
        }
        
        self.popoverPresentationController?.delegate = self.popoverPresentationController?.presentingViewController as? UIPopoverPresentationControllerDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Enable swipe back gesture
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = Module.subclasses().count
        
        if !clottingEnabled {
           count -= 1
        }
        
        if isSuccessFailureAction != nil {
            return count - 1
        } else if parallelModuleParentIndex != nil {
            return count - 4
        } else {
            return count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ModuleCollectionViewCell
        let module = { () -> Module in
            
            var modules = Module.subclasses() as! [Module]
            if !clottingEnabled {
                modules = modules.filter({ $0.label != "Clotting" })
            }
            
            if self.isSuccessFailureAction != nil {
                return modules.filter({ $0.label != "Wait for Action" })[indexPath.row]
            } else if self.parallelModuleParentIndex != nil {
                return modules.filter({ $0.label != "Wait for Action" && $0.label != "Stop Simulation" && $0.label != mainQueue.moduleAt(index: self.parallelModuleParentIndex!)?.label && $0.label != "Delay" })[indexPath.row]
            } else {
               return modules[indexPath.row]
            }
        }()
        
        cell.frame = CGRect(origin: cell.frame.origin, size: CGSize(width: moduleCellWidth, height: moduleCellHeight))
        cell.contentView.frame = CGRect(origin: cell.contentView.frame.origin, size: CGSize(width: moduleCellWidth, height: moduleCellHeight))
        cell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
        cell.moduleLabel.text = module.label
        cell.moduleLabel.textColor = (UIColor.black.isContrasting(with: cell.backgroundColor!)) ? UIColor.black : UIColor.white
        cell.layer.cornerRadius = moduleCornerRadius
        cell.layer.masksToBounds = true
        cell.layoutSubviews()
        cell.updateConstraints()
        return cell
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */
    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = self.collectionView?.cellForItem(at: indexPath) as! ModuleCollectionViewCell
        if let moduleLabel = selectedCell.moduleLabel.text {
            switch moduleLabel {
            case "Stop Simulation":
                if isSuccessFailureAction == nil {
                    self.performSegue(withIdentifier: "addToSequence", sender: self)
                } else {
                    self.performSegue(withIdentifier: "addToWaitForAction", sender: self)
                }
            case "Change Parameters":
                self.performSegue(withIdentifier: "configureChangeParameters", sender: self)
            case "Wait for Action":
                self.performSegue(withIdentifier: "configureWaitForAction", sender: self)
            default:
                self.performSegue(withIdentifier: "configureModule", sender: self)
            }
        }
    }
    
    // MARK: LAYOUT DELEGATE
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: moduleCellWidth, height: moduleCellHeight)
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Segueing from module selector...")
        var module: Module?
        if let indexPath = self.collectionView?.indexPathsForSelectedItems?.first {
            let selectedCell = self.collectionView?.cellForItem(at: indexPath) as! ModuleCollectionViewCell
            if let moduleLabel = selectedCell.moduleLabel.text {
                module = Module.moduleFromLabel(moduleLabel: moduleLabel) as? Module
            }
        }
        
        switch segue.identifier {
        case "configureModule"?:
            if let indexPath = self.collectionView?.indexPathsForSelectedItems?.first {
                let selectedCell = self.collectionView?.cellForItem(at: indexPath) as! ModuleCollectionViewCell
                if let moduleLabel = selectedCell.moduleLabel.text {
                    if let destination = segue.destination as? ModuleConfigureViewController {
                        destination.selectedModuleLabel = moduleLabel
                        destination.isSuccessFailureAction = isSuccessFailureAction
                        destination.parallelModuleParentIndex = parallelModuleParentIndex
                    }
                }
            }
        case "addToSequence"?:
            mainQueue.addToQueue(module: module!)
            if let destination = segue.destination as? SequenceCollectionViewController {
                destination.justAdded = true
            }
        case "addToWaitForAction"?:
            if isSuccessFailureAction == true {
                mainQueue.addToSuccessQueue(module: module!, waitForAction: currentWaitForAction)
            } else if isSuccessFailureAction == false {
                mainQueue.addToFailureQueue(module: module!, waitForAction: currentWaitForAction)
            }
            if let destination = segue.destination as? WaitForActionViewController {
                destination.justAdded = true
            }
        case "configureChangeParameters"?:
            if let destination = segue.destination as? ParameterControlViewController {
                destination.isSuccessFailureAction = isSuccessFailureAction
                destination.parallelModuleParentIndex = parallelModuleParentIndex
            }
        default:
            break
        }
    }
}
