//
//  WaitForActionViewController.swift
//  SequenceManager
//
//  Created by Abdol on 3/22/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import RealmSwift

internal var currentWaitForAction = WaitForAction()

class WaitForActionViewController: UIViewController, UICollectionViewDelegate, KDDragAndDropCollectionViewDataSource {

    let reuseIdentifier = "blockCell"
    var dragAndDropManager : KDDragAndDropManager?
    var dataSource = List<AnyModule>()
    var editModuleIndex: Int?
    var editMode = false
    var justAdded = false
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var sequenceCollectionView: UICollectionView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var actionField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.tintColor = successTintColor
        let font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight(rawValue: 2))
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font],
                                                for: .normal)
        
        if editModuleIndex == nil {
            currentWaitForAction = WaitForAction()
        } else {
            setupEditMode()
        }
        dataSource = currentWaitForAction.successModules
        
        self.dragAndDropManager = KDDragAndDropManager(canvas: self.sequenceCollectionView, collectionViews: [sequenceCollectionView])
        self.title = "Configure Wait For Action"
        self.view.backgroundColor = gradientFrom(color: waitForActionBackgroundColor, frame: self.view.frame)
        sequenceCollectionView.backgroundColor = self.view.backgroundColor
        
        let editButton = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(WaitForActionViewController.toggleEditMode))
        self.navigationItem.rightBarButtonItem = editButton
        
        descriptionLabel.text = currentWaitForAction.descriptionString
        
        // Action field setup
        let textColor = (UIColor.black.isContrasting(with: self.view.backgroundColor!)) ? UIColor.black : UIColor.white
        fieldBackgroundColor = UIColor(hex: currentWaitForAction.tintColorString!).add(hue: 0, saturation: -0.3, brightness: 0, alpha: 0)
        actionField.backgroundColor = fieldBackgroundColor
        let actionPlaceholder = NSAttributedString(string: "enter trainee's action", attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColorAlt])
        actionField.attributedPlaceholder = actionPlaceholder
        actionField.textColor = textColor
        descriptionLabel.textColor = textColor
        actionLabel.textColor = textColor
        
        if currentWaitForAction.actionLabel != nil {
            actionField.text = currentWaitForAction.actionLabel!
        }
        addDoneButtonOnKeyboard(actionField, target: self, action: #selector(WaitForActionViewController.doneButtonAction))
        
        // Add button setup
        addButton.setTitleColor(moduleButtonTextColor, for: .normal)
        addButton.backgroundColor = moduleButtonBackgroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dataSource = segmentedControl.selectedSegmentIndex == 0 ? currentWaitForAction.successModules : currentWaitForAction.failureModules
        sequenceCollectionView.reloadData()
        
        let itemsCount = sequenceCollectionView.numberOfItems(inSection: 0)
        if justAdded && itemsCount > 0  {
            sequenceCollectionView.scrollToItem(at: IndexPath(row: itemsCount - 1, section: 0), at: .bottom, animated: true)
            justAdded = false
        }
        
        // Disable swipe back gesture
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    // MARK: COLLECTION VIEW DATASOURCE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (dataSource.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row != (dataSource.count) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ModuleCollectionViewCell
            cell.isHidden = false
            
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingPathOfCellBeingDragged.row == indexPath.row {
                        print("KD: Hiding cell \(indexPath.row)")
                        cell.isHidden = true
                    }
                }
            }
            
            let module = dataSource[indexPath.row].value
            cell.backgroundColor = gradientFrom(color: UIColor(hex: module.tintColorString!), frame: cell.frame)
            let textColor = (UIColor.black.isContrasting(with: cell.backgroundColor!)) ? UIColor.black : UIColor.white
            cell.moduleLabel.text = module.label
            cell.detailLabel.text = Module.detailTextFromModule(module: module)
            
            cell.moduleLabel.textColor = textColor
            cell.detailLabel.textColor = textColor
            
            cell.deleteButton.isHidden = true
            cell.reorderButton.isHidden = true
            
            if editMode {
                cell.deleteButton.isHidden = false
                cell.reorderButton.isHidden = false
                cell.deleteButton.setTitleColor(textColor, for: .normal)
                cell.deleteButton.tag = indexPath.row
                cell.deleteButton.addTarget(self, action: #selector(SequenceCollectionViewController.deleteModule), for: .touchUpInside)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addBlockCell", for: indexPath) as! ModuleCollectionViewCell
            cell.moduleLabel.text = "+ add"
            cell.backgroundColor = addModuleCellBackgroundColor
            return cell
        }
    }
    
    // MARK: COLLECTION VIEW DELEGATE
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? ModuleCollectionViewCell {
            switch cell.moduleLabel.text! {
            case "Delay", "Bleeding", "Oxygenation Failure", "Pump Noise", "Line Shattering", "Power Disconnection", "Vital Signs":
                self.performSegue(withIdentifier: "editModule", sender: self)
            case "Change Parameters":
                self.performSegue(withIdentifier: "editChangeParametersFromWaitForAction", sender: self)
            case "Display Message":
                self.performSegue(withIdentifier: "editModule", sender: self)
            default:
                break
            }
        }
    }
    
    // MARK : KDDragAndDropCollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, dataItemForIndexPath indexPath: IndexPath) -> AnyObject {
        var item: Module
        print("KD: dataItemForIndexPath:")
        if indexPath.row != (collectionView.numberOfItems(inSection: indexPath.section) - 1) {
            if !dataSource.isEmpty {
                debugPrint(dataSource)
                item = dataSource[indexPath.row].value
                print("module:")
                debugPrint(item)
            } else {
                print("dataItemForIndexPath no item found.")
                return Module()
            }
        } else {
            print("Attempting to move add button...")
            return Module()
        }
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, insertDataItem dataItem : AnyObject, atIndexPath indexPath: IndexPath) -> Void {
        print("Inserted item at \(indexPath).")
    }
    
    func collectionView(_ collectionView: UICollectionView, deleteDataItemAtIndexPath indexPath : IndexPath) -> Void {
        // TODO: Implement delete
        print("Deleted item from \(indexPath).")
    }
    
    func collectionView(_ collectionView: UICollectionView, moveDataItemFromIndexPath from: IndexPath, toIndexPath to : IndexPath) -> Void {
        if to.row != (collectionView.numberOfItems(inSection: to.section) - 1) {
            let isSuccessFailure = segmentedControl.selectedSegmentIndex == 0 ? true : false
            let sIndex = from.row
            let dIndex = to.row
            print("Moving item from \(sIndex) to \(dIndex)...")
            mainQueue.reorder(from: sIndex, to: dIndex, toSuccessQueue: isSuccessFailure)
        }
        dataSource = segmentedControl.selectedSegmentIndex == 0 ? currentWaitForAction.successModules : currentWaitForAction.failureModules
    }
    
    func collectionView(_ collectionView: UICollectionView, indexPathForDataItem dataItem: AnyObject) -> IndexPath? {
        if let candidate = dataItem as? Module {
            for item in dataSource {
                if candidate.id == item.value.id {
                    let index = dataSource.index(of: item)!
                    let indexPath = IndexPath(row: index, section: 0)
                    
                    print("KD: indexPathForDataItem \(index) with module:")
                    debugPrint(candidate)
                    
                    return indexPath
                }
            }
        } else {
            print("No index path found.")
        }
        return nil
    }
    
    // MARK: SEGMENTED CONTROL
    @IBAction func segmentedControlDidChange(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            print("Changing to success...")
            dataSource = currentWaitForAction.successModules
            debugPrint(currentWaitForAction.successModules)
            sequenceCollectionView.reloadData()
            segmentedControl.tintColor = successTintColor
        case 1:
            print("Changing to failure...")
            dataSource = currentWaitForAction.failureModules
            debugPrint(currentWaitForAction.failureModules)
            sequenceCollectionView.reloadData()
            segmentedControl.tintColor = failureTintColor
        default:
            break
        }
    }
    
    // MARK: EDIT MODE
    func setupEditMode() {
        currentWaitForAction = mainQueue.moduleAt(index: editModuleIndex!) as! WaitForAction
        addButton.setTitle("Update", for: .normal)
    }
    
    @IBAction func toggleEditMode(_ sender: UIBarButtonItem) {
        editMode = !editMode
        sender.title = editMode ? "Done" : "Edit"
        sequenceCollectionView?.reloadData()
    }
    
    func deleteModule(_ sender: UIButton) {
        print("Deleting module \(sender.tag)...")
        let successFailure = segmentedControl.selectedSegmentIndex == 0 ? true : false
        mainQueue.deleteModuleAt(index: sender.tag, fromSucessFailure: successFailure, waitForAction: currentWaitForAction)
        dataSource = segmentedControl.selectedSegmentIndex == 0 ? currentWaitForAction.successModules : currentWaitForAction.failureModules
        sequenceCollectionView.reloadData()
    }

    @objc func doneButtonAction() {
        actionField.resignFirstResponder()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier != "addToSequenceFromWaitForAction" && identifier != "editModule" && identifier != "editChangeParametersFromWaitForAction" {
            if currentWaitForAction.successModules.isEmpty || currentWaitForAction.failureModules.isEmpty {
                let alert = UIAlertController(title: "Incomplete Setup", message: "Please make sure to place at least one module for both success and failure conditions.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alert, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    // MARK: NAVIGATION
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        try! realm.write {
            currentWaitForAction.actionLabel = actionField.text
        }
        
        switch segue.identifier {
        case "editModule"?:
            if let destination = segue.destination as? ModuleConfigureViewController {
                if let indexPath = sequenceCollectionView.indexPathsForSelectedItems?.first {
                    destination.editModuleIndex = indexPath.row
                    if let selectedCell = sequenceCollectionView?.cellForItem(at: indexPath) as? ModuleCollectionViewCell {
                        destination.selectedModuleLabel = selectedCell.moduleLabel.text
                        destination.isSuccessFailureAction = (segmentedControl.selectedSegmentIndex == 0) ? true : false
                    }
                }
            }
        case "editChangeParametersFromWaitForAction"?:
            if let destination = segue.destination as? ParameterControlViewController {
                if let indexPath = sequenceCollectionView.indexPathsForSelectedItems?.first {
                    destination.editModuleIndex = indexPath.row
                    destination.isSuccessFailureAction = (segmentedControl.selectedSegmentIndex == 0) ? true : false
                }
            }
        case "addToSequenceFromWaitForAction"?:
            if let destination = segue.destination as? ModuleCollectionViewController {
                destination.isSuccessFailureAction = (segmentedControl.selectedSegmentIndex == 0) ? true : false
            }
        default:
            if editModuleIndex == nil {
                mainQueue.addToQueue(waitForAction: currentWaitForAction)
                if let destination = segue.destination as? SequenceCollectionViewController {
                    destination.justAdded = true
                }
            } else {
                debugPrint(currentWaitForAction)
                print(editModuleIndex as Any)
                mainQueue.updateModuleAt(index: editModuleIndex!, module: currentWaitForAction)
            }
        }
    }
    
    @IBAction func prepareForUnwindToWaitForAction(_ segue: UIStoryboardSegue) {
        print("Unwinding to sequence to Wait for Action configure screen...")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("Removing keyboard observers...")
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
}




























