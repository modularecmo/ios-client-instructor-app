//
//  ChangeParameters.swift
//  SequenceManager
//
//  Created by Abdol on 2/28/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class ChangeParameters: Module {
    @objc dynamic var parameters: Parameters?
    
    required override init() {
        super.init()
        self.label = "Change Parameters"
        self.name = "changeParameters"
        self.tintColorString = "#30C2FF"
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["rpm": parameters?.rpm.value, "Pven": parameters?.Pven.value, "Part": parameters?.Part.value, "Pint": parameters?.Pint.value, "svO2": parameters?.svO2.value, "Tart": parameters?.Tart.value, "Tven": parameters?.Tven.value, "Hb": parameters?.Hb.value, "Hct": parameters?.Hct.value, "V": parameters?.V.value, "deltaP": parameters?.deltaP.value]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
