//
//  Utilities.swift
//  AssistedControl
//
//  Created by Air on 10/20/16.
//  Copyright © 2016 QU. All rights reserved.
//

import Foundation
import UIKit
import Intents

// PARAMETER LABEL FORMATTING
let rpmFormat = "%4.0f"
let Vformat = "%.2f"
let pressureFormat = "%.0f"
let deltaPressureFormat = "%.1f"
let temperatureFormat = "%.1f"
let HbFormat = "%.1f"
let HctFormat = "%.1f"
let svO2Format = "%.1f"

// MARK: UTILITIY METHODS

// GRADIENT GENERATOR
func gradientFrom(color: UIColor, frame: CGRect) -> UIColor {
    return color
    //return UIColor(gradientStyle: .leftToRight, withFrame: frame, andColors: [color.add(hue: 0.0, saturation: gradientSaturation, brightness: 0, alpha: 0), color])
}

func gradientFrom(firstColor: UIColor, secondColor: UIColor, frame: CGRect) -> UIColor {
    //return UIColor(gradientStyle: .topToBottom, withFrame: frame, andColors: [firstColor, secondColor])
    return firstColor
}

// DRAW TIMELINE
func drawTimelineTo(view: UICollectionView, height: CGFloat? = nil) {
    //// Bezier Drawing
    let bezierPath = UIBezierPath()
    bezierPath.move(to: CGPoint(x: view.frame.width / 2, y: 0))
    bezierPath.addLine(to: CGPoint(x: view.frame.width / 2, y: height != nil ? height! - 77 : view.frame.height))
    
    let layer = CAShapeLayer()
    layer.strokeColor = timelineStrokeColor.cgColor
    layer.path = bezierPath.cgPath
    layer.lineWidth = 2
    
    view.backgroundView = UIView(frame: CGRect(origin: view.frame.origin, size: CGSize(width: view.frame.width, height: (height != nil ? height! - 77 : view.frame.height))))
    view.backgroundView?.clipsToBounds = true
    view.backgroundView?.layer.masksToBounds = true
    view.backgroundView?.layer.addSublayer(layer)
}

// MARK: KEYBOARD METHODS
func addDoneButtonOnKeyboard(_ sender: UITextField, target: Any, action: Selector)
{
    let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: 320, height: 50))
    doneToolbar.barStyle = UIBarStyle.default
    
    let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: target, action: action)
    done.tintColor = UIColor.black
    
    doneToolbar.items = [flexSpace, done]
    doneToolbar.sizeToFit()
    
    sender.inputAccessoryView = doneToolbar
}

// MARK: SIRI
func requestSiriAuthorization() {
    if #available(iOS 10.0, *) {
        INPreferences.requestSiriAuthorization { (status) in
            switch status {
            case .authorized:
                print("Siri authorized.")
            default:
                print("Siri not authorized.")
            }
        }
    } else {
        // Fallback on earlier versions
    }
}

// MARK: REACHABILITY
func checkInternetConnection(target: UIViewController, handler: ((Bool) -> Void)? = nil, message: String = "You are not connected to the internet. Please connect to a network to enable live control.") {
    let connected = Reachability.isConnectedToNetwork()
    if !connected {
        let alert = UIAlertController(title: "Cannot Connect", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        target.present(alert, animated: true, completion: nil)
    }
    handler?(connected)
}

// MARK: LABEL SUBSCRIPT/SUPERSCRIPT
extension NSMutableAttributedString
{
    enum scripting : Int
    {
        case aSub = -1
        case aSuper = 1
    }
    
    func characterSubscriptAndSuperscript(string:String,
                                          characters:[Character],
                                          type:scripting,
                                          fontSize:CGFloat,
                                          scriptFontSize:CGFloat,
                                          offSet:Int,
                                          length:[Int],
                                          alignment:NSTextAlignment, color: UIColor = UIColor.black)-> NSMutableAttributedString
    {
        let paraghraphStyle = NSMutableParagraphStyle()
        // Set The Paragraph aligmnet , you can ignore this part and delet off the function
        paraghraphStyle.alignment = alignment
        
        var scriptedCharaterLocation = Int()
        //Define the fonts you want to use and sizes
        let stringFont = UIFont.systemFont(ofSize: fontSize)
        let scriptFont = UIFont.systemFont(ofSize: scriptFontSize)
        // Define Attributes of the text body , this part can be removed of the function
        let attString = NSMutableAttributedString(string:string, attributes: [NSAttributedString.Key.font:stringFont,NSAttributedString.Key.foregroundColor:color,NSAttributedString.Key.paragraphStyle: paraghraphStyle])
        
        // the enum is used here declaring the required offset
        let baseLineOffset = offSet * type.rawValue
        // enumerated the main text characters using a for loop
        for (i,c) in string.characters.enumerated()
        {
            // enumerated the array of first characters to subscript
            for (theLength,aCharacter) in characters.enumerated()
            {
                if c == aCharacter
                {
                    // Get to location of the first character
                    scriptedCharaterLocation = i
                    //Now set attributes starting from the character above
                    attString.setAttributes([NSAttributedString.Key.font:scriptFont,
                                             // baseline off set from . the enum i.e. +/- 1
                                             NSAttributedString.Key.baselineOffset:baseLineOffset,
                                             NSAttributedString.Key.foregroundColor:color],
                                            // the range from above location
                        range:NSRange(location:scriptedCharaterLocation,
                                      // you define the length in the length array 
                            // if subscripting at different location 
                            // you need to define the length for each one
                            length:length[theLength]))
                    
                }
            }
        }
        return attString}
}
















