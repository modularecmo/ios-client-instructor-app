//
//  WaitForActionCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 5/5/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import RealmSwift

class WaitForActionCollectionViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var moduleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var reorderButton: UIButton!
    @IBOutlet weak var successCollectionView: UICollectionView!
    @IBOutlet weak var failureCollectionView: UICollectionView!
    
    var successModules = List<AnyModule>()
    var failureModules = List<AnyModule>()
    
    var collectionViewHeight: CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = moduleShadowRadius
        self.layer.shadowOpacity = moduleShadowOpacity
        self.layer.masksToBounds = false;
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: moduleCornerRadius).cgPath
        
        self.layer.cornerRadius = moduleCornerRadius
        
        successCollectionView.backgroundColor = UIColor.clear;
        successCollectionView.backgroundView = nil
        
        failureCollectionView.backgroundColor = UIColor.clear;
        failureCollectionView.backgroundView = nil
        
        successCollectionView.reloadData()
        failureCollectionView.reloadData()
        
        // Draw timelines
        drawTimelineTo(view: failureCollectionView, height: collectionViewHeight)
        drawTimelineTo(view: successCollectionView, height: collectionViewHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 1:
            let height = CGFloat(successModules.count) * waitForActionModuleItemSize.height
            collectionViewHeight = collectionViewHeight < height ? height : collectionViewHeight
            print("Success count \(successModules.count)")
            return successModules.count
        case 2:
            let height = CGFloat(failureModules.count) * waitForActionModuleItemSize.height
            collectionViewHeight = collectionViewHeight < height ? height : collectionViewHeight
            print("Failure count \(successModules.count)")
            return failureModules.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("Cell \(indexPath.row) for tag \(collectionView.tag)")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "moduleCell", for: indexPath) as! WaitForActionSubCollectionViewCell
        cell.moduleLabel.textColor = UIColor.white
        switch collectionView.tag {
        case 1:
            print("Success \(indexPath.row)")
            cell.backgroundColor = UIColor(hex: successModules[indexPath.row].value.tintColorString!)
            cell.moduleLabel.text = successModules[indexPath.row].value.label
        case 2:
            print("Failure \(indexPath.row)")
            cell.backgroundColor = UIColor(hex: failureModules[indexPath.row].value.tintColorString!)
            cell.moduleLabel.text = failureModules[indexPath.row].value.label
        default:
            break
        }

        cell.layer.cornerRadius = moduleCornerRadius
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }

}
