//
//  Bleeding.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Bleeding: Module {
    @objc dynamic var flowRate: Float = 1
    
    required override init() {
        super.init()
        self.label = "Bleeding"
        self.name = "bleeding"
        self.descriptionString = "Simulate patient bleeding using thermochromic ink for a specified period (in seconds) with a specified flow rate. \n\nTip: Add a Change Parameters module in parallel indicate bleeding on CARDIOHELP screen. "
        self.tintColorString = "#FF473A"
    }
    
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil", "bleedFlow": flowRate]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    @objc static func getMinimumMaximumBleedFlowRate(completion: @escaping () -> ()) {
        databaseReference.child(modulesMasterPath + "/" + patientDiagnosticsMasterPath).observeSingleEvent(of: .value, with: { (snapshot) in
        print("Retreiving bleeding max/min bleed flow rate values...")
        debugPrint(snapshot)
        if let patientDiagnostics = snapshot.value as? [String: Any] {
            if let _minimumFlowRate = patientDiagnostics[bleedMinimumFlowRatePath] as? NSNumber {
                if _minimumFlowRate != -1 {
                    minimumFlowRate = _minimumFlowRate
                }
            }
            if let _maximumFlowRate = patientDiagnostics[bleedMaximumFlowRatePath] as? NSNumber {
                if _maximumFlowRate != -1 {
                    maximumFlowRate = _maximumFlowRate
                }
            }
        }
        print("minimumFlowRate: \(minimumFlowRate), maximumFlowRate: \(maximumFlowRate).")
        completion()
        saveSettings()
        })
    }
}
