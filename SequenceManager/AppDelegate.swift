//
//  AppDelegate.swift
//  SequenceManager
//
//  Created by Abdol on 2/28/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import TaskQueue
import RealmSwift
// import Fabric
// import Crashlytics

// MARK: Databases
public var databaseReference: DatabaseReference! = Database.database().reference()
public var couchDBclient: minimalCouch!
public var realm = try! Realm()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        FirebaseApp.configure()
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 20,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
//        try! realm.write {
//            realm.deleteAll()
//        }

//        try! realm.write {
//            realm.delete(realm.objects(Settings.self))
//        }
        
        // Setup app analytics
        // Fabric.with([Crashlytics.self])

        // Load settings
        if let _settings = realm.objects(Settings.self).first {
            settings = _settings
        } else {
            // Create settings object if it doesn't exist
            try! realm.write {
                realm.add(settings)
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        if databaseReference != nil {
            databaseReference.child("sequence/running").setValue(false)
            databaseReference.child("sequence/main").setValue(nil)
            databaseReference.child("sequence/currentJobIndex").setValue(0)
        }
    }
}

