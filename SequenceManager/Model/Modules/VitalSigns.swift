//
//  VitalSigns.swift
//  SequenceManager
//
//  Created by Abdol on 9/19/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class VitalSigns: Module {
    required override init() {
        super.init()
        self.label = "Vital Signs"
        self.name = "vitalSigns"
        self.descriptionString = "Shows parameters to be applied in the vital signs monitor."
        self.tintColorString = "#714771"
    }
    
    @objc dynamic var heartRate: Float = 0
    @objc dynamic var spO2: Float = 0
    @objc dynamic var bloodPressure: String = ""
    @objc dynamic var respiratoryRate: Float = 0
    @objc dynamic var co2: Float = 0
    @objc dynamic var temperature: Float = 0
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
