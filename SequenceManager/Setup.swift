//
//  Setup.swift
//  SequenceManager
//
//  Created by Abdol on 3/18/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import UIKit

// MARK: GENERAL
let navigationBarTintColor = UIColor(hex: "#eee")
let navigationBarTextColor = UIColor.black
let timelineStrokeColor = UIColor(hex: "#D0D0D0")
let clottingEnabled = false
let useAnimations = false

// MARK: SETTINGS
var settings = Settings() {
    didSet {
        loadSettings()
    }
}

// MARK: COUCHDB
let couchDBEnabled = false

/*let databaseName = "ecmo"
let documentID = "database"
let databaseURL = "http://192.168.43.72:5984/" //"http://127.0.0.1:5984/"
let databaseUsername: String? = nil
let databasePassword: String? = nil*/

let databaseName = "ecmo"
//let documentID = "module_database"
let documentID = "database"
let parametersDocumentID = "parameters_database"
let sequenceDocumentID = "sequence_database"
let databaseURL = "http://192.168.4.1:5984/" // Make sure you add forward slash at the end of this string
let databaseUsername: String? = nil
let databasePassword: String? = nil

// MARK: FIREBASE PATHS
let modulesMasterPath = "modules"
let patientMasterPath = "patient"
let patientDiagnosticsMasterPath = "patientDiagnostics"
let heaterMasterPath = "heater"
let oxygenatorMasterPath = "oxygenator"
// Oxygenator paths
let oxygenatorNoisePath = "noise"
let clottingPath = "clotting"
// Patient paths
let bleedingPath = "bleed"
let bleedingFlowRatePath = "bleedLimit"
let lineShatteringPath = "lineShattering"
let pumpPath = "loop"
// Patient diagnostics
let bleedMaximumFlowRatePath = "bleedHighFlow"
let bleedMinimumFlowRatePath = "bleedLowFlow"
let bleedDiagnosticFlowRate = "bleedFlow"
// Heater paths
let deoxygenationPath = "oxygenation"
// Other paths
let powerDisconnectionPath = "powerDisconnection"
let displayMessagePath = "displayMessage"

// MARK: MODULE CONFIGURATION 
let moduleCornerRadius: CGFloat = 7
let moduleConfigurationCornerRadius: CGFloat = 15
let moduleShadowOpacity: Float = 0.45
let moduleShadowRadius: CGFloat = 4
let buttonCornerRadius: CGFloat = 11
var cellWidth: CGFloat = 480
let cellHeight: CGFloat = 135
let waitForActionCellHeight: CGFloat = 400
let waitForActionModuleItemSize = CGSize(width: 110, height: 60)
let flowRateUnit = "l/m"

// MARK: MODULE SELECTOR
let moduleCellHeight: CGFloat = 100
let moduleCellWidth: CGFloat = 200

// MARK: BLEEDING MODULE CONFIGURATION
var minimumFlowRate: NSNumber = 0
var maximumFlowRate: NSNumber = 0

// MARK: CLOTTING MODULE CONFIGURATION
var currentClotting: Clotting = Clotting()
let numberOfClottingMatrixCells = 30
var clottingCellDeactivatedColor = UIColor.black
var clottingCellActivatedColor = UIColor.red
let clottingCellCornerRadius: CGFloat = 15 // half of cell width
let clotMatrixCollectionViewRotationAngle = CGFloat(Double.pi / 4)
let clotMatrixCollectionViewScaleFactor: CGFloat = 0.7

// MARK: SEQUENCE CONFIGURATION
let sequenceShadowRadius: CGFloat = 10
let sequenceCornerRadius: CGFloat = 10
let sequenceShadowOpacity: Float = 0.35
let moduleIndicatorFontSize: CGFloat = 23
let sequenceCellHeight: CGFloat = 170
let sequenceCellWidth: CGFloat = 255

// MARK: COLORS
var normalBarBackgroundColor = UIColor.white
var textColor: UIColor = UIColor.black
var fieldTextColor: UIColor = UIColor.black
var fieldBackgroundColor: UIColor = UIColor.white
var disabledFieldColor = UIColor(hex: "#eee")
var disabledFieldAlpha: CGFloat = 0.4
var altSwitchOnTintColor: UIColor = UIColor(hex: "#118AB2")
var placeholderTextColor: UIColor = UIColor.white
var placeholderTextColorAlt: UIColor = UIColor(hex: "#EBEBEB")
var successTintColor: UIColor = UIColor(hex: "#50D889")
var failureTintColor: UIColor = UIColor(hex: "#EB5757")
let waitForActionBackgroundColor = UIColor(hex: WaitForAction().tintColorString!).add(hue: 0.0, saturation: -0.2, brightness: 0.1, alpha: 0.0)
let addModuleCellBackgroundColor = UIColor(hex: "#E3E3E3")
let addSequenceCellBackgroundColor = UIColor(hex: "#E6E6E6")
let gradientSaturation: CGFloat = -0.22
let sequenceSelectorTintColor = UIColor.black
let deleteSequenceButtonTintColor = UIColor.red
let moduleButtonBackgroundColor = UIColor.white
let moduleButtonTextColor = UIColor.black
let moduleButtonBackgroundColorAlt = UIColor.black
let moduleButtonTextColorAlt = UIColor.white
let parameterControlColor = UIColor(hex: "#7F7C7C")
let pumpModuleBackgroundColor = UIColor(hex: "#21ADBC")
let sendMessageHighlightColor = UIColor(hex: "#29BC55")
let removeParallelModuleBackgroundColor = UIColor(hex: "#FF6666")

// MARK: PARAMETER CONTROL
let continuousSliderEvents = false
let continuousSliderCloudUpdates = false

// MARK: RUNNING VIEW
let runningBackgroundColor = UIColor.white
let connectingTabBarBackgroundColor = UIColor(hex: "#FF8C42")
let runningTabBarBackgroundColor = UIColor(hex: "#FF473A")
let runningTextColor = UIColor.white
let disabledModuleAlpha: CGFloat = 0.35
let moduleChangeThreshold: Double = 4 // in seconds
let waitForActionChangeThreshold: Double = 4 // in seconds
let waitForActionSubModuleLoadingBackgroundColor = UIColor.gray
let waitForActionSubModuleLoadingMessage = "Loading module..."
let splitViewWidthConstant: CGFloat = 2.2
let splitViewPreferredPrimaryColumnWidthFraction: CGFloat = 0.35
let runningModuleCellHeight: CGFloat = 270
let runningModuleCellWidth: CGFloat = 230
let vitalSignsRunningCellDetailLabelFontSize: CGFloat = 17

// MARK: SPECIAL
var emojis = ["🎬", "🚑", "💉", "🌡", "🤒", "🤕", "😷"]

var mainQueue = QueueManager()
