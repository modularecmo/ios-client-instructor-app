//
//  SequenceCollectionViewCell.swift
//  SequenceManager
//
//  Created by Abdol on 4/2/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import UIKit

class SequenceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var modulesStackView: UIStackView!
    
    override func layoutSubviews() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = sequenceShadowRadius
        self.layer.shadowOpacity = sequenceShadowOpacity
        self.layer.masksToBounds = false;
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: sequenceCornerRadius).cgPath
        
        self.layer.cornerRadius = sequenceCornerRadius
    }
}
