//
//  Deoxygenation.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Deoxygenation: Module {    
    required override init() {
        super.init()
        self.label = "Oxygenation Failure"
        self.name = "deoxygenation"
        self.descriptionString = "Simulate blood deoxygenation (stop thermochromic system's color change effect) for a specified period (in seconds)."
        self.tintColorString = "#7A0800"
    }
    
    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil"]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
