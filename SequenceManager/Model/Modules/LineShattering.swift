//
//  LineShattering.swift
//  SequenceManager
//
//  Created by Abdol on 3/1/17.
//  Copyright © 2017 Abdol. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class LineShattering: Module {
    @objc dynamic var mode: Int = 2 // 1 is center signal, 2 is soft shattering, and 3 is hard shattering
    @objc dynamic var center: Bool = false
    
    required override init() {
        super.init()
        self.label = "Line Shattering"
        self.name = "lineShattering"
        self.descriptionString = "Simulate line shattering for a specified period (in seconds)."
        self.tintColorString = "#9A3EC2"
    }

    override func dictionary() -> [String: Any] {
        var dictionary: [String: Any] = ["duration": duration.value ?? "nil", "mode": mode, "center": center]
        if self.parallelModule != nil {
            dictionary["parallelModule"] = [self.parallelModule!.value.name : self.parallelModule!.value.dictionary()]
        }
        return dictionary
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
}
